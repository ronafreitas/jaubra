import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';//, Headers, RequestOptions
//import 'rxjs/add/operator/map';
 
@Injectable()
export class CadastroProvider {

  //private API_URL = 'http://localhost/api_jaubra/';
  private API_URL = 'http://jaubra.faromidia.com.br/api_jaubra/';
 
  constructor(public http: Http) { }

	checaEmail(email: string){
		return new Promise((resolve, reject) => {
		  let url = this.API_URL + 'checaemail/' + email;
		  this.http.get(url)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}

  cadastrar(
  	email: string, 
  	nome: string,
  	sobrenome: string,
  	celular: string,
  	senha: string,
  	perfil: string,
  	cpf: string
  	) {
    return new Promise((resolve, reject) => {
      var data = {
        email: email,
        nome: nome,
        sobrenome: sobrenome,
        celular: celular,
        senha: senha,
        perfil: perfil,
        cpf: cpf
      };
 
      this.http.post(this.API_URL + 'cadastrar', data)
        .subscribe((result: any) => {
          resolve(result.json());
        },
        (error) => {
          reject(error.json());
        });
    });
  }
 
  login(cpf: string, senha: string) {
    return new Promise((resolve, reject) => {

      var data = {
        cpf: cpf,
        senha: senha
      };

      /*let headers = new Headers({
        'Content-Type': 'application/json'
      });

      let options = new RequestOptions({
        headers: headers
      });*/

      /*var optionsx = {
        headers : {
          'Content-Type' : 'application/json'
        }
      }*/

      this.http.post(this.API_URL + 'login', data)
      .subscribe((result: any) => {
        resolve(result.json());
      },
      (error) => {
        reject(error.json());
      });
    });
  }

  logout(id_user: number){
    return new Promise((resolve, reject) => {
      var data = {id: id_user};
      this.http.post(this.API_URL + 'logout', data)
      .subscribe((result: any) => {
        resolve(result.json());
      },
      (error) => {
        reject(error.json());
      });
    });
  }

}