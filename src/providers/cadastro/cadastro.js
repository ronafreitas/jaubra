var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Http } from '@angular/http'; //, Headers, RequestOptions
//import 'rxjs/add/operator/map';
var CadastroProvider = /** @class */ (function () {
    function CadastroProvider(http) {
        this.http = http;
        //private API_URL = 'http://localhost/api_jaubra/';
        this.API_URL = 'http://jaubra.faromidia.com.br/api_jaubra/';
    }
    CadastroProvider.prototype.checaEmail = function (email) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = _this.API_URL + 'checaemail/' + email;
            _this.http.get(url)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    CadastroProvider.prototype.cadastrar = function (email, nome, sobrenome, celular, senha, perfil, cpf) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var data = {
                email: email,
                nome: nome,
                sobrenome: sobrenome,
                celular: celular,
                senha: senha,
                perfil: perfil,
                cpf: cpf
            };
            _this.http.post(_this.API_URL + 'cadastrar', data)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    CadastroProvider.prototype.login = function (cpf, senha) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var data = {
                cpf: cpf,
                senha: senha
            };
            /*let headers = new Headers({
              'Content-Type': 'application/json'
            });
      
            let options = new RequestOptions({
              headers: headers
            });*/
            /*var optionsx = {
              headers : {
                'Content-Type' : 'application/json'
              }
            }*/
            _this.http.post(_this.API_URL + 'login', data)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    CadastroProvider.prototype.logout = function (id_user) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var data = { id: id_user };
            _this.http.post(_this.API_URL + 'logout', data)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    CadastroProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http])
    ], CadastroProvider);
    return CadastroProvider;
}());
export { CadastroProvider };
//# sourceMappingURL=cadastro.js.map