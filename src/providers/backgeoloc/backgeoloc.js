var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
//import { Platform } from 'ionic-angular';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
//import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';
//import { Geolocation, Geoposition } from '@ionic-native/geolocation';
//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/filter';
var BackgeolocProvider = /** @class */ (function () {
    //public watch: any;
    function BackgeolocProvider(backgroundGeolocation) {
        this.backgroundGeolocation = backgroundGeolocation;
    }
    /*
    - desiredAccuracy:
    Precisão desejada em metros. Valores possíveis [0, 10, 100, 1000], quanto menor, mais preciso, mas também consome mais bateria
    
    - stationaryRadius:
    distancia em metros q o motorista tem q percorrer, após ter ficado parado, para que seja medida novamente
    
    - distanceFilter:
    A distância mínima em metros que um dispositivo deve ser movida horizontalmente antes que um evento de atualização seja gerado
    */
    BackgeolocProvider.prototype.iniciarBackgroundGeo = function (motorista, cidEst) {
        var ts = this;
        ts.backgroundGeolocation.stop();
        var config = {
            desiredAccuracy: 0,
            stationaryRadius: 1,
            distanceFilter: 1,
            notificationTitle: 'Jaubra',
            notificationText: 'em movimento',
            notificationIconLarge: "icon",
            notificationIconSmall: "icon",
            notificationIconColor: "#33afb5",
            debug: false,
            locationProvider: ts.backgroundGeolocation.LocationProvider.DISTANCE_FILTER_PROVIDER,
            url: 'http://jaubra.faromidia.com.br/api_jaubra_bkg/?m=' + motorista + "&ci=" + cidEst,
            //syncUrl: 'http://jaubra.faromidia.com.br/api_jaubra_bkg_erro/?m='+motorista+"&ce="+cidEst,
            httpHeaders: {
                "Content-Type": "application/json",
                'X-FOO': 'sssss444444'
            },
            postTemplate: {
                lat: '@latitude',
                lon: '@longitude',
                foo: 'abc123'
            },
            stopOnTerminate: false,
            interval: 2000,
            syncThreshold: 5000,
            maxLocations: 10000,
            startForeground: true,
            startOnBoot: false
        };
        ts.backgroundGeolocation.configure(config)
            .subscribe(function (location) {
            //console.log('looc',location);
            ts.backgroundGeolocation.finish();
        }, function (err) {
            console.log('err-1', err);
        });
        ts.backgroundGeolocation.start();
        /*
        

            COLOCAR WHATCH para forenground   e   setar startForeground: para false

        
        */
        /*

- location 	Location 	all 	all 	on location update
- stationary 	Location 	all 	DIS,ACT 	on device entered stationary mode
- activity 	Activity 	Android 	ACT 	on activity detection
- error 	{ code, message } 	all 	all 	on plugin error
- authorization 	status 	all 	all 	on user toggle location service
- start 		all 	all 	geolocation has been started
- stop 		all 	all 	geolocation has been stopped
- foreground 		Android 	all 	app entered foreground state (visible)
- background 		Android 	all 	app entered background state

        */
        /*
        BackgroundGeolocation.headlessTask(function(event) {

            if(event.name === 'location' || event.name === 'stationary'){
                var xhr = new XMLHttpRequest();
                xhr.open('POST', 'http://192.168.81.14:3000/headless');
                xhr.setRequestHeader('Content-Type', 'application/json');
                xhr.send(JSON.stringify(event.params));
            }
        
            return 'Processing event: ' + event.name; // will be logged
        });

        Event listeners can registered with:

        const eventSubscription = BackgroundGeolocation.on('event', callbackFn);
        
        And unregistered:
        
        eventSubscription.remove();
        */
    };
    BackgeolocProvider.prototype.pararBackgroundGeo = function () {
        var ts = this;
        ts.backgroundGeolocation.stop();
        var config = {
            desiredAccuracy: 1000,
            stationaryRadius: 1000,
            distanceFilter: 1000,
            notificationTitle: 'Jaubra - desligado',
            notificationText: 'off',
            notificationIconLarge: "icon",
            notificationIconSmall: "icon",
            notificationIconColor: "#33afb5",
            debug: false,
            locationProvider: this.backgroundGeolocation.LocationProvider.DISTANCE_FILTER_PROVIDER,
            url: 'http://jaubra.faromidia.com.br/api_jaubra_bkg/?m=1&ci=xx',
            //syncUrl: 'http://jaubra.faromidia.com.br/api_jaubra_bkg_erro/?m='+motorista+"&ce="+cidEst,
            httpHeaders: {
                "Content-Type": "application/json",
                'X-FOO': 'sss11s4s'
            },
            postTemplate: {
                lat: '@latitude',
                lon: '@longitude',
                foo: 'aa33eeeee'
            },
            stopOnTerminate: true,
            interval: 2000000,
            syncThreshold: 5000,
            maxLocations: 10000,
            startForeground: false,
            startOnBoot: false
        };
        ts.backgroundGeolocation.configure(config)
            .subscribe(function (location) {
            console.log('looc2', location);
            //ts.backgroundGeolocation.finish();
            //ts.backgroundGeolocation.stop();
        }, function (err) {
            console.log('err-2', err);
        });
        ts.backgroundGeolocation.start();
        //ts.backgroundGeolocation.stop();
        //ts.backgroundGeolocation.start();
        //this.backgroundGeolocation.finish();
        //ts.backgroundGeolocation.stop();
        /*BackgroundGeolocation.on('location', function(location) {
            // handle your locations here
            // to perform long running operation on iOS
            // you need to create background task
            BackgroundGeolocation.startTask(function(taskKey) {
                // execute long running task
                // eg. ajax post location
                // IMPORTANT: task has to be ended by endTask
                BackgroundGeolocation.endTask(taskKey);
            });
        });*/
    };
    BackgeolocProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [BackgroundGeolocation])
    ], BackgeolocProvider);
    return BackgeolocProvider;
}());
export { BackgeolocProvider };
//# sourceMappingURL=backgeoloc.js.map