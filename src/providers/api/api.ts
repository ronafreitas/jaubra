import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
//import { HttpClient } from '@angular/common/http';
//public http: HttpClient
// ver https://www.djamware.com/post/59fc9da680aca7739224ee20/ionic-3-and-angular-5-mobile-app-example
// e ver https://stackoverflow.com/questions/45129790/difference-between-http-and-httpclient-in-angular-4
//import { Observable } from 'rxjs/Observable';
//import { map, catchError } from 'rxjs/operators';

@Injectable()
export class ApiProvider {

  //private API_URL = 'http://localhost/api_jaubra/';
  private API_URL = 'http://jaubra.faromidia.com.br/api_jaubra/';
	
	constructor(public http: Http) { }

	/*
	* recupera dados cadastrais dos usuários
	* GET
	* return object
	*/
	getDadosCadastrais(id: number){
		return new Promise((resolve, reject) => {
		  let url = this.API_URL + 'dadoscadastrais/' + id;
		  this.http.get(url)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}

	/*
	* atualiza dados cadastrais dos usuários
	* POST
	* return object
	*/
	updateDadosCadastrais(dados:any[]) {
		return new Promise((resolve, reject) => {
		  var data = {dados:dados};
		  this.http.post(this.API_URL + 'updatecadastro', data)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}

	/*
	* pega email e senha para depois enviar para recuperar senha
	* GET
	* return array/object
	*/
	getEmailSenhaRecuperar(email: string){
		return new Promise((resolve, reject) => {
		  let url = this.API_URL + 'getemailsenha/' + email;
		  this.http.get(url)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}

	/*
	* pega email e senha para depois enviar para recuperar senha
	* GET
	* return array/object
	*/
	dadosMotoristaCorrida(id_usuario: number){
		return new Promise((resolve, reject) => {
		  let url = this.API_URL+'dados-motorista-corrida/'+id_usuario;
		  this.http.get(url)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}


	/*
	* pega email e senha para depois enviar para recuperar senha
	* GET
	* return array/object
	*/
	filtaLatLong(latlong:string){
		return new Promise((resolve, reject) => {
		  let url = this.API_URL + 'filtalatlong/'+latlong;
		  this.http.get(url)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}


	/*
	* checa se CPF já está cadastrado ou não
	* GET
	* return boolean
	*/
	checacpf(cpf: string){
		return new Promise((resolve, reject) => {
		  let url = this.API_URL + 'checacpfexiste/' + cpf;
		  this.http.get(url)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}

	/*
	* checa se EMAIL já está cadastrado ou não
	* GET
	* return boolean
	*/
	checaemail(email: string){
		return new Promise((resolve, reject) => {
		  let url = this.API_URL + 'checaemailexiste/' + email;
		  this.http.get(url)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}

	/*
	* recupera a senha do usuário
	* POST
	* return object
	*/
	recuperarSenha(email:string, senha:string) {
		return new Promise((resolve, reject) => {
		  var data = {email:email, senha:senha};
		  this.http.post(this.API_URL + 'email/recuperasenha', data)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}

	/*
	* grava meio de pagamento (dinheiro ou cartão)
	* POST
	* return object
	*/
	padraoMeioPagamento(tipo:string, id_usuario:number) {
		return new Promise((resolve, reject) => {
		  var data = {tipo:tipo, id_usuario:id_usuario};
		  this.http.post(this.API_URL + 'meiopagamento', data)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}

	/*
	* grava endereço padrão
	* POST
	* return object
	*/
	padraoEndereco(id_endereco:string, id_usuario:number) {
		return new Promise((resolve, reject) => {
		  var data = {id_endereco:id_endereco, id_usuario:id_usuario};
		  this.http.post(this.API_URL + 'enderecopadrao', data)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}

	/*
	* grava veículo padrão
	* POST
	* return object
	*/
	padraoVeiculo(id_veiculo:number, id_usuario:number) {
		return new Promise((resolve, reject) => {
		  var data = {id_veiculo:id_veiculo, id_usuario:id_usuario};
		  this.http.post(this.API_URL + 'veiculopadrao', data)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}


	/*
	* atualiza cartão de crédito padrão
	* POST
	* return object
	*/
	cartaoPadrao(id_cartao:number, id_usuario:number) {
		return new Promise((resolve, reject) => {
		  var data = {id_cartao:id_cartao, id_usuario:id_usuario};
		  this.http.post(this.API_URL + 'cartaopadrao', data)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}

	/*
	* atualiza dados cadastrais dos usuários
	* POST
	* return array
	*/
	gravaEnderecos(id:number,dados:any[]) {
		return new Promise((resolve, reject) => {
		  var data = {id:id,dados:dados};
		  this.http.post(this.API_URL + 'gravaenderecos', data)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}


	/*
	* cadastra novo cartão de crédito
	* POST
	* return object
	* id = id do usuário
	*/
	novoCartaoCredito(id:number,dados:any[]) {
		return new Promise((resolve, reject) => {
		  var data = {id:id,dados:dados};
		  this.http.post(this.API_URL + 'novocartaocredito', data)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}

	/*
	* cadastra novo veículo
	* POST
	* return object
	* id = id do usuário
	*/
	novoVeiculo(id:number,dados:any[]) {
		return new Promise((resolve, reject) => {
		  var data = {id:id,dados:dados};
		  this.http.post(this.API_URL + 'novoveiculo', data)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}


	/*
	* recupera dados cadastrais dos usuários
	* GET
	* return object
	*/
	getMeusVeiculos(id: number){
		return new Promise((resolve, reject) => {
		  let url = this.API_URL + 'meusveiculos/' + id;
		  this.http.get(url)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}

	/*
	* recupera dados cadastrais dos usuários
	* GET
	* return object
	*/
	getMeioPagamento(id_usuario: number){
		return new Promise((resolve, reject) => {
		  let url = this.API_URL + 'meiodepagamento/' + id_usuario;
		  this.http.get(url)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}


	/*
	* recupera dados cadastrais dos usuários
	* GET
	* return array/object
	*/
	getMeusEnderecos(id: number){
		return new Promise((resolve, reject) => {
		  let url = this.API_URL + 'meusenderecos/' + id;
		  this.http.get(url)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}

	/*
	* recupera todos os cartões do usuário
	* GET
	* return array
	*/
	getMeusCartoes(id_usuario: number){
		return new Promise((resolve, reject) => {
		  let url = this.API_URL + 'meuscartoes/' + id_usuario;
		  this.http.get(url)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}

}
