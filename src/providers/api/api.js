var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
//import { HttpClient } from '@angular/common/http';
//public http: HttpClient
// ver https://www.djamware.com/post/59fc9da680aca7739224ee20/ionic-3-and-angular-5-mobile-app-example
// e ver https://stackoverflow.com/questions/45129790/difference-between-http-and-httpclient-in-angular-4
//import { Observable } from 'rxjs/Observable';
//import { map, catchError } from 'rxjs/operators';
var ApiProvider = /** @class */ (function () {
    function ApiProvider(http) {
        this.http = http;
        //private API_URL = 'http://localhost/api_jaubra/';
        this.API_URL = 'http://jaubra.faromidia.com.br/api_jaubra/';
    }
    /*
    * recupera dados cadastrais dos usuários
    * GET
    * return object
    */
    ApiProvider.prototype.getDadosCadastrais = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = _this.API_URL + 'dadoscadastrais/' + id;
            _this.http.get(url)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    /*
    * atualiza dados cadastrais dos usuários
    * POST
    * return object
    */
    ApiProvider.prototype.updateDadosCadastrais = function (dados) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var data = { dados: dados };
            _this.http.post(_this.API_URL + 'updatecadastro', data)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    /*
    * pega email e senha para depois enviar para recuperar senha
    * GET
    * return array/object
    */
    ApiProvider.prototype.getEmailSenhaRecuperar = function (email) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = _this.API_URL + 'getemailsenha/' + email;
            _this.http.get(url)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    /*
    * pega email e senha para depois enviar para recuperar senha
    * GET
    * return array/object
    */
    ApiProvider.prototype.dadosMotoristaCorrida = function (id_usuario) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = _this.API_URL + 'dados-motorista-corrida/' + id_usuario;
            _this.http.get(url)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    /*
    * pega email e senha para depois enviar para recuperar senha
    * GET
    * return array/object
    */
    ApiProvider.prototype.filtaLatLong = function (latlong) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = _this.API_URL + 'filtalatlong/' + latlong;
            _this.http.get(url)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    /*
    * checa se CPF já está cadastrado ou não
    * GET
    * return boolean
    */
    ApiProvider.prototype.checacpf = function (cpf) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = _this.API_URL + 'checacpfexiste/' + cpf;
            _this.http.get(url)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    /*
    * checa se EMAIL já está cadastrado ou não
    * GET
    * return boolean
    */
    ApiProvider.prototype.checaemail = function (email) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = _this.API_URL + 'checaemailexiste/' + email;
            _this.http.get(url)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    /*
    * recupera a senha do usuário
    * POST
    * return object
    */
    ApiProvider.prototype.recuperarSenha = function (email, senha) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var data = { email: email, senha: senha };
            _this.http.post(_this.API_URL + 'email/recuperasenha', data)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    /*
    * grava meio de pagamento (dinheiro ou cartão)
    * POST
    * return object
    */
    ApiProvider.prototype.padraoMeioPagamento = function (tipo, id_usuario) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var data = { tipo: tipo, id_usuario: id_usuario };
            _this.http.post(_this.API_URL + 'meiopagamento', data)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    /*
    * grava endereço padrão
    * POST
    * return object
    */
    ApiProvider.prototype.padraoEndereco = function (id_endereco, id_usuario) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var data = { id_endereco: id_endereco, id_usuario: id_usuario };
            _this.http.post(_this.API_URL + 'enderecopadrao', data)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    /*
    * grava veículo padrão
    * POST
    * return object
    */
    ApiProvider.prototype.padraoVeiculo = function (id_veiculo, id_usuario) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var data = { id_veiculo: id_veiculo, id_usuario: id_usuario };
            _this.http.post(_this.API_URL + 'veiculopadrao', data)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    /*
    * atualiza cartão de crédito padrão
    * POST
    * return object
    */
    ApiProvider.prototype.cartaoPadrao = function (id_cartao, id_usuario) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var data = { id_cartao: id_cartao, id_usuario: id_usuario };
            _this.http.post(_this.API_URL + 'cartaopadrao', data)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    /*
    * atualiza dados cadastrais dos usuários
    * POST
    * return array
    */
    ApiProvider.prototype.gravaEnderecos = function (id, dados) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var data = { id: id, dados: dados };
            _this.http.post(_this.API_URL + 'gravaenderecos', data)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    /*
    * cadastra novo cartão de crédito
    * POST
    * return object
    * id = id do usuário
    */
    ApiProvider.prototype.novoCartaoCredito = function (id, dados) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var data = { id: id, dados: dados };
            _this.http.post(_this.API_URL + 'novocartaocredito', data)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    /*
    * cadastra novo veículo
    * POST
    * return object
    * id = id do usuário
    */
    ApiProvider.prototype.novoVeiculo = function (id, dados) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var data = { id: id, dados: dados };
            _this.http.post(_this.API_URL + 'novoveiculo', data)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    /*
    * recupera dados cadastrais dos usuários
    * GET
    * return object
    */
    ApiProvider.prototype.getMeusVeiculos = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = _this.API_URL + 'meusveiculos/' + id;
            _this.http.get(url)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    /*
    * recupera dados cadastrais dos usuários
    * GET
    * return object
    */
    ApiProvider.prototype.getMeioPagamento = function (id_usuario) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = _this.API_URL + 'meiodepagamento/' + id_usuario;
            _this.http.get(url)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    /*
    * recupera dados cadastrais dos usuários
    * GET
    * return array/object
    */
    ApiProvider.prototype.getMeusEnderecos = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = _this.API_URL + 'meusenderecos/' + id;
            _this.http.get(url)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    /*
    * recupera todos os cartões do usuário
    * GET
    * return array
    */
    ApiProvider.prototype.getMeusCartoes = function (id_usuario) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = _this.API_URL + 'meuscartoes/' + id_usuario;
            _this.http.get(url)
                .subscribe(function (result) {
                resolve(result.json());
            }, function (error) {
                reject(error.json());
            });
        });
    };
    ApiProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http])
    ], ApiProvider);
    return ApiProvider;
}());
export { ApiProvider };
//# sourceMappingURL=api.js.map