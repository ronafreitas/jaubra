var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import firebase from 'firebase';
import { BackgeolocProvider } from '../providers/backgeoloc/backgeoloc';
import { CadastroProvider } from './../providers/cadastro/cadastro';
var MyApp = /** @class */ (function () {
    function MyApp(platform, menuCtrl, statusBar, alertCtrl, bkgProv, cadastroProvider, splashScreen) {
        //localStorage.clear(); 
        this.platform = platform;
        this.menuCtrl = menuCtrl;
        this.statusBar = statusBar;
        this.alertCtrl = alertCtrl;
        this.bkgProv = bkgProv;
        this.cadastroProvider = cadastroProvider;
        this.splashScreen = splashScreen;
        this.initializeApp();
        if (localStorage.getItem('dados_usuario')) {
            if (JSON.parse(localStorage.getItem('dados_usuario')).perfil == 'mot') {
                this.rootPage = 'HomeMotoristaPage';
            }
            else {
                this.rootPage = 'HomePassageiroPage';
            }
        }
        else {
            this.rootPage = 'LoginPage';
        }
        //this.rootPage = 'LoginPage';
    }
    MyApp.prototype.sair = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Sair do aplicativo',
            message: 'Deseja mesmo sair?',
            buttons: [
                { text: 'Cancelar', role: 'cancel', handler: function () { } },
                { text: 'Sair',
                    handler: function () {
                        var meuId = JSON.parse(localStorage.getItem('dados_usuario')).id;
                        _this.cadastroProvider.logout(meuId)
                            .then(function (result) {
                            if (result.error) {
                                //alert('Ocorreu um erro, tente novamente');
                            }
                            else {
                                var cidade_esta = JSON.parse(localStorage.getItem('cidade_esta'));
                                if (JSON.parse(localStorage.getItem('dados_usuario')).perfil == 'mot') {
                                    firebase.database().ref('motoristas/' + cidade_esta + "/" + meuId).remove();
                                    firebase.database().ref('chamadas/motoristas/' + cidade_esta + "/" + meuId).remove();
                                    firebase.database().ref('usuarios_fcm/' + meuId).remove();
                                    localStorage.clear();
                                    _this.bkgProv.pararBackgroundGeo();
                                    _this.nav.push('LoginPage');
                                }
                                else {
                                    firebase.database().ref('chamadas/passageiro/' + meuId).remove();
                                    firebase.database().ref('usuarios_fcm/' + meuId).remove();
                                    localStorage.clear();
                                    _this.nav.push('LoginPage');
                                }
                            }
                        })
                            .catch(function (error) {
                            console.log('error1', error);
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        if (page == 'DadosPas') {
            this.nav.push("DadosCadastraisPassageiroPage");
        }
        else if (page == 'DadosMot') {
            this.nav.push("DadosCadastraisMotoristaPage");
        }
        else if (page == 'Veiculos') {
            this.nav.push("VeiculosPage");
        }
        else if (page == 'Enderecos') {
            this.nav.push("EnderecosPage");
        }
        else if (page == 'Pagamentos') {
            this.nav.push("PagamentosPage");
        }
        else if (page == 'Sair') {
            this.nav.push("SairPage");
        }
    };
    __decorate([
        ViewChild(Nav),
        __metadata("design:type", Nav)
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Component({
            templateUrl: 'app.html'
        }),
        __metadata("design:paramtypes", [Platform,
            MenuController,
            StatusBar,
            AlertController,
            BackgeolocProvider,
            CadastroProvider,
            SplashScreen])
    ], MyApp);
    return MyApp;
}());
export { MyApp };
//# sourceMappingURL=app.component.js.map