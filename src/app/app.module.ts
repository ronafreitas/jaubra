import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import * as firebase from 'firebase';
import { CadastroProvider } from '../providers/cadastro/cadastro';
import { HttpModule } from '@angular/http';
import { BackgroundGeolocation } from "@ionic-native/background-geolocation";
import { ApiProvider } from '../providers/api/api';
import { ValidaCpfProvider } from '../providers/valida-cpf/valida-cpf';
import { BackgeolocProvider } from '../providers/backgeoloc/backgeoloc';
import { Push } from '@ionic-native/push';
export const config = {
  apiKey: "AIzaSyCo0Ip1NloOK1YQpRZvxZbC5zBFDIw8rSo",
  authDomain: "geojaubraapp.firebaseapp.com",
  databaseURL: "https://geojaubraapp.firebaseio.com",
  projectId: "geojaubraapp",
  storageBucket: "geojaubraapp.appspot.com",
  messagingSenderId: "632970989526"
};
firebase.initializeApp(config);

@NgModule({
  declarations: [MyApp],
  imports:[
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp],
  providers: [
    StatusBar,
    SplashScreen,
    Push,
    CadastroProvider,
    ApiProvider,
    Geolocation,
    Camera,
    File,
    FileTransfer,
    FileTransferObject,
    ValidaCpfProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    BackgroundGeolocation,
    BackgeolocProvider
  ]
})
export class AppModule {}
