import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController,AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import firebase from 'firebase';
import { BackgeolocProvider } from '../providers/backgeoloc/backgeoloc';
import { CadastroProvider } from './../providers/cadastro/cadastro';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any;
  constructor(public platform: Platform, 
    public menuCtrl: MenuController, 
    public statusBar: StatusBar, 
    private alertCtrl: AlertController,
    private bkgProv: BackgeolocProvider,
    private cadastroProvider: CadastroProvider,
    public splashScreen: SplashScreen){

      //localStorage.clear(); 
 
      this.initializeApp();
      if( localStorage.getItem('dados_usuario') ){
        if(JSON.parse(localStorage.getItem('dados_usuario')).perfil == 'mot'){
          this.rootPage = 'HomeMotoristaPage';
        }else{
          this.rootPage = 'HomePassageiroPage';
        }
      }else{
        this.rootPage = 'LoginPage';
      }

      //this.rootPage = 'LoginPage';
  }

  sair(){
    let alert = this.alertCtrl.create({
      title: 'Sair do aplicativo',
      message: 'Deseja mesmo sair?',
      buttons: [
      {text: 'Cancelar',role: 'cancel',handler: () => {}},
      {text: 'Sair',
        handler: () => {
          var meuId = JSON.parse(localStorage.getItem('dados_usuario')).id;
          this.cadastroProvider.logout(meuId)
          .then((result: any) => {
            if(result.error){
              //alert('Ocorreu um erro, tente novamente');
            }else{
              var cidade_esta = JSON.parse(localStorage.getItem('cidade_esta'));
              if(JSON.parse(localStorage.getItem('dados_usuario')).perfil == 'mot'){
                firebase.database().ref('motoristas/'+cidade_esta+"/"+meuId).remove();
                firebase.database().ref('chamadas/motoristas/'+cidade_esta+"/"+meuId).remove();
                firebase.database().ref('usuarios_fcm/'+meuId).remove();
                localStorage.clear();
                this.bkgProv.pararBackgroundGeo();
                this.nav.push('LoginPage');
              }else{
                firebase.database().ref('chamadas/passageiro/'+meuId).remove();
                firebase.database().ref('usuarios_fcm/'+meuId).remove();
                localStorage.clear();
                this.nav.push('LoginPage');
              }
            }
          })
          .catch((error: any) => {
            console.log('error1',error);
          });
        }
      }
      ]
    });
    alert.present();
  }

  initializeApp(){
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page){
    if(page == 'DadosPas'){
      this.nav.push("DadosCadastraisPassageiroPage");
    }else if(page == 'DadosMot'){
      this.nav.push("DadosCadastraisMotoristaPage");
    }else if(page == 'Veiculos'){
      this.nav.push("VeiculosPage");
    }else if(page == 'Enderecos'){
      this.nav.push("EnderecosPage");
    }else if(page == 'Pagamentos'){
      this.nav.push("PagamentosPage");
    }else if(page == 'Sair'){
      this.nav.push("SairPage");
    }
  }

}
