var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, NgZone } from '@angular/core';
import { IonicPage, NavParams, ViewController, AlertController, LoadingController, ToastController } from 'ionic-angular';
var ModalMeuEnderecoPage = /** @class */ (function () {
    function ModalMeuEnderecoPage(zone, alertCtrl, loadingCtrl, toastCtrl, navParams, viewCtrl) {
        this.zone = zone;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.service = new google.maps.places.AutocompleteService();
        this.defaulend = '';
        this.toast_end = null;
        this.alterado = false;
        this.apagado = false;
        this.address = {
            place: ''
        };
        this.autocompleteItems = [];
        this.autocomplete = {
            query: ''
        };
        this.mesloc = [];
        this.listmesloc = [];
    }
    ModalMeuEnderecoPage.prototype.ionViewDidLoad = function () {
        this.mesloc = JSON.parse(localStorage.getItem('meus_locais'));
        this.listaEnderecos(this.mesloc);
    };
    ModalMeuEnderecoPage.prototype.listaEnderecos = function (locais) {
        this.listmesloc = [];
        for (var l in locais) {
            this.listmesloc.push(locais[l]);
        }
        var ts = this;
        setTimeout(function () {
            for (var l in locais) {
                if (locais[l].padrao == true) {
                    ts.alterado = true;
                    ts.apagado = true;
                    ts.defaulend = locais[l].placeId;
                    ts.localpadrao = locais[l];
                    ts.key_local = l;
                }
            }
        }, 100);
    };
    ModalMeuEnderecoPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ModalMeuEnderecoPage.prototype.selecionaEndereco = function (item) {
        var ts = this;
        var address = item.descricao;
        var detalhes = item.detalhes;
        var geocoder = new google.maps.Geocoder();
        var lod = ts.loadingCtrl.create({
            content: 'Aguarde...'
        });
        lod.present();
        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                lod.dismiss();
                ts.alert = ts.alertCtrl.create({
                    title: 'Salvar novo local',
                    message: item.descricao,
                    inputs: [
                        {
                            name: 'nome',
                            placeholder: 'Nome desse local: Casa, Trabalho...'
                        }
                    ],
                    buttons: [
                        {
                            text: 'Salvar em meus locais',
                            handler: function (data) {
                                ts.adicionarLista(data, item, results);
                            }
                        },
                        {
                            text: 'Usar apenas essa vez',
                            handler: function () {
                                var endereconovo = [];
                                endereconovo.push({
                                    nome: item.descricao,
                                    endereco: item.descricao,
                                    placeId: item.placeId,
                                    detalhes: detalhes,
                                    lat: results[0].geometry.location.lat(),
                                    lng: results[0].geometry.location.lng(),
                                    padrao: false
                                });
                                ts.viewCtrl.dismiss(endereconovo[0]);
                            }
                        },
                    ],
                    enableBackdropDismiss: true
                });
                ts.alert.present();
                ts.alert.onDidDismiss(function (result) {
                    if (result == "") {
                        //ts.viewCtrl.dismiss();
                    }
                });
            }
            else {
                var alert_1 = this.alertCtrl.create({
                    title: 'Não foi possível gravar esse endereço, tente novamente.',
                    buttons: ['Ok']
                });
                alert_1.present();
                console.log('erro');
            }
        });
    };
    ModalMeuEnderecoPage.prototype.confirmar = function () {
        var locais = this.mesloc;
        var endsend;
        for (var l in locais) {
            if (locais[l].padrao == true) {
                endsend = locais[l];
                localStorage.setItem('meu_local_padrao', JSON.stringify(locais[l]));
            }
        }
        this.viewCtrl.dismiss(endsend);
    };
    ModalMeuEnderecoPage.prototype.apagar = function (local) {
        if (local) {
            var ts_1 = this;
            if (ts_1.toast_end) {
                ts_1.toast_end.dismiss();
            }
            if (ts_1.toastlocal) {
                ts_1.toastlocal.dismiss();
            }
            var alert_2 = this.alertCtrl.create({
                title: 'Apagar este local?',
                message: local.nome + ': ' + local.endereco,
                buttons: [
                    {
                        text: 'Não',
                        role: 'cancel',
                        handler: function () { }
                    },
                    {
                        text: 'Sim',
                        handler: function () {
                            var novolocais = [];
                            ts_1.mesloc = JSON.parse(localStorage.getItem('meus_locais'));
                            for (var x in ts_1.mesloc) {
                                if (x == ts_1.key_local) {
                                    //console.log('ende selec',ts.mesloc[x]);
                                }
                                else {
                                    novolocais.push(ts_1.mesloc[x]);
                                }
                            }
                            localStorage.removeItem('meus_locais');
                            localStorage.setItem('meus_locais', JSON.stringify(novolocais));
                            ts_1.listaEnderecos(novolocais);
                            var count = 0;
                            for (var k in novolocais) {
                                //console.log(novolocais[k]);
                                count++;
                                //if(novolocais.hasOwnProperty(k)) count++;
                            }
                            local = null;
                            ts_1.localpadrao = null;
                            if (count == 0) {
                                ts_1.apagado = false;
                                ts_1.alterado = false;
                            }
                            ts_1.mesloc = novolocais;
                        }
                    }
                ]
            });
            alert_2.present();
        }
        else {
            var alert_3 = this.alertCtrl.create({
                title: 'Não foi possível excluir',
                subTitle: 'Necessário selecionar um local',
                buttons: ['Ok']
            });
            alert_3.present();
        }
    };
    ModalMeuEnderecoPage.prototype.adicionarLista = function (data, item, results) {
        var enderecofinal = [];
        var ts = this;
        //var address = item.descricao;
        var detalhes = item.detalhes;
        if (data.nome != "") {
            /*var count = 0;
            for(var k in ts.mesloc){
              if(ts.mesloc.hasOwnProperty(k)) count++;
            }
            var pdr;
            console.log('count',count);
            if(count == 0){
                pdr = true;
                ts.alterado=true;
            }else{
                pdr = false;
            }*/
            enderecofinal.push({
                nome: data.nome,
                endereco: item.descricao,
                placeId: item.placeId,
                detalhes: detalhes,
                lat: results[0].geometry.location.lat(),
                lng: results[0].geometry.location.lng(),
                padrao: false
                //padrao:pdr
            });
            var loading = ts.loadingCtrl.create({
                content: 'Aguarde...'
            });
            loading.present();
            ts.mesloc.push(enderecofinal[0]);
            localStorage.setItem('meus_locais', JSON.stringify(ts.mesloc));
            ts.autocomplete.query = '';
            loading.dismiss();
            ts.autocompleteItems = [];
            ts.listaEnderecos(ts.mesloc);
            /*//if(pdr){
                setTimeout(function(){
                    ts.toastlocal = ts.toastCtrl.create({
                        message: 'Local padrão gravado. Escolha SELELCIONAR acima para utilizar este endereço na chamada',
                        duration: 6500,
                        position: 'middle',
                        dismissOnPageChange: true,
                        showCloseButton:true,
                        closeButtonText:'ok'
                    });
                    ts.toastlocal.present();
                }, 600);
            }*/
            setTimeout(function () {
                ts.toastlocal = ts.toastCtrl.create({
                    message: 'Local salvo. Clique sobre ele para tornar padrão',
                    duration: 3500,
                    position: 'middle',
                    dismissOnPageChange: true,
                    showCloseButton: true,
                    closeButtonText: 'ok'
                });
                ts.toastlocal.present();
            }, {
                setTimeout: function () { }, function: function () {
                    document.getElementById("alert-input-0-0").focus();
                }, 300: 
            });
            return false;
        }
    };
    ModalMeuEnderecoPage.prototype.buscaEndereco = function () {
        if (this.autocomplete.query == '') {
            this.autocompleteItems = [];
            return;
        }
        var me = this;
        this.service.getPlacePredictions({ input: this.autocomplete.query, componentRestrictions: { country: 'BR' } }, function (predictions, status) {
            me.autocompleteItems = [];
            me.zone.run(function () {
                if (status == "OK") {
                    predictions.forEach(function (prediction) {
                        me.autocompleteItems.push({ descricao: prediction.description, detalhes: prediction.terms, placeId: prediction.place_id });
                    });
                }
            });
        });
    };
    ModalMeuEnderecoPage.prototype.selecionarPadrao = function (placeId) {
        var locais = this.mesloc;
        this.mesloc = [];
        for (var l in locais) {
            if (locais[l].placeId == placeId) {
                locais[l].padrao = true;
                this.localpadrao = locais[l];
                this.mesloc.push(locais[l]);
                this.key_local = l;
            }
            else {
                locais[l].padrao = false;
                this.mesloc.push(locais[l]);
            }
        }
        if (this.toast_end) {
            this.toast_end.dismiss();
        }
        if (this.toastlocal) {
            this.toastlocal.dismiss();
        }
        localStorage.setItem('meus_locais', JSON.stringify(this.mesloc));
        this.toast_end = this.toastCtrl.create({
            message: 'Local padrão gravado. Escolha SELELCIONAR acima para utilizar este endereço na chamada',
            duration: 6500,
            position: 'middle',
            dismissOnPageChange: true,
            showCloseButton: true,
            closeButtonText: 'ok'
        });
        this.toast_end.present();
        this.alterado = true;
        this.apagado = true;
    };
    ModalMeuEnderecoPage.prototype.ionViewWillLeave = function () {
        if (this.toast_end) {
            this.toast_end.dismiss();
        }
    };
    ModalMeuEnderecoPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-modal-meu-endereco',
            templateUrl: 'modal-meu-endereco.html',
        }),
        __metadata("design:paramtypes", [NgZone,
            AlertController,
            LoadingController,
            ToastController,
            NavParams,
            ViewController])
    ], ModalMeuEnderecoPage);
    return ModalMeuEnderecoPage;
}());
export { ModalMeuEnderecoPage };
//# sourceMappingURL=modal-meu-endereco.js.map