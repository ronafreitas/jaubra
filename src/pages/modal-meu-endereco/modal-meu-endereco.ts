import { Component,NgZone } from '@angular/core';
import { IonicPage, NavParams,ViewController,AlertController,LoadingController,ToastController } from 'ionic-angular';
declare let google;
@IonicPage()
@Component({
  selector: 'page-modal-meu-endereco',
  templateUrl: 'modal-meu-endereco.html',
})
export class ModalMeuEnderecoPage {
	autocompleteItems;
	autocomplete;
	service = new google.maps.places.AutocompleteService();
	address:any;
	alert:any;
	mesloc:any;
	listmesloc:any;
	defaulend:any='';
	toast_end:any=null;
	toastlocal:any;
	alterado:boolean=false;
	localpadrao:any;
	key_local:any;
	apagado:boolean=false;
	constructor(
		private zone: NgZone,
		private alertCtrl: AlertController,
		public loadingCtrl: LoadingController,
		private toastCtrl: ToastController,
		public navParams: NavParams,
		public viewCtrl: ViewController)
	{
		this.address = {
			place: ''
		};

		this.autocompleteItems = [];
		this.autocomplete = {
			query: ''
		};
		this.mesloc =[]
		this.listmesloc =[]
	}

	ionViewDidLoad(){
		this.mesloc = JSON.parse(localStorage.getItem('meus_locais'));
		this.listaEnderecos(this.mesloc);
	}

	listaEnderecos(locais){
		this.listmesloc = [];
		for(var l in locais){
			this.listmesloc.push(locais[l]);
		}
		var ts = this;
		setTimeout(function(){
			for(var l in locais){
				if(locais[l].padrao==true){
					ts.alterado=true;
					ts.apagado=true;
					ts.defaulend=locais[l].placeId;
					ts.localpadrao = locais[l];
					ts.key_local = l;
				}
			}
		}, 100);
	}

	dismiss(){
		this.viewCtrl.dismiss();
	}

	selecionaEndereco(item: any){
		var ts = this;
		var address = item.descricao;
		var detalhes = item.detalhes;
		var geocoder = new google.maps.Geocoder();
		
		let lod = ts.loadingCtrl.create({
			content: 'Aguarde...'
		});
		lod.present();
		geocoder.geocode( { 'address': address}, function(results, status){
			if(status == google.maps.GeocoderStatus.OK){
				lod.dismiss();
				ts.alert = ts.alertCtrl.create({
					title: 'Salvar novo local',
					message: item.descricao,
				    inputs: [
				      {
				        name: 'nome',
				        placeholder: 'Nome desse local: Casa, Trabalho...'
				      }
				    ],					
					buttons: [
						{
							text: 'Salvar em meus locais',
							handler: data => {
								ts.adicionarLista(data,item,results);
							}
						},
						{
							text: 'Usar apenas essa vez',
							handler: () => {
								var endereconovo = [];
								endereconovo.push(
									{
										nome:item.descricao,
										endereco:item.descricao, 
										placeId: item.placeId, 
										detalhes:detalhes, 
										lat:results[0].geometry.location.lat(), 
										lng:results[0].geometry.location.lng(),
										padrao:false
									}
								);
						  		ts.viewCtrl.dismiss(endereconovo[0]);
							}
						},
						/*{
							text: 'Cancelar',
							role: 'cancel',
							handler: () => {
								
							}
						},*/
					],
					enableBackdropDismiss: true
				});
				ts.alert.present();
				ts.alert.onDidDismiss((result) =>{
					if(result == ""){
						//ts.viewCtrl.dismiss();
					}
				});
			}else{
				let alert = this.alertCtrl.create({
					title: 'Não foi possível gravar esse endereço, tente novamente.',
					buttons: ['Ok']
				});
				alert.present();	
			  	console.log('erro');
			}
		});
	}

	confirmar(){
		var locais = this.mesloc;
		var endsend;
		for(var l in locais){
			if(locais[l].padrao==true){
				endsend=locais[l];
				localStorage.setItem('meu_local_padrao', JSON.stringify(locais[l]));
			}
		}
		this.viewCtrl.dismiss(endsend);
	}

	apagar(local){
		if(local){
			let ts = this;
			if( ts.toast_end ){
				ts.toast_end.dismiss();
			}
			if( ts.toastlocal ){
				ts.toastlocal.dismiss();
			}
			let alert = this.alertCtrl.create({
				title: 'Apagar este local?',
				message: local.nome+': '+local.endereco,
				buttons: [
					{
						text: 'Não',
						role: 'cancel',
						handler: () => {}
					},
					{
						text: 'Sim',
						handler: () => {
							var novolocais = []
							ts.mesloc = JSON.parse(localStorage.getItem('meus_locais'));
							for(var x in ts.mesloc){
								if(x == ts.key_local){
									//console.log('ende selec',ts.mesloc[x]);
								}else{
									novolocais.push(ts.mesloc[x]);
								}
							}
							localStorage.removeItem('meus_locais');
							localStorage.setItem('meus_locais', JSON.stringify(novolocais));
							ts.listaEnderecos(novolocais);
							var count=0;
							for(var k in novolocais){ 
								//console.log(novolocais[k]);
								count++;
								//if(novolocais.hasOwnProperty(k)) count++;
							}
							local = null;
							ts.localpadrao=null;
							if(count == 0){
								ts.apagado=false;
								ts.alterado=false;
							}
							ts.mesloc = novolocais
						}
					}
				]
			});
			alert.present();
		}else{
			let alert = this.alertCtrl.create({
				title: 'Não foi possível excluir',
				subTitle: 'Necessário selecionar um local',
				buttons: ['Ok']
			});
			alert.present();
		}
	}
	
	adicionarLista(data,item,results){
		var enderecofinal = [];
		let ts = this;
		//var address = item.descricao;
		var detalhes = item.detalhes;

		if(data.nome != ""){
			/*var count = 0;
			for(var k in ts.mesloc){
			  if(ts.mesloc.hasOwnProperty(k)) count++;
			}
			var pdr;
			console.log('count',count);
			if(count == 0){
				pdr = true;
				ts.alterado=true;
			}else{
				pdr = false;
			}*/

			enderecofinal.push(
				{
					nome:data.nome,
					endereco:item.descricao, 
					placeId: item.placeId, 
					detalhes:detalhes, 
					lat:results[0].geometry.location.lat(), 
					lng:results[0].geometry.location.lng(),
					padrao:false
					//padrao:pdr
				}
			);
			
			let loading = ts.loadingCtrl.create({
				content: 'Aguarde...'
			});
			loading.present();
			ts.mesloc.push(enderecofinal[0]);
			localStorage.setItem('meus_locais', JSON.stringify(ts.mesloc));
			ts.autocomplete.query=''
			loading.dismiss();
			ts.autocompleteItems = [];
			ts.listaEnderecos(ts.mesloc);
			/*//if(pdr){
				setTimeout(function(){
					ts.toastlocal = ts.toastCtrl.create({
						message: 'Local padrão gravado. Escolha SELELCIONAR acima para utilizar este endereço na chamada',
						duration: 6500,
						position: 'middle',
						dismissOnPageChange: true,
						showCloseButton:true,
						closeButtonText:'ok'
					});
					ts.toastlocal.present();
				}, 600);
			}*/
			setTimeout(function(){
				ts.toastlocal = ts.toastCtrl.create({
					message: 'Local salvo. Clique sobre ele para tornar padrão',
					duration: 3500,
					position: 'middle',
					dismissOnPageChange: true,
					showCloseButton:true,
					closeButtonText:'ok'
				});
				ts.toastlocal.present();
			}, 700);
		}else{
			setTimeout(function(){
				document.getElementById("alert-input-0-0").focus();
			}, 300);
			return false;
		}
	}

	buscaEndereco(){
		
		if(this.autocomplete.query == ''){
		  this.autocompleteItems = [];
		  return;
		}
		let me = this;
		this.service.getPlacePredictions({ input: this.autocomplete.query, componentRestrictions: {country: 'BR'} }, (predictions, status) => {
		  me.autocompleteItems = []; 
		  me.zone.run( () => {
		  	if(status == "OK"){
			    predictions.forEach( (prediction) => {
			    	me.autocompleteItems.push({descricao:prediction.description, detalhes:prediction.terms, placeId:prediction.place_id});
			    });
		    }
		  });
		});
	}

	selecionarPadrao(placeId){
		var locais  = this.mesloc;
		this.mesloc = [];
		for(var l in locais){
			if(locais[l].placeId == placeId){
				locais[l].padrao=true;
				this.localpadrao = locais[l];
				this.mesloc.push(locais[l]);
				this.key_local = l;
			}else{
				locais[l].padrao=false;
				this.mesloc.push(locais[l]);
			}	
		}
		if( this.toast_end ){
			this.toast_end.dismiss();
		}
		if( this.toastlocal ){
			this.toastlocal.dismiss(); 
		}
		localStorage.setItem('meus_locais', JSON.stringify(this.mesloc));
		this.toast_end = this.toastCtrl.create({
			message: 'Local padrão gravado. Escolha SELELCIONAR acima para utilizar este endereço na chamada',
			duration: 6500,
			position: 'middle',
			dismissOnPageChange: true,
			showCloseButton:true,
			closeButtonText:'ok'
		});
		this.toast_end.present();
		this.alterado=true;
		this.apagado=true;
	}

	ionViewWillLeave(){
		if( this.toast_end ){
			this.toast_end.dismiss();
		}	
	}
}
