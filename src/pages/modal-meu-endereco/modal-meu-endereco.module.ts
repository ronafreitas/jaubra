import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalMeuEnderecoPage } from './modal-meu-endereco';

@NgModule({
  declarations: [
    ModalMeuEnderecoPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalMeuEnderecoPage),
  ],
})
export class ModalMeuEnderecoPageModule {}
