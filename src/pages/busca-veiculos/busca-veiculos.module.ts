import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuscaVeiculosPage } from './busca-veiculos';

@NgModule({
  declarations: [
    BuscaVeiculosPage,
  ],
  imports: [
    IonicPageModule.forChild(BuscaVeiculosPage),
  ],
})
export class BuscaVeiculosPageModule {}
