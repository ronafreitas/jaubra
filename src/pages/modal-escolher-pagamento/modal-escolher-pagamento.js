var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';
var ModalEscolherPagamentoPage = /** @class */ (function () {
    function ModalEscolherPagamentoPage(viewCtrl, navParams) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.escolha = 'dinheiro';
    }
    ModalEscolherPagamentoPage.prototype.ionViewDidLoad = function () {
        this.preco = this.navParams.data.data.preco;
        this.duracao = this.navParams.data.data.duracao;
        this.distancia = this.navParams.data.data.distancia;
    };
    ModalEscolherPagamentoPage.prototype.voltar = function () {
        this.viewCtrl.dismiss();
    };
    ModalEscolherPagamentoPage.prototype.confirmar = function () {
        this.viewCtrl.dismiss({ busca: 'ok', escolha: this.escolha, dados: this.navParams.data.data });
    };
    ModalEscolherPagamentoPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-modal-escolher-pagamento',
            templateUrl: 'modal-escolher-pagamento.html',
        }),
        __metadata("design:paramtypes", [ViewController, NavParams])
    ], ModalEscolherPagamentoPage);
    return ModalEscolherPagamentoPage;
}());
export { ModalEscolherPagamentoPage };
//# sourceMappingURL=modal-escolher-pagamento.js.map