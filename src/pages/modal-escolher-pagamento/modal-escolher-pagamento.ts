import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-modal-escolher-pagamento',
  templateUrl: 'modal-escolher-pagamento.html',
})
export class ModalEscolherPagamentoPage {
	preco:any;
	duracao:any;
	distancia:any;
	tipo:any;
	escolha:string='dinheiro';
	constructor(public viewCtrl: ViewController, public navParams: NavParams){}

	ionViewDidLoad() {
		this.preco = this.navParams.data.data.preco;
		this.duracao = this.navParams.data.data.duracao;
		this.distancia = this.navParams.data.data.distancia;
	}

	voltar(){
		this.viewCtrl.dismiss();
	}

	confirmar(){
		this.viewCtrl.dismiss({busca:'ok', escolha:this.escolha, dados: this.navParams.data.data});
	}
}
