import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalEscolherPagamentoPage } from './modal-escolher-pagamento';

@NgModule({
  declarations: [
    ModalEscolherPagamentoPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalEscolherPagamentoPage),
  ],
})
export class ModalEscolherPagamentoPageModule {}
