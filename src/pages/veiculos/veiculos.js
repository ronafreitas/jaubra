var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, AlertController, ToastController } from 'ionic-angular';
import { ApiProvider } from './../../providers/api/api';
var VeiculosPage = /** @class */ (function () {
    function VeiculosPage(navCtrl, alertCtrl, apiProvider, modalCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.apiProvider = apiProvider;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.veiculos = [];
        this.totalvei = 0;
        this.padraoveic = '';
    }
    VeiculosPage.prototype.meusVeiculos = function () {
        var ts = this;
        this.apiProvider.getMeusVeiculos(parseInt(JSON.parse(localStorage.getItem('dados_usuario')).id))
            .then(function (result) {
            ts.veiculos = [];
            result.forEach(function (re) {
                ts.totalvei++;
                if (re.padrao == 1) {
                    ts.padraoveic = re.id;
                }
                ts.veiculos.push(re);
            });
        })
            .catch(function (error) {
            console.log('error1', error);
        });
    };
    VeiculosPage.prototype.ionViewDidEnter = function () {
        this.meusVeiculos();
        /*if(JSON.parse(localStorage.getItem('dados_usuario') ).perfil == 'pas'){
            this.menuCtrl.enable(true, 'pas');
            this.menuCtrl.enable(false, 'mot');
        }else{
            this.menuCtrl.enable(false, 'pas');
            this.menuCtrl.enable(true, 'mot');
        }*/
    };
    VeiculosPage.prototype.addVeiculo = function () {
        var _this = this;
        if (this.totalvei == 2) {
            var alert_1 = this.alertCtrl.create({
                title: 'Só é permitido adicionar 2 veículos',
                buttons: ['Ok']
            });
            alert_1.present();
        }
        else {
            //this.navCtrl.push('NovoVeiculoPage');
            var nven = this.modalCtrl.create('ModalNovoVeiculoPage');
            nven.present();
            nven.onDidDismiss(function (result) {
                if (result) {
                    //console.log(result);
                    if (result.confirmado == 'ok') {
                        _this.meusVeiculos();
                    }
                }
            });
        }
    };
    VeiculosPage.prototype.salvar = function (carro) {
        console.log(carro);
    };
    VeiculosPage.prototype.selecionarPadrao = function (vecl) {
        var _this = this;
        this.apiProvider.padraoVeiculo(vecl, parseInt(JSON.parse(localStorage.getItem('dados_usuario')).id))
            .then(function (result) {
            if (result.error) {
                var toast = _this.toastCtrl.create({
                    message: 'Não foi possível gravar veículo padrão',
                    duration: 3000,
                    position: 'bottom',
                    cssClass: "toasterro"
                });
                toast.present();
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: 'Veículo padrão confirmado',
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();
            }
        })
            .catch(function (error) {
            console.log('error1', error);
        });
    };
    VeiculosPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-veiculos',
            templateUrl: 'veiculos.html',
        }),
        __metadata("design:paramtypes", [NavController,
            AlertController,
            ApiProvider,
            ModalController,
            ToastController])
    ], VeiculosPage);
    return VeiculosPage;
}());
export { VeiculosPage };
//# sourceMappingURL=veiculos.js.map