import { Component } from '@angular/core';
import { IonicPage, ModalController,NavController,AlertController,ToastController } from 'ionic-angular';
import { ApiProvider } from './../../providers/api/api';

@IonicPage()
@Component({
  selector: 'page-veiculos',
  templateUrl: 'veiculos.html',
})
export class VeiculosPage {

	veiculos:any=[];
	totalvei:number=0;
	padraoveic:any='';

	constructor(public navCtrl: NavController,
		private alertCtrl: AlertController,
		private apiProvider:ApiProvider,
		public modalCtrl: ModalController,
		private toastCtrl: ToastController
		) {
	
	}

	meusVeiculos(){
		var ts = this;
		this.apiProvider.getMeusVeiculos(parseInt(JSON.parse(localStorage.getItem('dados_usuario')).id))
		.then((result: any) => {
			ts.veiculos=[]
		    result.forEach( (re) => {
		    	ts.totalvei++;
		    	if(re.padrao == 1){
		    		ts.padraoveic = re.id
		    	}
		    	ts.veiculos.push(re);
		    });
		})
		.catch((error: any) => {
			console.log('error1',error);
		});
	}

	ionViewDidEnter(){
		this.meusVeiculos();
		/*if(JSON.parse(localStorage.getItem('dados_usuario') ).perfil == 'pas'){
			this.menuCtrl.enable(true, 'pas');
			this.menuCtrl.enable(false, 'mot');
		}else{
			this.menuCtrl.enable(false, 'pas');
			this.menuCtrl.enable(true, 'mot');
		}*/
	}

	addVeiculo(){
		if(this.totalvei == 2){
			let alert = this.alertCtrl.create({
				title: 'Só é permitido adicionar 2 veículos',
				buttons: ['Ok']
			});
			alert.present();
		}else{
			//this.navCtrl.push('NovoVeiculoPage');
			let nven = this.modalCtrl.create('ModalNovoVeiculoPage');
	    	nven.present();
			nven.onDidDismiss((result) =>{
				if(result){
					//console.log(result);
					if(result.confirmado == 'ok'){
						this.meusVeiculos();
					}
				}
			});
		}
	}

	salvar(carro){
		console.log(carro);
	}

	selecionarPadrao(vecl){
		this.apiProvider.padraoVeiculo(vecl, parseInt(JSON.parse(localStorage.getItem('dados_usuario')).id) )
		.then((result: any) => {
			if(result.error){
				let toast = this.toastCtrl.create({
					message: 'Não foi possível gravar veículo padrão',
					duration: 3000,
					position: 'bottom',
					cssClass: "toasterro"
				});
				toast.present();
			}else{
				let toast = this.toastCtrl.create({
					message: 'Veículo padrão confirmado',
					duration: 3000,
					position: 'bottom'
				});
				toast.present();
			}
		})
		.catch((error: any) => {
			console.log('error1',error);
		});
	}

}
