import { Component } from '@angular/core';
import { IonicPage, AlertController,ToastController } from 'ionic-angular';
import { ApiProvider } from './../../providers/api/api';

@IonicPage()
@Component({
  selector: 'page-dados-cadastrais-passageiro',
  templateUrl: 'dados-cadastrais-passageiro.html',
})
export class DadosCadastraisPassageiroPage {

	user:any={id:'',nome:'', sobrenome:'', celular:'',cpf:'', senha:'', perfil:''};
	passwordType: string = 'password';
 	passwordIcon: string = 'eye-off';
	constructor(private apiProvider:ApiProvider,private alertCtrl: AlertController, private toastCtrl: ToastController) {
	}

	ionViewDidLoad() {
		this.user.id = JSON.parse(localStorage.getItem('dados_usuario') ).id;
		this.user.nome = JSON.parse(localStorage.getItem('dados_usuario') ).nome;
		this.user.sobrenome = JSON.parse(localStorage.getItem('dados_usuario') ).sobrenome;
		this.user.celular = JSON.parse(localStorage.getItem('dados_usuario') ).celular;
		this.user.cpf = JSON.parse(localStorage.getItem('dados_usuario') ).cpf;
		this.user.senha = JSON.parse(localStorage.getItem('dados_usuario') ).senha;
		this.user.perfil = 'pas';
	}
	isReadonly(){
		return true;
	}

	hideShowPassword() {
		this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
		this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
	}

	salvar(user){
		if(
			user.nome == '' || 
			user.sobrenome == '' || 
			user.celular == '' || 
			user.cpf == '' || 
			user.senha == '' 
			){

			let alert = this.alertCtrl.create({
				title: 'Todos os campos são obrigatórios',
				buttons: ['Ok']
			});
			alert.present();
			
			return false

		}else{
			this.apiProvider.updateDadosCadastrais(user)
			.then((result: any) => {

				if(result.success){

					var dadosgravar={
						id: this.user.id,
						perfil: this.user.perfil,
						nome: user.nome,
						sobrenome: user.sobrenome,
						celular: user.celular,
						senha: user.senha,
						cpf: user.cpf,
					}
					localStorage.setItem('dados_usuario',JSON.stringify(dadosgravar));

					let toast = this.toastCtrl.create({
						message: 'Cadastro atualizado',
						duration: 3000,
						position: 'bottom'
					});
					toast.present();

				}else{
					let toast = this.toastCtrl.create({
						message: 'Não foi possível atualizar cadastro, tente novamente',
						duration: 3000,
						position: 'bottom'
					});
					toast.present();
				}
			})
			.catch((error: any) => {
				console.log('error1',error);
			});
	
		}
	}

}
