var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, AlertController, ToastController } from 'ionic-angular';
import { ApiProvider } from './../../providers/api/api';
var DadosCadastraisPassageiroPage = /** @class */ (function () {
    function DadosCadastraisPassageiroPage(apiProvider, alertCtrl, toastCtrl) {
        this.apiProvider = apiProvider;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.user = { id: '', nome: '', sobrenome: '', celular: '', cpf: '', senha: '', perfil: '' };
        this.passwordType = 'password';
        this.passwordIcon = 'eye-off';
    }
    DadosCadastraisPassageiroPage.prototype.ionViewDidLoad = function () {
        this.user.id = JSON.parse(localStorage.getItem('dados_usuario')).id;
        this.user.nome = JSON.parse(localStorage.getItem('dados_usuario')).nome;
        this.user.sobrenome = JSON.parse(localStorage.getItem('dados_usuario')).sobrenome;
        this.user.celular = JSON.parse(localStorage.getItem('dados_usuario')).celular;
        this.user.cpf = JSON.parse(localStorage.getItem('dados_usuario')).cpf;
        this.user.senha = JSON.parse(localStorage.getItem('dados_usuario')).senha;
        this.user.perfil = 'pas';
    };
    DadosCadastraisPassageiroPage.prototype.isReadonly = function () {
        return true;
    };
    DadosCadastraisPassageiroPage.prototype.hideShowPassword = function () {
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
        this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    };
    DadosCadastraisPassageiroPage.prototype.salvar = function (user) {
        var _this = this;
        if (user.nome == '' ||
            user.sobrenome == '' ||
            user.celular == '' ||
            user.cpf == '' ||
            user.senha == '') {
            var alert_1 = this.alertCtrl.create({
                title: 'Todos os campos são obrigatórios',
                buttons: ['Ok']
            });
            alert_1.present();
            return false;
        }
        else {
            this.apiProvider.updateDadosCadastrais(user)
                .then(function (result) {
                if (result.success) {
                    var dadosgravar = {
                        id: _this.user.id,
                        perfil: _this.user.perfil,
                        nome: user.nome,
                        sobrenome: user.sobrenome,
                        celular: user.celular,
                        senha: user.senha,
                        cpf: user.cpf,
                    };
                    localStorage.setItem('dados_usuario', JSON.stringify(dadosgravar));
                    var toast = _this.toastCtrl.create({
                        message: 'Cadastro atualizado',
                        duration: 3000,
                        position: 'bottom'
                    });
                    toast.present();
                }
                else {
                    var toast = _this.toastCtrl.create({
                        message: 'Não foi possível atualizar cadastro, tente novamente',
                        duration: 3000,
                        position: 'bottom'
                    });
                    toast.present();
                }
            })
                .catch(function (error) {
                console.log('error1', error);
            });
        }
    };
    DadosCadastraisPassageiroPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-dados-cadastrais-passageiro',
            templateUrl: 'dados-cadastrais-passageiro.html',
        }),
        __metadata("design:paramtypes", [ApiProvider, AlertController, ToastController])
    ], DadosCadastraisPassageiroPage);
    return DadosCadastraisPassageiroPage;
}());
export { DadosCadastraisPassageiroPage };
//# sourceMappingURL=dados-cadastrais-passageiro.js.map