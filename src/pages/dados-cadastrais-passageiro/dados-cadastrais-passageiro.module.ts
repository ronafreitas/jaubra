import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DadosCadastraisPassageiroPage } from './dados-cadastrais-passageiro';

@NgModule({
  declarations: [
    DadosCadastraisPassageiroPage,
  ],
  imports: [
    IonicPageModule.forChild(DadosCadastraisPassageiroPage),
  ],
})
export class DadosCadastraisPassageiroPageModule {}
