var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, AlertController, ToastController } from 'ionic-angular';
import { ApiProvider } from './../../providers/api/api';
//import { ModalNovoPagamentoPage } from '../../pages/modal-novo-pagamento/modal-novo-pagamento';
var PagamentosPage = /** @class */ (function () {
    function PagamentosPage(navCtrl, apiProvider, alertCtrl, modalCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.apiProvider = apiProvider;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.cartoes = [];
        this.totalpgmt = 0;
        this.tipopagamento = false;
    }
    /*
    MenuController
    public menuCtrl: MenuController

    ionViewDidEnter(){
        if(JSON.parse(localStorage.getItem('dados_usuario') ).perfil == 'pas'){
            this.menuCtrl.enable(true, 'pas');
            this.menuCtrl.enable(false, 'mot');
        }else{
            this.menuCtrl.enable(false, 'pas');
            this.menuCtrl.enable(true, 'mot');
        }
    }*/
    PagamentosPage.prototype.meuscartoes = function () {
        var _this = this;
        this.cartoes = [];
        this.totalpgmt = 0;
        this.tipopagamento = false;
        this.apiProvider.getMeusCartoes(parseInt(JSON.parse(localStorage.getItem('dados_usuario')).id))
            .then(function (result) {
            result.forEach(function (re) {
                _this.totalpgmt++;
                var id = re.id;
                var tipo = re.tipo;
                var numero = re.numero;
                var padrao = re.padrao;
                _this.cartoes.push({ id: id, padrao: padrao, tipo: tipo, numero: numero.substr(numero.length - 4) });
            });
        })
            .catch(function (error) {
            console.log('error1', error);
        });
    };
    PagamentosPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.meuscartoes();
        this.apiProvider.getMeioPagamento(parseInt(JSON.parse(localStorage.getItem('dados_usuario')).id))
            .then(function (result) {
            result.forEach(function (re) {
                _this.pagamento = re.tipo;
                if (re.tipo == "dinheiro") {
                    _this.tipopagamento = true;
                }
                else {
                    _this.tipopagamento = false;
                }
            });
        })
            .catch(function (error) {
            console.log('error1', error);
        });
    };
    PagamentosPage.prototype.addPagamento = function () {
        var _this = this;
        if (this.tipopagamento) {
            var alert_1 = this.alertCtrl.create({
                title: 'Escolha Cartão como meio de pagamento',
                buttons: ['Ok']
            });
            alert_1.present();
        }
        else {
            if (this.totalpgmt == 2) {
                var alert_2 = this.alertCtrl.create({
                    title: 'Só é permitido adicionar 2 cartões',
                    buttons: ['Ok']
                });
                alert_2.present();
            }
            else {
                //let psh = this.navCtrl.push('NovoPagamentoPage');
                var psh = this.modalCtrl.create('ModalNovoPagamentoPage');
                psh.present();
                psh.onDidDismiss(function (result) {
                    if (result) {
                        //console.log(result);
                        if (result.confirmado == 'ok') {
                            _this.meuscartoes();
                        }
                    }
                });
            }
        }
    };
    PagamentosPage.prototype.selecionarPadrao = function (pgtm) {
        var _this = this;
        this.apiProvider.padraoMeioPagamento(pgtm, parseInt(JSON.parse(localStorage.getItem('dados_usuario')).id))
            .then(function (result) {
            if (result.error) {
                var toast = _this.toastCtrl.create({
                    message: 'Não foi possível gravar meio de pagamento',
                    duration: 3000,
                    position: 'bottom',
                    cssClass: "toasterro"
                });
                toast.present();
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: 'Meio de pagamento padrão confirmado',
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();
                if (pgtm == 'dinheiro') {
                    _this.tipopagamento = true;
                }
                else {
                    _this.tipopagamento = false;
                }
            }
        })
            .catch(function (error) {
            console.log('error1', error);
        });
    };
    PagamentosPage.prototype.selecionarPadraoCartao = function (id_cartao) {
        var _this = this;
        this.apiProvider.cartaoPadrao(id_cartao, parseInt(JSON.parse(localStorage.getItem('dados_usuario')).id))
            .then(function (result) {
            if (result.error) {
                var toast = _this.toastCtrl.create({
                    message: 'Não foi possível selecionar o cartão',
                    duration: 2000,
                    position: 'bottom',
                    cssClass: "toasterro"
                });
                toast.present();
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: 'Cartão padrão selecionado',
                    duration: 2000,
                    position: 'bottom'
                });
                toast.present();
            }
        })
            .catch(function (error) {
            console.log('error1', error);
        });
    };
    PagamentosPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-pagamentos',
            templateUrl: 'pagamentos.html',
        }),
        __metadata("design:paramtypes", [NavController,
            ApiProvider,
            AlertController,
            ModalController,
            ToastController])
    ], PagamentosPage);
    return PagamentosPage;
}());
export { PagamentosPage };
//# sourceMappingURL=pagamentos.js.map