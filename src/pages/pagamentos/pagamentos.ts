import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController,AlertController,ToastController } from 'ionic-angular';
import { ApiProvider } from './../../providers/api/api';
//import { ModalNovoPagamentoPage } from '../../pages/modal-novo-pagamento/modal-novo-pagamento';

@IonicPage()
@Component({
  selector: 'page-pagamentos',
  templateUrl: 'pagamentos.html',
})
export class PagamentosPage {

	pagamento:string;
	cartao:string;
	cartoes:any=[];
	totalpgmt:number=0;
	tipopagamento:boolean=false;

	constructor(public navCtrl: NavController, 
		private apiProvider:ApiProvider, 
		private alertCtrl: AlertController, 
		public modalCtrl: ModalController,
		private toastCtrl: ToastController,
		) {

	}
 
	/*
	MenuController
	public menuCtrl: MenuController

	ionViewDidEnter(){
		if(JSON.parse(localStorage.getItem('dados_usuario') ).perfil == 'pas'){
			this.menuCtrl.enable(true, 'pas');
			this.menuCtrl.enable(false, 'mot');
		}else{
			this.menuCtrl.enable(false, 'pas');
			this.menuCtrl.enable(true, 'mot');
		}
	}*/

	meuscartoes(){
		this.cartoes=[]
		this.totalpgmt=0
		this.tipopagamento=false
		this.apiProvider.getMeusCartoes(parseInt(JSON.parse(localStorage.getItem('dados_usuario')).id))
		.then((result: any) => {
			result.forEach( (re) => {
				this.totalpgmt++;
				var id = re.id;
				var tipo = re.tipo;
				var numero = re.numero;
				var padrao = re.padrao;
				this.cartoes.push({id:id,padrao:padrao,tipo:tipo,numero:numero.substr(numero.length - 4)});
				
			});
		})
		.catch((error: any) => {
			console.log('error1',error);
		});
	}

	ionViewDidLoad(){

		this.meuscartoes();

		this.apiProvider.getMeioPagamento(parseInt(JSON.parse(localStorage.getItem('dados_usuario')).id))
		.then((result: any) => {
			result.forEach( (re) => {
				this.pagamento = re.tipo;
				if(re.tipo == "dinheiro"){
					this.tipopagamento=true;
				}else{
					this.tipopagamento=false;
				}
			});
			
		})
		.catch((error: any) => {
			console.log('error1',error);
		});
	}

	addPagamento(){
		if(this.tipopagamento){
			let alert = this.alertCtrl.create({
				title: 'Escolha Cartão como meio de pagamento',
				buttons: ['Ok']
			});
			alert.present();
		}else{
			if(this.totalpgmt == 2){
				let alert = this.alertCtrl.create({
					title: 'Só é permitido adicionar 2 cartões',
					buttons: ['Ok']
				});
				alert.present();
			}else{
				//let psh = this.navCtrl.push('NovoPagamentoPage');

				let psh = this.modalCtrl.create('ModalNovoPagamentoPage');
		    	psh.present();
				psh.onDidDismiss((result) =>{
					if(result){
						//console.log(result);
						if(result.confirmado == 'ok'){
							this.meuscartoes();
						}
					}
				});
			}
		}
	}

	selecionarPadrao(pgtm){
		this.apiProvider.padraoMeioPagamento(pgtm, parseInt(JSON.parse(localStorage.getItem('dados_usuario')).id) )
		.then((result: any) => {
			if(result.error){
				let toast = this.toastCtrl.create({
					message: 'Não foi possível gravar meio de pagamento',
					duration: 3000,
					position: 'bottom',
					cssClass: "toasterro"
				});
				toast.present();
			}else{
				let toast = this.toastCtrl.create({
					message: 'Meio de pagamento padrão confirmado',
					duration: 3000,
					position: 'bottom'
				});
				toast.present();

				if(pgtm == 'dinheiro'){
					this.tipopagamento=true;
				}else{
					this.tipopagamento=false;
				}
			}
		})
		.catch((error: any) => {
			console.log('error1',error);
		});
	}

	selecionarPadraoCartao(id_cartao:number){
		this.apiProvider.cartaoPadrao(id_cartao, parseInt(JSON.parse(localStorage.getItem('dados_usuario')).id) )
		.then((result: any) => {
			if(result.error){
				let toast = this.toastCtrl.create({
					message: 'Não foi possível selecionar o cartão',
					duration: 2000,
					position: 'bottom',
					cssClass: "toasterro"
				});
				toast.present();
			}else{
				let toast = this.toastCtrl.create({
					message: 'Cartão padrão selecionado',
					duration: 2000,
					position: 'bottom'
				});
				toast.present();
			}
		})
		.catch((error: any) => {
			console.log('error1',error);
		});
	}

}
