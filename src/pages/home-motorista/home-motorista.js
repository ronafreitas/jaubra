var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { IonicPage, MenuController, AlertController, ModalController, ToastController, Platform, LoadingController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import firebase from 'firebase';
import { BackgeolocProvider } from '../../providers/backgeoloc/backgeoloc';
import { Push } from '@ionic-native/push';
import { Http, Headers, RequestOptions } from '@angular/http';
var HomeMotoristaPage = /** @class */ (function () {
    function HomeMotoristaPage(menuCtrl, geolocation, toastCtrl, zone, modalCtrl, loadingCtrl, platform, bkgProv, push, http, alertCtrl) {
        this.menuCtrl = menuCtrl;
        this.geolocation = geolocation;
        this.toastCtrl = toastCtrl;
        this.zone = zone;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.platform = platform;
        this.bkgProv = bkgProv;
        this.push = push;
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.isCorrida = false;
        this.corridaCancelada = false;
        this.onceCP = false;
        this.cidade_esta = '';
        this.origem = '';
        this.destino = '';
        this.oncemarker = true;
        this.quem_cancela_busca = true;
        this.onceMTatualiza = false;
        this.emcorrida = false;
        this.appOn = false;
        this.distProxPassa = 0.031; // distancia do passageiro para encerra corrida, 0.031 = 31 metros
        this.motorista_cancela_busca = false;
        this.infomarkrpass = false;
        this.motoUpdateLocationBuscaPassagOn = true;
        this.busca_passageiro = false;
        this.alert_passageiro_confirmou = null;
        // ???????????????????
        this.menuCtrl.enable(false, 'pas');
        this.menuCtrl.enable(true, 'mot');
        // ???????????????????
        //localStorage.removeItem('corrida_atual');
        if (!localStorage.getItem('corrida_atual')) {
            localStorage.setItem('corrida_atual', JSON.stringify([]));
        }
        if (!localStorage.getItem('id_fcm')) {
            localStorage.setItem('id_fcm', '');
        }
        if (!localStorage.getItem('status')) {
            localStorage.setItem('status', 'true');
            this.statusMorotista = true;
        }
        this.fb = firebase.database();
        this.novosmarkers = [];
        this.markers_passageiros = [];
        /*this.loading_esperar = this.loadingCtrl.create({
            content: 'Aguarde...'
        });
        this.loading_esperar.present();*/
    }
    HomeMotoristaPage.prototype.ionViewDidLoad = function () {
        var ts = this;
        //platform.ready().then(() => {
        ts.meuid = JSON.parse(localStorage.getItem('dados_usuario')).id;
        ts.cidade_esta = 'ba/salvador';
        localStorage.setItem('cidade_esta', JSON.stringify(ts.cidade_esta));
        if (!localStorage.getItem('aguarda_passageiro_entrar')) {
            localStorage.setItem('aguarda_passageiro_entrar', 'false');
        }
        ts.init();
        ts.listarChamadas();
        if (ts.platform.is('cordova') || ts.platform.is('ios') || ts.platform.is('android')) {
            ts.platform.pause.subscribe(function () {
                console.log('pause');
                //this.watchSubLoc.unsubscribe();
                ts.bkgProv.iniciarBackgroundGeo(ts.meuid, ts.cidade_esta);
            });
            /*this.platform.resume.subscribe(() => {
                this.atualizarMinhaLocalOnForeground();
            });*/
        }
        localStorage.setItem('primeiro_acesso', 'true');
        ts.registraFCM();
        //});
    };
    HomeMotoristaPage.prototype.init = function () {
        var _this = this;
        //return new Promise((resolve, reject)=> {
        this.geolocation.getCurrentPosition({ enableHighAccuracy: false })
            .then(function (position) {
            var ts = _this;
            //ts.minhas_coord = {lat: -23.543205,lng: -46.641311};
            ts.minhas_coord = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            var mylocation = ts.minhas_coord;
            _this.map = new google.maps.Map(_this.mapElement.nativeElement, {
                zoom: 14,
                center: mylocation,
                disableDefaultUI: true,
                styles: [
                    { "featureType": "administrative", "elementType": "all", "stylers": [{ "visibility": "off" }] },
                    { "featureType": "landscape", "elementType": "all", "stylers": [{ "visibility": "off" }] },
                    { "featureType": "poi", "elementType": "all", "stylers": [{ "visibility": "off" }] },
                    { "featureType": "road", "elementType": "all", "stylers": [{ "visibility": "on" }] },
                    { "featureType": "transit", "elementType": "all", "stylers": [{ "visibility": "off" }] }
                ],
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            /*if(this.loading_esperar){
                this.loading_esperar.dismiss();
            }*/
            //var latlng = {lat: -23.543205, lng: -46.641311};
            var latlng = { lat: position.coords.latitude, lng: position.coords.longitude };
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        var checkstatus = ts.fb.ref("motoristas/" + ts.cidade_esta + "/" + ts.meuid + "/status");
                        var funcSts = function (childSnapshot) {
                            var snap = childSnapshot.val();
                            ts.zone.run(function () {
                                if (snap != null) {
                                    ts.statusMorotista = snap;
                                    ts.fb.ref('motoristas/' + ts.cidade_esta + "/" + ts.meuid)
                                        .set({
                                        id: ts.meuid,
                                        lat: latlng.lat,
                                        lng: latlng.lng,
                                        status: ts.statusMorotista
                                    });
                                    localStorage.setItem('status', snap.toString());
                                    if (snap) {
                                        ts.meuMarker(mylocation);
                                    }
                                }
                                else {
                                    ts.statusMorotista = true;
                                    ts.fb.ref('motoristas/' + ts.cidade_esta + "/" + ts.meuid)
                                        .set({
                                        id: ts.meuid,
                                        lat: latlng.lat,
                                        lng: latlng.lng,
                                        status: ts.statusMorotista
                                    });
                                    localStorage.setItem('status', 'true');
                                    ts.meuMarker(mylocation);
                                }
                            });
                        };
                        checkstatus.once('value', funcSts);
                    }
                    else {
                        alert('Não foi possível obter sua localização (e1)');
                    }
                }
                else {
                    alert('Não foi possível obter sua localização (e2)');
                }
            });
            if (localStorage.getItem('status') == 'true') {
                ts.bkgProv.iniciarBackgroundGeo(ts.meuid, ts.cidade_esta);
            }
            //resolve(this.map);
            return;
        })
            .catch(function (err) {
            console.error('Could not read current location');
            //reject(err);
        });
        //});
    };
    HomeMotoristaPage.prototype.meuMarker = function (mylocation) {
        if (localStorage.getItem('status') == 'true') {
            var ts_1 = this;
            ts_1.meu_marker = new google.maps.Marker({
                position: mylocation,
                map: ts_1.map,
                icon: {
                    url: 'assets/imgs/perfil_motorista.png',
                    scaledSize: new google.maps.Size(40, 40)
                }
            });
            ts_1.map.setCenter(mylocation);
            //this.appOn=true;
            //this.map.setZoom(14);
            ts_1.fb.ref("motoristas/" + ts_1.cidade_esta + "/" + ts_1.meuid)
                .on('value', function (childSnapshot) {
                var snap = childSnapshot.val();
                if (snap !== null) {
                    if (ts_1.meu_marker) {
                        ts_1.meu_marker.setMap(null);
                    }
                    ts_1.minhas_coord = { lat: snap.lat, lng: snap.lng };
                    ts_1.meu_marker = new google.maps.Marker({
                        position: ts_1.minhas_coord,
                        map: ts_1.map,
                        icon: {
                            url: 'assets/imgs/perfil_motorista.png',
                            scaledSize: new google.maps.Size(40, 40)
                        }
                    });
                }
            });
        }
    };
    HomeMotoristaPage.prototype.atualizarMinhaLocalOnForeground = function () {
        var ts = this;
        ts.fb.ref("motoristas/" + ts.cidade_esta + "/" + ts.meuid)
            .on('value', function (childSnapshot) {
            var snap = childSnapshot.val();
            if (snap !== null) {
                if (ts.meu_marker) {
                    ts.meu_marker.setMap(null);
                }
                ts.minhas_coord = { lat: snap.lat, lng: snap.lng };
                ts.meu_marker = new google.maps.Marker({
                    position: ts.minhas_coord,
                    map: ts.map,
                    icon: {
                        url: 'assets/imgs/perfil_motorista.png',
                        scaledSize: new google.maps.Size(40, 40)
                    }
                });
            }
        });
        /*let ts = this;
        ts.watchSubLoc = ts.geolocation.watchPosition({enableHighAccuracy: true})
        .filter((p) => p.coords !== undefined) //Filter Out Errors
        .subscribe(position => {

            if(ts.meu_marker){
                ts.meu_marker.setMap(null);
            }

            ts.minhas_coord = {lat: position.coords.latitude,lng:position.coords.longitude};
            ts.fb.ref("motoristas/"+ts.cidade_esta+"/"+ts.meuid).update({
                lat:position.coords.latitude,
                lng:position.coords.longitude,
                tipo:'fore'
            });

            ts.meu_marker = new google.maps.Marker({
                position: ts.minhas_coord,
                map: ts.map,
                icon: {
                    url: 'assets/imgs/perfil_motorista.png',
                    scaledSize: new google.maps.Size(40, 40)
                }
            });
            //console.log('watcMoto',position.coords.longitude + ',  ' + position.coords.latitude);
        });
        ts.bkgProv.pararBackgroundGeo();*/
    };
    // logo q motorista inicia APP
    HomeMotoristaPage.prototype.listarChamadas = function () {
        var ts = this;
        ts.fb.ref("chamadas/motoristas/" + ts.cidade_esta + "/" + ts.meuid)
            .on('value', function (childSnapshot) {
            var snap = childSnapshot.val();
            if (snap !== null) {
                ts.infomarkrpass = true;
                var origem, destino, latlong_motorista, latlong_passageiro, solicitante, latlong_parada_final, addListener;
                var marker = [];
                for (var cm in snap) {
                    origem = snap[cm].origem;
                    destino = snap[cm].destino;
                    latlong_motorista = snap[cm].latlong_motorista;
                    latlong_passageiro = snap[cm].latlong_passageiro;
                    solicitante = snap[cm].solicitante;
                    latlong_parada_final = snap[cm].latlong_parada_final;
                    var psh = {
                        origem: origem,
                        destino: destino,
                        latlong_motorista: latlong_motorista,
                        latlong_passageiro: latlong_passageiro,
                        latlong_parada_final: latlong_parada_final,
                        solicitante: solicitante
                    };
                    //ts.dadosdestacorrida = psh;
                    ts.dadosdestacorrida = snap[cm];
                    ts.minhas_coord = { lat: latlong_passageiro.lat, lng: latlong_passageiro.lng };
                    var locpass = ts.minhas_coord;
                    marker.push(solicitante);
                    var mkr = new google.maps.Marker({
                        position: locpass,
                        map: ts.map,
                        animation: google.maps.Animation.DROP,
                        icon: {
                            url: 'assets/imgs/perfil_usuario.png',
                            scaledSize: new google.maps.Size(40, 40)
                        },
                        id: solicitante,
                        detalhes: psh
                    });
                    marker[solicitante] = mkr;
                    ts.novosmarkers.push(mkr);
                    ts.markers_passageiros.push(mkr);
                    ts.origem = psh.origem;
                    ts.destino = psh.destino;
                    addListener = function (i) {
                        marker[i].addListener('click', function () {
                            if (ts.oncemarker) {
                                ts.oncemarker = false;
                                ts.appOn = false;
                                var tsthis_1 = this;
                                ts.zone.run(function () {
                                    //ts.infomarkrpass=true;
                                    setTimeout(function () {
                                        ts.origem = tsthis_1.detalhes.origem;
                                        ts.destino = tsthis_1.detalhes.destino;
                                    }, 100);
                                    setTimeout(function () {
                                        ts.oncemarker = true;
                                    }, 500);
                                });
                            }
                        });
                    };
                    addListener(solicitante);
                }
                /*ts.fb.ref("chamadas/motoristas/"+ts.cidade_esta+"/"+ts.meuid)
                .on('child_removed', function(childSnapshot){
                    var snap = childSnapshot.val();
                    if(snap !== null){
                        ts.infomarkrpass=false;
                        ts.appOn=true;
                        if(ts.markers_passageiros){
                            for(var i = 0; i < ts.markers_passageiros.length; i++){
                                if(ts.markers_passageiros[i]){
                                    ts.markers_passageiros[i].setMap(null);
                                }
                            }
                        }
                    }
                });*/
                ts.quem_cancela_busca = true;
                //ts.infomarkrpass=true;
                ts.appOn = false;
            }
            else {
                /*

                após atenderChamada
                localStorage.setItem('aguarda_passageiro_entrar', 'true');
                localStorage.setItem('id_passageiro_aguardando', JSON.stringify(dataChamadaObj.solicitante));
                localStorage.setItem('corrida_atual', JSON.stringify(dataChamadaObj));

                */
                ts.busca_passageiro = false;
                if (localStorage.getItem('aguarda_passageiro_entrar')) {
                    if (localStorage.getItem('aguarda_passageiro_entrar') == 'true') {
                        var id_passageiro = localStorage.getItem('id_passageiro_aguardando');
                        if (id_passageiro != 'null') {
                            var buscaPasAguar = 'buscar_passageiro/' + ts.cidade_esta + "/" + id_passageiro + "/" + ts.meuid + "/iniciada";
                            ts.alert_aguardar_passageiro = setInterval(function () {
                                ts.fb.ref(buscaPasAguar)
                                    .once('value', function (childSnapshot) {
                                    var retn = childSnapshot.val();
                                    if (retn !== null) {
                                        ts.busca_passageiro = true;
                                        ts.atualizaLocalizacaoMotorista(retn, retn.latLng_motorista);
                                    }
                                });
                            }, 2000);
                            var buscaPas = 'buscar_passageiro/' + ts.cidade_esta + "/" + id_passageiro + "/" + ts.meuid;
                            ts.fb.ref(buscaPas + "/cancelado_passageiro")
                                .on('value', function (childSnapshot) {
                                if (ts.modal_mensagem != null) {
                                    ts.modal_mensagem.dismiss();
                                }
                                var snap = childSnapshot.val();
                                if (ts.onceCP == false) {
                                    if (snap != null) {
                                        ts.quem_cancela_busca = false;
                                        ts.motoUpdateLocationBuscaPassagOn = false;
                                        ts.busca_passageiro = false;
                                        var alert_1 = ts.alertCtrl.create({
                                            title: 'Corrida cancelada',
                                            message: 'Por alguma razão o passageiro cancelou a corrida',
                                            buttons: [
                                                { text: 'Ok', role: 'ok',
                                                    handler: function () {
                                                        localStorage.setItem('corrida_atual', JSON.stringify([]));
                                                        ts.init();
                                                        ts.listarChamadas();
                                                        setTimeout(function () {
                                                            ts.onceCP = false;
                                                        }, 300);
                                                    }
                                                }
                                            ],
                                            enableBackdropDismiss: false
                                        });
                                        alert_1.present();
                                        ts.onceCP = true;
                                        ts.appOn = true;
                                        ts.emcorrida = false;
                                    }
                                }
                            });
                            var buscaPas = 'buscar_passageiro/' + ts.cidade_esta + "/" + id_passageiro + "/" + ts.meuid;
                            ts.fb.ref(buscaPas + "/aguardando")
                                .on('value', function (childSnapshot) {
                                var snap = childSnapshot.val();
                                if (snap !== null) {
                                    var alert_2 = ts.alertCtrl.create({
                                        title: 'Você chegou ao local',
                                        message: 'Aguarde o passageiro confirmar...',
                                        buttons: [
                                            { text: 'Ok', role: 'ok',
                                                handler: function () {
                                                }
                                            }
                                        ],
                                        enableBackdropDismiss: false
                                    });
                                    alert_2.present();
                                    //ts.enviaPush(snap, "Motorista cancelou corrida", "Corrida cancelada");
                                }
                            });
                            ts.fb.ref(buscaPas + "/finalizada")
                                .on('value', function (childSnapshot) {
                                var snap = childSnapshot.val();
                                if (snap !== null) {
                                    var alert_3 = ts.alertCtrl.create({
                                        title: 'Passageiro aguardando',
                                        message: 'Inicie a corrida',
                                        buttons: [
                                            { text: 'Iniciar', role: 'iniciar',
                                                handler: function () {
                                                    ts.fb.ref("usuarios_fcm/" + id_passageiro + "/fcmId")
                                                        .once('value', function (childSnapshot) {
                                                        var pshs = childSnapshot.val();
                                                        if (pshs !== null) {
                                                            ts.enviaPush(pshs, "Motorista cancelou corrida", "Corrida cancelada");
                                                        }
                                                    });
                                                    ts.fb.ref(buscaPas).remove();
                                                    ts.iniciarCorrida(id_passageiro);
                                                }
                                            }
                                        ],
                                        enableBackdropDismiss: false
                                    });
                                    alert_3.present();
                                    //ts.enviaPush(snap, "Motorista cancelou corrida", "Corrida cancelada");
                                }
                            });
                        }
                    }
                    else {
                        setTimeout(function () {
                            ts.zone.run(function () {
                                ts.appOn = true;
                                ts.busca_passageiro = false;
                            });
                        }, 300);
                    }
                }
                else {
                    setTimeout(function () {
                        ts.zone.run(function () {
                            ts.appOn = true;
                            ts.busca_passageiro = false;
                        });
                    }, 300);
                }
            }
        }); // fim de .on('value' "chamadas/motoristas/
        ts.fb.ref("chamadas/motoristas/" + ts.cidade_esta + "/" + ts.meuid)
            .on('child_removed', function (childSnapshot) {
            var snap = childSnapshot.val();
            if (snap !== null) {
                ts.infomarkrpass = false;
                ts.appOn = true;
                if (ts.markers_passageiros) {
                    for (var i = 0; i < ts.markers_passageiros.length; i++) {
                        if (ts.markers_passageiros[i]) {
                            ts.markers_passageiros[i].setMap(null);
                        }
                    }
                }
            }
        });
    };
    /*aguardando(idUsuario){
        let ts = this;
        var buscaPas = 'buscar_passageiro/'+ts.cidade_esta+"/"+idUsuario+"/"+ts.meuid;
        ts.fb.ref(buscaPas+"/aguardando")
        .once('value', function (childSnapshot){
            var snap = childSnapshot.val();
            if(snap !== null){
                //ts.enviaPush(snap, "Motorista cancelou corrida", "Corrida cancelada");
            }
        });
    }*/
    HomeMotoristaPage.prototype.fechaInfo = function () {
        this.infomarkrpass = false;
        //evv.target.parentElement.parentElement.style.display='none';
        this.origem = '';
        this.destino = '';
        this.appOn = true;
    };
    HomeMotoristaPage.prototype.cancelarBusca = function () {
        var ts = this;
        var dadosCorridaCanc = JSON.parse(localStorage.getItem('corrida_atual'));
        var buscaPas = 'buscar_passageiro/' + ts.cidade_esta + "/" + dadosCorridaCanc.solicitante + "/" + ts.meuid;
        var alert = ts.alertCtrl.create({
            title: 'Cancelar corrida',
            message: 'Você tem certeza que deseja cancelar?',
            buttons: [
                { text: 'Não, continuar', role: 'iniciar', handler: function () { } },
                { text: 'Sim, cancelar corrida', role: 'cancelar',
                    handler: function () {
                        localStorage.setItem('aguarda_passageiro_entrar', 'false');
                        ts.motoUpdateLocationBuscaPassagOn = false;
                        ts.fb.ref(buscaPas + "/cancelado_motorista").set(true);
                        setTimeout(function () {
                            ts.init();
                            ts.listarChamadas();
                            ts.infomarkrpass = false;
                            ts.fb.ref(buscaPas + "/iniciada").remove();
                            ts.fb.ref(buscaPas).remove();
                            var motoBucs = 'motoristas/' + ts.cidade_esta + "/" + ts.meuid + "/busca";
                            ts.fb.ref(motoBucs).remove();
                            ts.fb.ref("usuarios_fcm/" + dadosCorridaCanc.solicitante + "/fcmId")
                                .once('value', function (childSnapshot) {
                                var snap = childSnapshot.val();
                                if (snap !== null) {
                                    ts.enviaPush(snap, "Motorista cancelou corrida", "Corrida cancelada");
                                }
                            });
                            ts.busca_passageiro = false;
                            ts.emcorrida = false;
                            localStorage.setItem('corrida_atual', JSON.stringify([]));
                            localStorage.setItem('id_passageiro_aguardando', 'null');
                        }, 300);
                        var idpassageiro = JSON.parse(localStorage.getItem('id_passageiro_aguardando'));
                        ts.fb.ref('chat/' + JSON.parse(localStorage.getItem('dados_usuario')).id + '-' + idpassageiro).remove();
                        ts.fb.ref('chat/' + idpassageiro + '-' + JSON.parse(localStorage.getItem('dados_usuario')).id).remove();
                    }
                }
            ],
            enableBackdropDismiss: false
        });
        alert.present();
    };
    HomeMotoristaPage.prototype.atenderChamada = function (dataChamadaObj, e) {
        e.target.parentElement.parentElement.style.display = 'none';
        var ts = this;
        ts.emcorrida = true;
        ts.infomarkrpass = false;
        var chamdaguard = ts.fb.ref("chamadas/passageiro/" + dataChamadaObj.solicitante);
        chamdaguard.once('value', function (childSnapshot) {
            var snap = childSnapshot.val();
            if (snap !== null) {
                for (var d in snap) {
                    ts.fb.ref("chamadas/motoristas/" + snap[d].local_base + "/" + snap[d].motorista + "/" + dataChamadaObj.solicitante).remove();
                    ts.fb.ref("chamadas/passageiro/" + dataChamadaObj.solicitante + "/" + d).remove();
                }
            }
        });
        var dadoscorrida = {
            motorista: ts.meuid,
            latLng_motorista: ts.minhas_coord,
            dados_corrida: dataChamadaObj
        };
        var buscaPas = 'buscar_passageiro/' + ts.cidade_esta + "/" + dataChamadaObj.solicitante + "/" + ts.meuid;
        ts.fb.ref(buscaPas + "/iniciada").set(dadoscorrida);
        var motoBucs = 'motoristas/' + ts.cidade_esta + "/" + ts.meuid + "/busca";
        ts.fb.ref(motoBucs).set(dadoscorrida);
        // set time interval
        /*ts.fb.ref(buscaPas+"/iniciada")
        .on('child_changed', function(childSnapshot){
            var snap = childSnapshot.val();
            if(snap !== null){
                ts.atualizaLocalizacaoMotorista(dadoscorrida,snap);
            }
        });*/
        //ts.busca_passageiro = true;
        ts.alert_aguardar_passageiro = setInterval(function () {
            ts.fb.ref(buscaPas + "/iniciada")
                .once('value', function (childSnapshot) {
                var retn = childSnapshot.val();
                if (retn !== null) {
                    ts.atualizaLocalizacaoMotorista(dadoscorrida, retn.latLng_motorista);
                }
            });
        }, 2000);
        ts.fb.ref("usuarios_fcm/" + dataChamadaObj.solicitante + "/fcmId")
            .once('value', function (childSnapshot) {
            var snap = childSnapshot.val();
            if (snap !== null) {
                ts.enviaPush(snap, "Corrida confirmada", "Motorista está a caminho");
            }
        });
        ts.fb.ref(buscaPas + "/cancelado_passageiro")
            .on('value', function (childSnapshot) {
            var snap = childSnapshot.val();
            if (ts.onceCP == false) {
                if (snap != null) {
                    ts.quem_cancela_busca = false;
                    ts.motoUpdateLocationBuscaPassagOn = false;
                    //ts.busca_passageiro = false;			    	
                    var alert_4 = ts.alertCtrl.create({
                        title: 'Corrida cancelada',
                        message: 'Por alguma razão o passageiro cancelou a corrida',
                        buttons: [
                            { text: 'Ok', role: 'ok',
                                handler: function () {
                                    localStorage.setItem('corrida_atual', JSON.stringify([]));
                                    ts.init();
                                    ts.listarChamadas();
                                    setTimeout(function () {
                                        ts.onceCP = false;
                                    }, 300);
                                }
                            }
                        ],
                        enableBackdropDismiss: false
                    });
                    alert_4.present();
                    ts.onceCP = true;
                    ts.appOn = true;
                    ts.emcorrida = false;
                }
            }
        });
        //ts.busca_passageiro=true;
        ts.appOn = false;
        localStorage.setItem('aguarda_passageiro_entrar', 'true');
        localStorage.setItem('id_passageiro_aguardando', JSON.stringify(dataChamadaObj.solicitante));
        localStorage.setItem('corrida_atual', JSON.stringify(dataChamadaObj));
    };
    // set time interval
    HomeMotoristaPage.prototype.atualizaLocalizacaoMotorista = function (dadosCorrida, latlng) {
        var ts = this;
        var latlngPassageiro = dadosCorrida.dados_corrida.latlong_passageiro;
        var distraj = ts.distanciaCorrida(latlng.lat, latlng.lng, latlngPassageiro.lat, latlngPassageiro.lng);
        if (distraj != 0) {
            if (distraj > ts.distProxPassa) {
                ts.criaRotaDoMotorista({ lat: latlng.lat, lng: latlng.lng }, latlngPassageiro);
            }
            else {
                if (distraj < ts.distProxPassa) {
                    if (ts.onceMTatualiza == false) {
                        ts.onceMTatualiza = true;
                        var idPassageiro_1 = dadosCorrida.dados_corrida.solicitante;
                        var idMotorista = dadosCorrida.motorista;
                        ts.infomarkrpass = false;
                        clearInterval(ts.alert_aguardar_passageiro);
                        // aguarda o passageiro criar 'buscar_passageiro/finalizada'
                        var buscaPas = 'buscar_passageiro/' + ts.cidade_esta + "/" + idPassageiro_1 + "/" + idMotorista;
                        console.log('dadosCorridaXX', dadosCorrida);
                        ts.fb.ref(buscaPas + "/finalizada")
                            .on('value', function (childSnapshot) {
                            var snap = childSnapshot.val();
                            if (snap !== null) {
                                ts.iniciarCorrida(idPassageiro_1);
                            }
                        });
                        ts.fb.ref(buscaPas + "/aguardando")
                            .once('value', function (childSnapshot) {
                            var snap = childSnapshot.val();
                            if (snap !== null) {
                                ts.toastCtrl.create({
                                    message: 'Você chegou ao local, aguarde o passageiro',
                                    position: 'middle',
                                    duration: 3500
                                }).present();
                            }
                        });
                    }
                }
            }
        }
    };
    HomeMotoristaPage.prototype.iniciarCorrida = function (idPassageiro) {
        localStorage.setItem('corrida_pendente', 'true');
        var ts = this;
        if (ts.alert_passageiro_confirmou != null) {
            ts.alert_passageiro_confirmou.dismiss();
        }
        ts.alert_passageiro_confirmou = ts.alertCtrl.create({
            title: 'Passageiro confirmou',
            message: 'Você já pode iniciar a corrida',
            buttons: [
                { text: 'Iniciar', role: 'Iniciar',
                    handler: function () {
                        var buscaPas = 'buscar_passageiro/' + ts.cidade_esta + "/" + idPassageiro;
                        ts.fb.ref(buscaPas).remove();
                        ts.corridaIniciada();
                    }
                }
            ],
            enableBackdropDismiss: false
        });
        ts.alert_passageiro_confirmou.present();
    };
    HomeMotoristaPage.prototype.corridaIniciada = function () {
        var ts = this;
        if (ts.meu_marker) {
            ts.meu_marker.setMap(null);
        }
        if (ts.markers_passageiros) {
            for (var i = 0; i < ts.markers_passageiros.length; i++) {
                if (ts.markers_passageiros[i]) {
                    ts.markers_passageiros[i].setMap(null);
                }
            }
        }
        // quando o motorista aceita, coloco a mesma lat e lng do usuário, afinal, se ele pegou o passageiro é pq eles estão no mesmo lugar
        var dadosCorrida = JSON.parse(localStorage.getItem('corrida_atual'));
        if (dadosCorrida.dados_corrida) {
            var corriInitDad = ts.fb.ref('corrida_iniciada/' + ts.cidade_esta + "/" + ts.meuid + "/" + dadosCorrida.dados_corrida.solicitante);
            corriInitDad.set({
                motorista: ts.meuid,
                dados_corrida: dadosCorrida,
                latLng_motorista: {
                    lat: dadosCorrida.dados_corrida.latlong_passageiro.lat,
                    lng: dadosCorrida.dados_corrida.latlong_passageiro.lng
                }
            });
            ts.corrida_final_atualiza = setInterval(function () {
                corriInitDad.once('value', function (childSnapshot) {
                    var retn = childSnapshot.val();
                    if (retn !== null) {
                        console.log(retn);
                        //ts.atualizaCorrida(retn.latLng_motorista,retn.dados_corrida.latlong_parada_final);
                    }
                });
            }, 2000);
            console.log('corrida iniciada - A');
        }
        else {
            var corriInit = ts.fb.ref('corrida_iniciada/' + ts.cidade_esta + "/" + ts.meuid + "/" + dadosCorrida.solicitante);
            corriInit.set({
                motorista: ts.meuid,
                dados_corrida: dadosCorrida,
                latLng_motorista: {
                    lat: dadosCorrida.latlong_passageiro.lat,
                    lng: dadosCorrida.latlong_passageiro.lng
                }
            });
            ts.corrida_final_atualiza = setInterval(function () {
                corriInit.once('value', function (childSnapshot) {
                    var retn = childSnapshot.val();
                    if (retn !== null) {
                        ts.atualizaCorrida(retn.latLng_motorista, retn.dados_corrida.latlong_parada_final);
                    }
                });
            }, 2000);
            console.log('corrida iniciada - B');
        }
    };
    HomeMotoristaPage.prototype.atualizaCorrida = function (latlngOrigem, latlngDestino) {
        var ts = this;
        var distcor = ts.distanciaCorrida(latlngOrigem.lat, latlngOrigem.lng, latlngDestino.lat, latlngDestino.lng);
        console.log('distcor', distcor);
        if (distcor != 0) {
            if (distcor > ts.distProxPassa) {
                ts.criaRotaDoMotorista({ lat: latlngOrigem.lat, lng: latlngOrigem.lng }, latlngDestino);
            }
            else {
                if (distcor < ts.distProxPassa) {
                    console.log(distcor + ' <' + ts.distProxPassa);
                    clearInterval(ts.corrida_final_atualiza);
                    // fim da corrida
                    // 1 - MOROTISTA: toast e reinicia maps
                    // 2 - passageiro: toast e reinicia maps
                    // remove aqui e também lá na API
                    var fimcorrida = ts.fb.ref('corrida_iniciada/' + ts.cidade_esta + "/" + ts.meuid);
                    fimcorrida.once('child_removed', function (childSnapshot) {
                        var _this = this;
                        var snap = childSnapshot.val();
                        if (snap !== null) {
                            var tofin = ts.toastCtrl.create({
                                message: 'Corrida finalizada',
                                position: 'middle',
                                duration: 3500
                            });
                            tofin.present();
                            tofin.onDidDismiss(function (result) {
                                _this.init();
                                //this.atualizarMinhaLocalOnForeground();
                                _this.listarChamadas();
                            });
                            //.dismiss();
                        }
                    });
                    fimcorrida.remove();
                }
            }
        }
    };
    HomeMotoristaPage.prototype.aguardando = function (idUsuario) {
        var ts = this;
        var buscaPas = 'buscar_passageiro/' + ts.cidade_esta + "/" + idUsuario + "/" + ts.meuid;
        ts.fb.ref(buscaPas + "/aguardando")
            .once('value', function (childSnapshot) {
            var snap = childSnapshot.val();
            if (snap !== null) {
                //ts.enviaPush(snap, "Motorista cancelou corrida", "Corrida cancelada");
            }
        });
    };
    HomeMotoristaPage.prototype.getDistanceFromLatLonInKm = function (lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius da terra em km
        var dLat = this.deg2rad(lat2 - lat1);
        var dLon = this.deg2rad(lon2 - lon1);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
                Math.sin(dLon / 2) * Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // distancia em km
        return d;
    };
    HomeMotoristaPage.prototype.deg2rad = function (deg) {
        return deg * (Math.PI / 180);
    };
    HomeMotoristaPage.prototype.distanciaCorrida = function (lat1, lng1, lat2, lng2) {
        var dts = this.getDistanceFromLatLonInKm(lat2, lng2, lat1, lng1);
        return Number((dts).toFixed(3));
    };
    HomeMotoristaPage.prototype.criaRotaDoMotorista = function (latlng1, latlng2) {
        var ts = this;
        var directionsService = new google.maps.DirectionsService;
        if (ts.directionsDisplay) {
            ts.directionsDisplay.setMap(null);
            ts.directionsDisplay = null;
        }
        ts.directionsDisplay = new google.maps.DirectionsRenderer();
        directionsService.route({
            origin: latlng1,
            destination: latlng2,
            travelMode: 'DRIVING'
        }, function (response, status) {
            if (status === 'OK') {
                for (var j = 0; j < response.routes.length; j++) {
                    ts.directionsDisplay.setMap(ts.map);
                    ts.directionsDisplay.setDirections(response);
                    ts.directionsDisplay.setRouteIndex(j);
                    ts.directionsDisplay.setOptions({
                        draggable: false,
                        hideRouteIndex: true,
                        polylineOptions: {
                            strokeColor: "#29a3a9",
                            strokeOpacity: 0.9,
                            strokeWeight: 4
                        }
                    });
                }
            }
            else {
                console.log('erro', status);
            }
        });
    };
    HomeMotoristaPage.prototype.statusMudar = function () {
        var msn, sts;
        var ts = this;
        if (ts.statusMorotista) {
            msn = 'Você voltou a aparecer nas buscas dos passageiros';
            sts = true;
            localStorage.setItem('status', 'true');
            //ts.bkgProv.iniciarBackgroundGeo(ts.meuid,ts.cidade_esta);
        }
        else {
            msn = 'Você não irá aparecer nas buscas dos passageiros';
            sts = false;
            localStorage.setItem('status', 'false');
            if (ts.meu_marker) {
                ts.meu_marker.setMap(null);
            }
            if (ts.markers_passageiros) {
                for (var i = 0; i < ts.markers_passageiros.length; i++) {
                    if (ts.markers_passageiros[i]) {
                        ts.markers_passageiros[i].setMap(null);
                    }
                }
            }
            //ts.bkgProv.pararBackgroundGeo();
        }
        ts.fb.ref('motoristas/' + ts.cidade_esta + "/" + ts.meuid + "/status").set(sts);
        ts.toastCtrl.create({
            message: msn,
            position: 'bottom',
            duration: 3500
        }).present();
        ts.infomarkrpass = false;
    };
    HomeMotoristaPage.prototype.registraFCM = function () {
        if (localStorage.getItem('id_fcm') == '') {
            var ts_2 = this;
            var options = {
                android: {
                    senderID: '632970989526',
                    clearNotifications: true,
                    clearBadge: false,
                    forceShow: true,
                    iconColor: '#33afb5'
                },
                ios: {
                    alert: 'true',
                    badge: true,
                    sound: 'true'
                },
                windows: {},
                browser: {}
            };
            var pushObject = this.push.init(options);
            pushObject.on('registration')
                .subscribe(function (regs) {
                localStorage.setItem('id_fcm', JSON.stringify(regs.registrationId));
                ts_2.fb.ref('usuarios_fcm/' + ts_2.meuid)
                    .set({ fcmId: regs.registrationId });
            });
        }
    };
    HomeMotoristaPage.prototype.enviaPush = function (regIdPassa, body, title) {
        var headers = new Headers({
            'Authorization': 'key=AAAAk2ACz9Y:APA91bECA4DYnVrTzhFGtCUa-9qqqSbAiGq3zvHtMObfmNKGPyyunu6vpfPlBWSxMDaDVyryCr4zEGKBKOZ-d6fMoeHwtGjObwf0hZxkohIxh58yECkoSU6ZH7pD0Pf7fa0vE9AhyeAQ',
            'Content-Type': 'application/json'
        });
        var options = new RequestOptions({ headers: headers });
        var notification = {
            "notification": {
                "title": title,
                "body": body,
                //"click_action": "FCM_PLUGIN_ACTIVITY",
                "sound": "sound2",
            }, "data": {
                "id_passageiro": this.meuid
            },
            "to": regIdPassa,
            "priority": "high"
        };
        var url = 'https://fcm.googleapis.com/fcm/send';
        this.http.post(url, notification, options)
            .subscribe(function (resp) {
            //console.log(resp);
        });
    };
    HomeMotoristaPage.prototype.iniciarMensagem = function () {
        var ts = this;
        var id_passageiro = JSON.parse(localStorage.getItem('id_passageiro_aguardando'));
        ts.modal_mensagem = ts.modalCtrl.create('ModalChatPage', { tipo: 'mot', id_usuario: id_passageiro });
        //let myModal = ts.modalCtrl.create('ModalChatPage', {motorista:ts.itemOpcPgmSelec});
        ts.modal_mensagem.present();
        ts.modal_mensagem.onDidDismiss(function (result) {
            if (result) {
                console.log('chat onDidDismiss', result);
            }
        });
    };
    __decorate([
        ViewChild('map'),
        __metadata("design:type", ElementRef)
    ], HomeMotoristaPage.prototype, "mapElement", void 0);
    HomeMotoristaPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-home-motorista',
            templateUrl: 'home-motorista.html',
        }),
        __metadata("design:paramtypes", [MenuController,
            Geolocation,
            ToastController,
            NgZone,
            ModalController,
            LoadingController,
            Platform,
            BackgeolocProvider,
            Push,
            Http,
            AlertController])
    ], HomeMotoristaPage);
    return HomeMotoristaPage;
}());
export { HomeMotoristaPage };
//# sourceMappingURL=home-motorista.js.map