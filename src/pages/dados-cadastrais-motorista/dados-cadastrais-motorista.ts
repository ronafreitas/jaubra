import { Component } from '@angular/core';
import { IonicPage,AlertController,ToastController} from 'ionic-angular';
import { ApiProvider } from './../../providers/api/api';
import { Camera, CameraOptions} from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

@IonicPage()
@Component({
  selector: 'page-dados-cadastrais-motorista',
  templateUrl: 'dados-cadastrais-motorista.html',
})
export class DadosCadastraisMotoristaPage {

	user:any={id:'',nome:'', sobrenome:'', celular:'',cpf:'', senha:'', cnh:'',endereco:'',vecimento:'', perfil:''};
	passwordType: string = 'password';
 	passwordIcon: string = 'eye-off';
 	image_cnh:any;
 	image_resid:any;
 	image_antece:any;
 	url_image_cnh:any;
 	url_image_resid:any;
 	url_image_antece:any;

	constructor(private alertCtrl: AlertController,
		public camera: Camera,
		private transfer: FileTransfer,
		private toastCtrl: ToastController,
		private apiProvider:ApiProvider){
	
	}
	ionViewDidLoad(){
		this.user.id = JSON.parse(localStorage.getItem('dados_usuario') ).id;
		this.user.nome = JSON.parse(localStorage.getItem('dados_usuario') ).nome;
		this.user.sobrenome = JSON.parse(localStorage.getItem('dados_usuario') ).sobrenome;
		this.user.celular = JSON.parse(localStorage.getItem('dados_usuario') ).celular;
		this.user.cpf = JSON.parse(localStorage.getItem('dados_usuario') ).cpf;
		this.user.senha = JSON.parse(localStorage.getItem('dados_usuario') ).senha;
		//
		this.user.cnh = JSON.parse(localStorage.getItem('dados_usuario') ).cnh;
		this.user.endereco = JSON.parse(localStorage.getItem('dados_usuario') ).endereco;
		this.user.vecimento = JSON.parse(localStorage.getItem('dados_usuario') ).vencimento_chn;
		this.user.perfil = 'mot';

		// MELHOR PEGAR LOGO TUDO NO LOGIN
		this.apiProvider.getDadosCadastrais(JSON.parse(localStorage.getItem('dados_usuario') ).id)
		.then((result: any) => {
			if(result[0]){
				var dados = result[0];
				this.user.cnh = parseInt(dados.cnh);
				this.user.endereco = dados.endereco;
				this.user.vecimento = dados.vencimento_chn;
			}
			//
		})
		.catch((error: any) => {
			console.log('error1',error);
		});
	}
	
	isReadonly(){
		return true;
	}

	hideShowPassword() {
		this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
		this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
	}

	fotocnh(){
		const options: CameraOptions = {
			quality: 50,
			allowEdit: true,
			destinationType: this.camera.DestinationType.NATIVE_URI,
			encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE,
			correctOrientation: true,
			saveToPhotoAlbum: false,
			targetWidth: 250,
			targetHeight: 400
		}
		this.captureCNH(options);
	}
	async captureCNH(options: CameraOptions){
		try {
		  const result = await this.camera.getPicture(options)

		  this.image_cnh ='1';
		  this.url_image_cnh =result;

		  /* file:///storage/emulated/0/Android/data/br.jaubra.app/cache/1523739550324.jpg */
		}
		catch (e) {
		  console.error(e);
		}
	}

	enviar(){

 		//url_image_cnh:any;
 		//url_image_resid:any;
 		//url_image_antece:any;

		const fileTransfer: FileTransferObject = this.transfer.create();

		let fileopts: FileUploadOptions = {
			fileKey: 'ionicfile',
			fileName: 'ionicfile',
			chunkedMode: false,
			mimeType: "image/jpeg",
			headers: {}
		}

		fileTransfer.upload(this.url_image_cnh, 'http://jaubra.faromidia.com.br/api_jaubra_uploads/index.php', fileopts)
		.then((data) => {
			console.log("Uploaded Successfully");
			console.log(data);
			this.url_image_cnh='';
			//this.imageFileName = "http://192.168.0.7:8080/static/images/ionicfile.jpg"
			// this.presentToast("Image uploaded successfully");
		}, (err) => {
			console.log(err);
		});
	}


	fotocomres(){
		const options: CameraOptions = {
			quality: 50,
			allowEdit: true,
			destinationType: this.camera.DestinationType.NATIVE_URI,
			encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE,
			correctOrientation: true,
			saveToPhotoAlbum: false,
			targetWidth: 250,
			targetHeight: 400
		}
		this.captureResidencia(options);
	}
	async captureResidencia(options: CameraOptions){
		try {
		  const result = await this.camera.getPicture(options)

		  this.image_resid = '1';
		  this.url_image_resid = result;

		}
		catch (e) {
		  console.error(e);
		}
	}

	fotoantec(){
		const options: CameraOptions = {
			quality: 50,
			allowEdit: true,
			destinationType: this.camera.DestinationType.NATIVE_URI,
			encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE,
			correctOrientation: true,
			saveToPhotoAlbum: false,
			targetWidth: 250,
			targetHeight: 400
		}
		this.captureAntecedentes(options);
	}
	async captureAntecedentes(options: CameraOptions){
		try {
		  const result = await this.camera.getPicture(options)

		  this.image_antece = '1';
		  this.url_image_antece = result;
		}
		catch (e) {
		  console.error(e);
		}
	}

	salvar(user){
		if(
			user.nome == '' || 
			user.sobrenome == '' || 
			user.celular == '' || 
			user.cpf == '' || 
			user.senha == '' || 
			user.cnh == '' || 
			user.endereco == '' || 
			user.vecimento == ''
			){

			let alert = this.alertCtrl.create({
				title: 'Todos os campos são obrigatórios',
				buttons: ['Ok']
			});
			alert.present();
			
			return false

		}else{
			this.apiProvider.updateDadosCadastrais(user)
			.then((result: any) => {
				if(result.error){
					let toast = this.toastCtrl.create({
						message: 'Não foi possível atualizar cadastro, tente novamente',
						duration: 3000,
						position: 'bottom',
						cssClass: "toasterro"
					});
					toast.present();
				}else{
					var dadosgravar={
						id: this.user.id,
						perfil: this.user.perfil,
						nome: user.nome,
						sobrenome: user.sobrenome,
						celular: user.celular,
						senha: user.senha,
						cpf: user.cpf,
						cnh: user.cnh,
						vecimento: user.vecimento,
					}
					localStorage.setItem('dados_usuario',JSON.stringify(dadosgravar));
					let toast = this.toastCtrl.create({
						message: 'Seu cadastro foi enviado para aprovação, aguarde a notificação com retorno',
						duration: 4500,
						position: 'bottom'
					});
					toast.present();
				}
			})
			.catch((error: any) => {
				console.log('error1',error);
			});
		}
	}

}
