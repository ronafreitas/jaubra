import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DadosCadastraisMotoristaPage } from './dados-cadastrais-motorista';

@NgModule({
  declarations: [
    DadosCadastraisMotoristaPage,
  ],
  imports: [
    IonicPageModule.forChild(DadosCadastraisMotoristaPage),
  ],
})
export class DadosCadastraisMotoristaPageModule {}
