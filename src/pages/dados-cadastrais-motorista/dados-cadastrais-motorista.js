var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Component } from '@angular/core';
import { IonicPage, AlertController, ToastController } from 'ionic-angular';
import { ApiProvider } from './../../providers/api/api';
import { Camera } from '@ionic-native/camera';
import { FileTransfer } from '@ionic-native/file-transfer';
var DadosCadastraisMotoristaPage = /** @class */ (function () {
    function DadosCadastraisMotoristaPage(alertCtrl, camera, transfer, toastCtrl, apiProvider) {
        this.alertCtrl = alertCtrl;
        this.camera = camera;
        this.transfer = transfer;
        this.toastCtrl = toastCtrl;
        this.apiProvider = apiProvider;
        this.user = { id: '', nome: '', sobrenome: '', celular: '', cpf: '', senha: '', cnh: '', endereco: '', vecimento: '', perfil: '' };
        this.passwordType = 'password';
        this.passwordIcon = 'eye-off';
    }
    DadosCadastraisMotoristaPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.user.id = JSON.parse(localStorage.getItem('dados_usuario')).id;
        this.user.nome = JSON.parse(localStorage.getItem('dados_usuario')).nome;
        this.user.sobrenome = JSON.parse(localStorage.getItem('dados_usuario')).sobrenome;
        this.user.celular = JSON.parse(localStorage.getItem('dados_usuario')).celular;
        this.user.cpf = JSON.parse(localStorage.getItem('dados_usuario')).cpf;
        this.user.senha = JSON.parse(localStorage.getItem('dados_usuario')).senha;
        //
        this.user.cnh = JSON.parse(localStorage.getItem('dados_usuario')).cnh;
        this.user.endereco = JSON.parse(localStorage.getItem('dados_usuario')).endereco;
        this.user.vecimento = JSON.parse(localStorage.getItem('dados_usuario')).vencimento_chn;
        this.user.perfil = 'mot';
        // MELHOR PEGAR LOGO TUDO NO LOGIN
        this.apiProvider.getDadosCadastrais(JSON.parse(localStorage.getItem('dados_usuario')).id)
            .then(function (result) {
            if (result[0]) {
                var dados = result[0];
                _this.user.cnh = parseInt(dados.cnh);
                _this.user.endereco = dados.endereco;
                _this.user.vecimento = dados.vencimento_chn;
            }
            //
        })
            .catch(function (error) {
            console.log('error1', error);
        });
    };
    DadosCadastraisMotoristaPage.prototype.isReadonly = function () {
        return true;
    };
    DadosCadastraisMotoristaPage.prototype.hideShowPassword = function () {
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
        this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    };
    DadosCadastraisMotoristaPage.prototype.fotocnh = function () {
        var options = {
            quality: 50,
            allowEdit: true,
            destinationType: this.camera.DestinationType.NATIVE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            saveToPhotoAlbum: false,
            targetWidth: 250,
            targetHeight: 400
        };
        this.captureCNH(options);
    };
    DadosCadastraisMotoristaPage.prototype.captureCNH = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.camera.getPicture(options)];
                    case 1:
                        result = _a.sent();
                        this.image_cnh = '1';
                        this.url_image_cnh = result;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.error(e_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    DadosCadastraisMotoristaPage.prototype.enviar = function () {
        //url_image_cnh:any;
        //url_image_resid:any;
        //url_image_antece:any;
        var _this = this;
        var fileTransfer = this.transfer.create();
        var fileopts = {
            fileKey: 'ionicfile',
            fileName: 'ionicfile',
            chunkedMode: false,
            mimeType: "image/jpeg",
            headers: {}
        };
        fileTransfer.upload(this.url_image_cnh, 'http://jaubra.faromidia.com.br/api_jaubra_uploads/index.php', fileopts)
            .then(function (data) {
            console.log("Uploaded Successfully");
            console.log(data);
            _this.url_image_cnh = '';
            //this.imageFileName = "http://192.168.0.7:8080/static/images/ionicfile.jpg"
            // this.presentToast("Image uploaded successfully");
        }, function (err) {
            console.log(err);
        });
    };
    DadosCadastraisMotoristaPage.prototype.fotocomres = function () {
        var options = {
            quality: 50,
            allowEdit: true,
            destinationType: this.camera.DestinationType.NATIVE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            saveToPhotoAlbum: false,
            targetWidth: 250,
            targetHeight: 400
        };
        this.captureResidencia(options);
    };
    DadosCadastraisMotoristaPage.prototype.captureResidencia = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.camera.getPicture(options)];
                    case 1:
                        result = _a.sent();
                        this.image_resid = '1';
                        this.url_image_resid = result;
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        console.error(e_2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    DadosCadastraisMotoristaPage.prototype.fotoantec = function () {
        var options = {
            quality: 50,
            allowEdit: true,
            destinationType: this.camera.DestinationType.NATIVE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            saveToPhotoAlbum: false,
            targetWidth: 250,
            targetHeight: 400
        };
        this.captureAntecedentes(options);
    };
    DadosCadastraisMotoristaPage.prototype.captureAntecedentes = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.camera.getPicture(options)];
                    case 1:
                        result = _a.sent();
                        this.image_antece = '1';
                        this.url_image_antece = result;
                        return [3 /*break*/, 3];
                    case 2:
                        e_3 = _a.sent();
                        console.error(e_3);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    DadosCadastraisMotoristaPage.prototype.salvar = function (user) {
        var _this = this;
        if (user.nome == '' ||
            user.sobrenome == '' ||
            user.celular == '' ||
            user.cpf == '' ||
            user.senha == '' ||
            user.cnh == '' ||
            user.endereco == '' ||
            user.vecimento == '') {
            var alert_1 = this.alertCtrl.create({
                title: 'Todos os campos são obrigatórios',
                buttons: ['Ok']
            });
            alert_1.present();
            return false;
        }
        else {
            this.apiProvider.updateDadosCadastrais(user)
                .then(function (result) {
                if (result.error) {
                    var toast = _this.toastCtrl.create({
                        message: 'Não foi possível atualizar cadastro, tente novamente',
                        duration: 3000,
                        position: 'bottom',
                        cssClass: "toasterro"
                    });
                    toast.present();
                }
                else {
                    var dadosgravar = {
                        id: _this.user.id,
                        perfil: _this.user.perfil,
                        nome: user.nome,
                        sobrenome: user.sobrenome,
                        celular: user.celular,
                        senha: user.senha,
                        cpf: user.cpf,
                        cnh: user.cnh,
                        vecimento: user.vecimento,
                    };
                    localStorage.setItem('dados_usuario', JSON.stringify(dadosgravar));
                    var toast = _this.toastCtrl.create({
                        message: 'Seu cadastro foi enviado para aprovação, aguarde a notificação com retorno',
                        duration: 4500,
                        position: 'bottom'
                    });
                    toast.present();
                }
            })
                .catch(function (error) {
                console.log('error1', error);
            });
        }
    };
    DadosCadastraisMotoristaPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-dados-cadastrais-motorista',
            templateUrl: 'dados-cadastrais-motorista.html',
        }),
        __metadata("design:paramtypes", [AlertController,
            Camera,
            FileTransfer,
            ToastController,
            ApiProvider])
    ], DadosCadastraisMotoristaPage);
    return DadosCadastraisMotoristaPage;
}());
export { DadosCadastraisMotoristaPage };
//# sourceMappingURL=dados-cadastrais-motorista.js.map