import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalNovoVeiculoPage } from './modal-novo-veiculo';

@NgModule({
  declarations: [
    ModalNovoVeiculoPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalNovoVeiculoPage),
  ],
})
export class ModalNovoVeiculoPageModule {}
