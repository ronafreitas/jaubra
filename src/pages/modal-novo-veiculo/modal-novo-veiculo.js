var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Component } from '@angular/core';
import { IonicPage, AlertController, ViewController } from 'ionic-angular';
import { ApiProvider } from './../../providers/api/api';
import { Camera } from '@ionic-native/camera';
import { FileTransfer } from '@ionic-native/file-transfer';
var ModalNovoVeiculoPage = /** @class */ (function () {
    function ModalNovoVeiculoPage(camera, transfer, apiProvider, viewCtrl, alertCtrl) {
        this.camera = camera;
        this.transfer = transfer;
        this.apiProvider = apiProvider;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
        this.carro = {
            descricao: '',
            ano: '',
            modelo: '',
            cor: '',
            renavam: '',
            chassi: '',
            placa: '',
        };
    }
    ModalNovoVeiculoPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ModalNovoVeiculoPage.prototype.salvar = function (carro) {
        var _this = this;
        if (carro.descricao == "" ||
            carro.ano == "" ||
            carro.modelo == "" ||
            carro.cor == "" ||
            carro.renavam == "" ||
            carro.chassi == "" ||
            carro.placa == "") {
            var alert_1 = this.alertCtrl.create({
                title: 'Todos os campos são obrigatórios.',
                buttons: ['Ok']
            });
            alert_1.present();
        }
        else {
            var ts = this;
            this.apiProvider.novoVeiculo(parseInt(JSON.parse(localStorage.getItem('dados_usuario')).id), carro)
                .then(function (result) {
                if (result.error) {
                    var alert_2 = _this.alertCtrl.create({
                        title: 'Não foi possível salvar, verifique e tente novamente',
                        buttons: ['Ok']
                    });
                    alert_2.present();
                }
                else {
                    //ts.navCtrl.setRoot('VeiculosPage');
                    ts.viewCtrl.dismiss({ confirmado: 'ok' });
                }
            })
                .catch(function (error) {
                console.log('error1', error);
            });
        }
    };
    ModalNovoVeiculoPage.prototype.fotodocum = function () {
        var options = {
            quality: 50,
            allowEdit: true,
            destinationType: this.camera.DestinationType.NATIVE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            saveToPhotoAlbum: false,
            targetWidth: 250,
            targetHeight: 400
        };
        this.captureCNH(options);
    };
    ModalNovoVeiculoPage.prototype.captureCNH = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.camera.getPicture(options)];
                    case 1:
                        result = _a.sent();
                        this.image_doc = '1';
                        this.url_image_doc = result;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.error(e_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ModalNovoVeiculoPage.prototype.enviar = function () {
        var _this = this;
        var fileTransfer = this.transfer.create();
        var fileopts = {
            fileKey: 'carro',
            fileName: 'documento',
            chunkedMode: false,
            mimeType: "image/jpeg",
            headers: {}
        };
        fileTransfer.upload(this.url_image_doc, 'http://jaubra.faromidia.com.br/api_jaubra_uploads/index.php', fileopts)
            .then(function (data) {
            console.log("Uploaded Successfully");
            console.log(data);
            _this.url_image_doc = '';
            //this.imageFileName = "http://192.168.0.7:8080/static/images/ionicfile.jpg"
            // this.presentToast("Image uploaded successfully");
        }, function (err) {
            console.log(err);
        });
    };
    ModalNovoVeiculoPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-modal-novo-veiculo',
            templateUrl: 'modal-novo-veiculo.html',
        }),
        __metadata("design:paramtypes", [Camera,
            FileTransfer,
            ApiProvider,
            ViewController,
            AlertController])
    ], ModalNovoVeiculoPage);
    return ModalNovoVeiculoPage;
}());
export { ModalNovoVeiculoPage };
//# sourceMappingURL=modal-novo-veiculo.js.map