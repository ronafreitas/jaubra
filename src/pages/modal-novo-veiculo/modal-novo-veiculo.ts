import { Component } from '@angular/core';
import { IonicPage,AlertController,ViewController } from 'ionic-angular';
import { ApiProvider } from './../../providers/api/api';
import { Camera, CameraOptions} from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

@IonicPage()
@Component({
  selector: 'page-modal-novo-veiculo',
  templateUrl: 'modal-novo-veiculo.html',
})
export class ModalNovoVeiculoPage {
	carro={
		descricao:'',
		ano:'',
		modelo:'',
		cor:'',
		renavam:'',
		chassi:'',
		placa:'',
	}

	image_doc:any;
	url_image_doc:any;

	constructor(
		public camera: Camera,
		private transfer: FileTransfer,
		private apiProvider:ApiProvider,
		public viewCtrl: ViewController,
		private alertCtrl: AlertController) {

	}

	dismiss(){
		this.viewCtrl.dismiss();
	}

	salvar(carro){
		if(
			carro.descricao == "" || 
			carro.ano == "" || 
			carro.modelo == "" || 
			carro.cor == "" || 
			carro.renavam == "" || 
			carro.chassi == "" || 
			carro.placa == ""
			){
				let alert = this.alertCtrl.create({
					title: 'Todos os campos são obrigatórios.',
					buttons: ['Ok']
				});
				alert.present();
		}else{
			var ts = this;
			this.apiProvider.novoVeiculo( parseInt(JSON.parse(localStorage.getItem('dados_usuario')).id), carro)
			.then((result: any) => {
				if(result.error){
					let alert = this.alertCtrl.create({
						title: 'Não foi possível salvar, verifique e tente novamente',
						buttons: ['Ok']
					});
					alert.present();
				}else{
					//ts.navCtrl.setRoot('VeiculosPage');
					ts.viewCtrl.dismiss({confirmado:'ok'}); 
				}
			})
			.catch((error: any) => {
				console.log('error1',error);
			});
		}
	}

	fotodocum(){
		const options: CameraOptions = {
			quality: 50,
			allowEdit: true,
			destinationType: this.camera.DestinationType.NATIVE_URI,
			encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE,
			correctOrientation: true,
			saveToPhotoAlbum: false,
			targetWidth: 250,
			targetHeight: 400
		}
		this.captureCNH(options);
	}

	async captureCNH(options: CameraOptions){
		try {
		  const result = await this.camera.getPicture(options)

		  this.image_doc ='1';
		  this.url_image_doc =result;

		  /* file:///storage/emulated/0/Android/data/br.jaubra.app/cache/1523739550324.jpg */
		}
		catch (e) {
		  console.error(e);
		}
	}

	enviar(){

		const fileTransfer: FileTransferObject = this.transfer.create();

		let fileopts: FileUploadOptions = {
			fileKey: 'carro',
			fileName: 'documento',
			chunkedMode: false,
			mimeType: "image/jpeg",
			headers: {}
		}

		fileTransfer.upload(this.url_image_doc, 'http://jaubra.faromidia.com.br/api_jaubra_uploads/index.php', fileopts)
		.then((data) => {
			console.log("Uploaded Successfully");
			console.log(data);
			this.url_image_doc='';
			//this.imageFileName = "http://192.168.0.7:8080/static/images/ionicfile.jpg"
			// this.presentToast("Image uploaded successfully");
		}, (err) => {
			console.log(err);
		});
	}

}
