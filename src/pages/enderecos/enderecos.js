var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, AlertController, ToastController } from 'ionic-angular';
import { ApiProvider } from './../../providers/api/api';
var EnderecosPage = /** @class */ (function () {
    function EnderecosPage(navCtrl, toastCtrl, modalCtrl, alertCtrl, apiProvider) {
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.apiProvider = apiProvider;
        this.address = [];
        this.defaulend = '';
        this.totalende = 0;
    }
    EnderecosPage.prototype.meusEnderecos = function () {
        var ts = this;
        this.apiProvider.getMeusEnderecos(parseInt(JSON.parse(localStorage.getItem('dados_usuario')).id))
            .then(function (result) {
            ts.address = [];
            result.forEach(function (re) {
                ts.totalende++;
                if (re.padrao == 1) {
                    ts.defaulend = re.id;
                }
                ts.address.push(re);
            });
        })
            .catch(function (error) {
            console.log('error1', error);
        });
    };
    EnderecosPage.prototype.ionViewDidLoad = function () {
        this.meusEnderecos();
    };
    EnderecosPage.prototype.addEndereco = function () {
        var _this = this;
        if (this.totalende == 3) {
            var alert_1 = this.alertCtrl.create({
                title: 'Você só pode inserir até 3 endereços',
                buttons: ['Ok']
            });
            alert_1.present();
        }
        else {
            //this.navCtrl.push('NovoEnderecoPage');
            var nven = this.modalCtrl.create('ModalNovoEnderecoPage');
            nven.present();
            nven.onDidDismiss(function (result) {
                if (result) {
                    //console.log(result);
                    if (result.confirmado == 'ok') {
                        _this.meusEnderecos();
                    }
                }
            });
        }
    };
    EnderecosPage.prototype.selecionarPadrao = function (ender) {
        var _this = this;
        this.apiProvider.padraoEndereco(ender, parseInt(JSON.parse(localStorage.getItem('dados_usuario')).id))
            .then(function (result) {
            if (result.error) {
                var toast = _this.toastCtrl.create({
                    message: 'Não foi possível gravar endereço padrão',
                    duration: 3000,
                    position: 'bottom',
                    cssClass: "toasterro"
                });
                toast.present();
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: 'Endereço padrão gravado',
                    duration: 2000,
                    position: 'bottom'
                });
                toast.present();
            }
        })
            .catch(function (error) {
            console.log('error1', error);
        });
    };
    EnderecosPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-enderecos',
            templateUrl: 'enderecos.html',
        }),
        __metadata("design:paramtypes", [NavController,
            ToastController,
            ModalController,
            AlertController,
            ApiProvider])
    ], EnderecosPage);
    return EnderecosPage;
}());
export { EnderecosPage };
//# sourceMappingURL=enderecos.js.map