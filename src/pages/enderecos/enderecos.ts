import { Component } from '@angular/core';
import { IonicPage, ModalController,NavController, AlertController,ToastController } from 'ionic-angular';
import { ApiProvider } from './../../providers/api/api';
@IonicPage()
@Component({
  selector: 'page-enderecos',
  templateUrl: 'enderecos.html',
})
export class EnderecosPage {

	address:any=[];
	defaulend:any='';
	totalende:number=0;
	constructor(public navCtrl: NavController, 
		private toastCtrl: ToastController,
		public modalCtrl: ModalController,
		private alertCtrl: AlertController,
		private apiProvider:ApiProvider) {

	}
	
	meusEnderecos(){
		var ts = this;
		this.apiProvider.getMeusEnderecos(parseInt(JSON.parse(localStorage.getItem('dados_usuario')).id))
		.then((result: any) => {
			ts.address=[]
		    result.forEach( (re) => {
		    	ts.totalende++;
		    	if(re.padrao == 1){
		    		ts.defaulend = re.id
		    	}
		    	ts.address.push(re);
		    });
		})
		.catch((error: any) => {
			console.log('error1',error);
		});
	}

	ionViewDidLoad(){
		this.meusEnderecos();
	}

	addEndereco(){
		if(this.totalende == 3){
			let alert = this.alertCtrl.create({
				title: 'Você só pode inserir até 3 endereços',
				buttons: ['Ok']
			});
			alert.present();
		}else{
			//this.navCtrl.push('NovoEnderecoPage');
			let nven = this.modalCtrl.create('ModalNovoEnderecoPage');
	    	nven.present();
			nven.onDidDismiss((result) =>{
				if(result){
					//console.log(result);
					if(result.confirmado == 'ok'){
						this.meusEnderecos();
					}
				}
			});
		}
	}

	selecionarPadrao(ender){
		this.apiProvider.padraoEndereco(ender, parseInt(JSON.parse(localStorage.getItem('dados_usuario')).id))
		.then((result: any) => {
			if(result.error){
				let toast = this.toastCtrl.create({
					message: 'Não foi possível gravar endereço padrão',
					duration: 3000,
					position: 'bottom',
					cssClass: "toasterro"
				});
				toast.present();
			}else{
				let toast = this.toastCtrl.create({
					message: 'Endereço padrão gravado',
					duration: 2000,
					position: 'bottom'
				});
				toast.present();
			}
		})
		.catch((error: any) => {
			console.log('error1',error);
		});
	}
}
