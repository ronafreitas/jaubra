var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, NgZone } from '@angular/core';
import { IonicPage, ViewController, AlertController, LoadingController } from 'ionic-angular';
import { ApiProvider } from './../../providers/api/api';
var ModalNovoEnderecoPage = /** @class */ (function () {
    function ModalNovoEnderecoPage(viewCtrl, zone, alertCtrl, loadingCtrl, apiProvider) {
        this.viewCtrl = viewCtrl;
        this.zone = zone;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.apiProvider = apiProvider;
        this.service = new google.maps.places.AutocompleteService();
        this.address = {
            place: ''
        };
        this.autocompleteItems = [];
        this.autocomplete = {
            query: ''
        };
    }
    ModalNovoEnderecoPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ModalNovoEnderecoPage.prototype.selecionaEndereco = function (item) {
        var ts = this;
        var address = item.descricao;
        var detalhes = item.detalhes;
        var geocoder = new google.maps.Geocoder();
        var enderecofinal = [];
        var lod = ts.loadingCtrl.create({
            content: 'Aguarde...'
        });
        lod.present();
        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                lod.dismiss();
                enderecofinal.push({ endereco: item.descricao, detalhes: detalhes, lat: results[0].geometry.location.lat(), lng: results[0].geometry.location.lng() });
                ts.alert = ts.alertCtrl.create({
                    title: 'Deseja salvar esse endereço?',
                    message: item.descricao,
                    buttons: [
                        {
                            text: 'Cancelar',
                            role: 'cancel',
                            handler: function () {
                            }
                        },
                        {
                            text: 'Salvar',
                            handler: function () {
                                var loading = ts.loadingCtrl.create({
                                    content: 'Aguarde...'
                                });
                                loading.present();
                                ts.apiProvider.gravaEnderecos(parseInt(JSON.parse(localStorage.getItem('dados_usuario')).id), enderecofinal)
                                    .then(function (result) {
                                    //console.log('end',enderecofinal);
                                    //console.log('result',result);
                                    loading.dismiss();
                                    ts.viewCtrl.dismiss({ confirmado: 'ok' });
                                })
                                    .catch(function (error) {
                                    console.log('error1', error);
                                });
                            }
                        }
                    ]
                });
                ts.alert.present();
            }
            else {
                var alert_1 = this.alertCtrl.create({
                    title: 'Não foi possível gravar esse endereço, tente novamente.',
                    buttons: ['Ok']
                });
                alert_1.present();
                console.log('erro');
            }
        });
    };
    ModalNovoEnderecoPage.prototype.buscaEndereco = function () {
        if (this.autocomplete.query == '') {
            this.autocompleteItems = [];
            return;
        }
        var me = this;
        this.service.getPlacePredictions({ input: this.autocomplete.query, componentRestrictions: { country: 'BR' } }, function (predictions, status) {
            me.autocompleteItems = [];
            me.zone.run(function () {
                predictions.forEach(function (prediction) {
                    me.autocompleteItems.push({ descricao: prediction.description, detalhes: prediction.terms });
                });
            });
        });
    };
    ModalNovoEnderecoPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-modal-novo-endereco',
            templateUrl: 'modal-novo-endereco.html',
        }),
        __metadata("design:paramtypes", [ViewController,
            NgZone,
            AlertController,
            LoadingController,
            ApiProvider])
    ], ModalNovoEnderecoPage);
    return ModalNovoEnderecoPage;
}());
export { ModalNovoEnderecoPage };
//# sourceMappingURL=modal-novo-endereco.js.map