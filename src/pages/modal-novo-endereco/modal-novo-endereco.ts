import { Component,NgZone } from '@angular/core';
import {  IonicPage, ViewController,AlertController,LoadingController } from 'ionic-angular';
declare let google;
import { ApiProvider } from './../../providers/api/api';

@IonicPage()
@Component({
  selector: 'page-modal-novo-endereco',
  templateUrl: 'modal-novo-endereco.html',
})
export class ModalNovoEnderecoPage {
	autocompleteItems;
	autocomplete;
	service = new google.maps.places.AutocompleteService();
	address:any;
	alert:any;

	constructor(
		public viewCtrl: ViewController, 
		private zone: NgZone,
		private alertCtrl: AlertController,
		public loadingCtrl: LoadingController,
		private apiProvider:ApiProvider) {

		this.address = {
			place: ''
		};

		this.autocompleteItems = [];
		this.autocomplete = {
			query: ''
		};
	}


	dismiss(){
		this.viewCtrl.dismiss();
	}

	selecionaEndereco(item: any){
		
		var ts = this;
		var address = item.descricao;
		var detalhes = item.detalhes;
		var geocoder = new google.maps.Geocoder();
		var enderecofinal = [];

		let lod = ts.loadingCtrl.create({
			content: 'Aguarde...'
		});
		lod.present()
		
		geocoder.geocode( { 'address': address}, function(results, status){
			if(status == google.maps.GeocoderStatus.OK){
				lod.dismiss();
				enderecofinal.push({endereco:item.descricao, detalhes:detalhes, lat:results[0].geometry.location.lat(), lng:results[0].geometry.location.lng()});
				ts.alert = ts.alertCtrl.create({
					title: 'Deseja salvar esse endereço?',
					message: item.descricao,
					buttons: [
						{
							text: 'Cancelar',
							role: 'cancel',
							handler: () => {
						  		
							}
						},
						{
							text: 'Salvar',
							handler: () => {
								let loading = ts.loadingCtrl.create({
									content: 'Aguarde...'
								});
								loading.present();

								ts.apiProvider.gravaEnderecos(
									parseInt(JSON.parse(localStorage.getItem('dados_usuario')).id),enderecofinal)
								.then((result: any) => {
									//console.log('end',enderecofinal);
									//console.log('result',result);
							  		loading.dismiss();
							  		ts.viewCtrl.dismiss({confirmado:'ok'}); 
								})
								.catch((error: any) => {
									console.log('error1',error);
								});
							}
						}
					]
				});
				ts.alert.present();
			}else{
				let alert = this.alertCtrl.create({
					title: 'Não foi possível gravar esse endereço, tente novamente.',
					buttons: ['Ok']
				});
				alert.present();	
			  console.log('erro');
			}
		});
	}

	buscaEndereco(){
		if(this.autocomplete.query == ''){
		  this.autocompleteItems = [];
		  return;
		}
		let me = this;
		this.service.getPlacePredictions({ input: this.autocomplete.query, componentRestrictions: {country: 'BR'} }, (predictions, status) => {
		  me.autocompleteItems = []; 
		  me.zone.run( () => {
		    predictions.forEach( (prediction) => {
		    	me.autocompleteItems.push({descricao:prediction.description, detalhes:prediction.terms});
		    });
		  });
		});
	}

}
