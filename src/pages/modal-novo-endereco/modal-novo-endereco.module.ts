import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalNovoEnderecoPage } from './modal-novo-endereco';

@NgModule({
  declarations: [
    ModalNovoEnderecoPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalNovoEnderecoPage),
  ],
})
export class ModalNovoEnderecoPageModule {}
