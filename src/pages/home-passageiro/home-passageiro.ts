import { Component,ViewChild,ElementRef,NgZone } from '@angular/core';
import { IonicPage,MenuController,NavController,ModalController,Platform,ViewController,LoadingController,ToastController,AlertController,Slides } from 'ionic-angular';
import firebase from 'firebase';
import { Geolocation } from '@ionic-native/geolocation';
declare var google: any;
import { ApiProvider } from './../../providers/api/api';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
@IonicPage()
@Component({
  selector: 'page-home-passageiro',
  templateUrl: 'home-passageiro.html',
})
export class HomePassageiroPage {
	@ViewChild('map') mapElement: ElementRef;
	@ViewChild(Slides) slides: Slides;
	map: any;
	autocompleteItems:any;
	autocomplete:any;
	mycontent:any;
	markers:any = [];
	iniciarbusca:boolean=false;
	service:any;
	address:any;
	alert:any;
	googleapi:any;
	placeIdOrigem:any;
	confirmachamada:boolean=false;
	clicameuslocais:boolean=false;
	onceMTatualiza:boolean=false;
	destinselec:boolean=true;
	aguardando_final:boolean=false;
	showmaps:boolean=false;
	origselec:boolean=true;
	chamadaefetuada:boolean=false;
	chamada_cancelada:boolean=true;
	chamando:boolean=false;
	info_motorista:boolean=false;
	onceCM:boolean=false;
	btn_atualizar:boolean=true;
	toast_aguardando_cancelar:boolean=false;
	opcpgm:any=[];
	imgiconinfoende_var:string='assets/icon/destino.png';
	textoinfoend_var:string='Digite e selecione o endereço de destino';
	meusenderecos:Array<{endereco:string, lat:any, long:any, id:string}>;
	tipoOrigemDestino:string='d';
	placeIdPesquisado:any;
	markerscarros:any;
	allmarkerscarros:any;
	marker_motorista_corrida_atual:any;
	directionsDisplay:any;
	indexOpcaoPgmt:any='0';
	itemOpcPgmSelec:any={
		tipo:'0',
		distancia:'',
		duracao:'',
		preco:'',
		pid_ori:'',
		pid_dest:''
	};
	curLatLng:any;
	distanciaRadius:number=3000; // metros
	setintv:any;
	alert_aguardar_passageiro:any;
	endereco_destino:string='';
	motorista_corrida:any;
	toast_corrida:any=null;
	toast_chamada:any=null;
	toast_ainda_chamada:any=null;
	fb;
	fb_chamada_aguardando:any=[];
	cidade_esta:string='';
	tempo_atualiza:number=1;
	lat_long_destino_escolhido:any;
	meuid:number;
	id_motorista_corrida:number;
	distProxPassa:number=0.031;// distancia do passageiro para encerra corrida, 0.031 = 31 metros
	alert_aguardando_iniciar:any;
	loading_iniciocorrida:any;
	minha_endereco_atual:any;
	modal_mensagem:any;
	pontuacao_motorista:string='4,8';
	pontuacao_carro:string='5,0';
	opcoes_carros = [];

	constructor(public navCtrl: NavController, 
		public menuCtrl: MenuController, 
		public platform: Platform,
		public modalCtrl: ModalController,
		private zone: NgZone,
		private toastCtrl: ToastController,
		private apiProvider:ApiProvider,
		private alertCtrl: AlertController,
		public viewCtrl: ViewController, 
		public loadingCtrl: LoadingController,
		private push: Push,
		private geolocation: Geolocation,
		public http: Http) {

		this.menuCtrl.enable(true, 'pas');
		this.menuCtrl.enable(false, 'mot');
		this.showmaps=true; 
		
		platform.ready().then(() => {
			
			/* DEV */

			//localStorage.setItem('busca_aguardando', 'false');
			//localStorage.removeItem('corrida_iniciada');
			//localStorage.setItem('id_motorista_corrida', JSON.stringify(0));

			if(!localStorage.getItem('busca_aguardando')){
				localStorage.setItem('busca_aguardando', 'false');
			}
			if(!localStorage.getItem('corrida_iniciada')){
				localStorage.setItem('corrida_iniciada', 'false');
			}
			if(!localStorage.getItem('id_fcm')){
				localStorage.setItem('id_fcm', '');
			}
			if( localStorage.getItem('corrida_iniciada') == 'true' ){
				this.origselec=false;
				this.btn_atualizar=false;
				this.aguardando_final=true;
				setTimeout(function(){
					var ovm = document.getElementById("overMaps");
					ovm.style.display='block';
				}, 500);
			}else{
				this.getLocation();
				if( localStorage.getItem('busca_aguardando') == 'true' ){
					//this.aguardaEntrarCarro();
					this.motoristaAguardandoPassageiroEntrarNoCarro();
				}else{
					this.atualizaVeiculos();
				}
			}		
			this.registraFCM();
		});
		this.motorista_corrida = {carro:'', nome:'', placa:''}
		this.autocompleteItems = [];
		this.autocomplete = {
			query_origem: '',
			query_destino: ''
		};

		//localStorage.removeItem('meus_locais'); localStorage.removeItem('meu_local_padrao');
		if(!localStorage.getItem('meus_locais')){
			localStorage.setItem('meus_locais', JSON.stringify([]));
		}
		if(!localStorage.getItem('meu_local_padrao')){
			localStorage.setItem('meu_local_padrao', JSON.stringify([]));
		}
		// através da localização atual, tenta filtra e coletar a 'cidade' e o 'estado'
		if(!localStorage.getItem('filtro_ce')){
			localStorage.setItem('filtro_ce', '');
		}

		if(!localStorage.getItem('corrida_atual')){
			localStorage.setItem('corrida_atual', JSON.stringify([]));
		}
		if(!localStorage.getItem('id_motorista_corrida')){
			localStorage.setItem('id_motorista_corrida', JSON.stringify(0));
		}

		this.fb = firebase.database();
		this.cidade_esta = 'ba/salvador';
		localStorage.setItem('cidade_esta', JSON.stringify(this.cidade_esta));

		this.meuid	= JSON.parse(localStorage.getItem('dados_usuario')).id;

	}

	ionViewDidLoad(){

		/*setTimeout(function(){
			if(document.getElementById('boxpgmt0')){
				document.getElementById('boxpgmt0').classList.add('forcefocus');
			}
		}, 300);*/
		let ts = this;

		if( localStorage.getItem('buscando_motorista') == 'true' ){

			if(ts.toast_chamada){
				ts.toast_chamada.dismiss();
			}

   			let toast_aguardando = ts.toastCtrl.create({
				message: 'Espere motorista confirmar',
				position: 'middle',
				cssClass:'toast_motorista_aguardando',
				showCloseButton:true,
				closeButtonText:'Cancelar',
			});
			toast_aguardando.present();
			ts.toast_chamada = toast_aguardando;

			setTimeout(function(){
				if(document.getElementById('overMaps')){
		        	var ovm = document.getElementById("overMaps");
					ovm.style.display='block';
				}
			}, 300);
			ts.confirmacaoCorrida();
   			//setTimeout(function(){

				toast_aguardando.onDidDismiss(() => {
					var ovm = document.getElementById("overMaps");
					ovm.style.display='none';
					// cancelado pelo fim da busca
					if(ts.toast_aguardando_cancelar){
						console.log('motorista xxxx');
					}else{
						console.log('VOCÊ CANCELOU A BUSCA');
						localStorage.setItem('buscando_motorista', 'false');
						localStorage.setItem('id_motorista_corrida', JSON.stringify(0));
						var chamdaguard = ts.fb.ref("chamadas/passageiro/"+ts.meuid);
				        chamdaguard.once('value', function(childSnapshot){
				        	var snap = childSnapshot.val();
				        	if(snap !== null){
					        	for(var d in snap){
					        		ts.fb.ref("chamadas/motoristas/"+snap[d].local_base+"/"+snap[d].motorista+"/"+ts.meuid).remove();
					        		ts.fb.ref("chamadas/passageiro/"+ts.meuid+"/"+d).remove();
					        	}
				        	}
							ts.info_motorista=false;
							ts.getLocation();
							//ts.atualizaVeiculos(null, null);
				        });
					}
					for(var c in ts.fb_chamada_aguardando){
						ts.fb_chamada_aguardando[c].remove();
					}
					ts.origselec=true;
					//ts.atualizaVeiculos(null, null);
				});
			//}, 300);

			setTimeout(function(){
				ts.destinselec = false;
				ts.origselec = false;
			}, 800);
		
		}else{
	        /*var buscaPasAguar = 'buscar_passageiro/'+ts.cidade_esta+"/"+ts.meuid+"/"+JSON.parse(localStorage.getItem('id_motorista_corrida'))+"/iniciada";
	        console.log(buscaPasAguar);
	       	ts.fb.ref(buscaPasAguar)
	       	.on('value', function(childSnapshot){
	       		var retn = childSnapshot.val();
	       		console.log('retn',retn);
	       		if(retn !== null){
	       			ts.atualizaLocalizacaoMotorista(retn,retn.latLng_motorista);
	       		}
	        });*/
		}
	}

/*





adaptar 'ba/salvador' em casos como por exemplo 'sp/são paulo'





*/
	
	getLocation(){
		let ts = this;
	  	this.geolocation.getCurrentPosition({enableHighAccuracy: true})
	    .then((position) => {
			let mylocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
			//let mylocation = new google.maps.LatLng(-23.544826,-46.636622);
			this.map = new google.maps.Map(this.mapElement.nativeElement,{
			  	zoom: 15,
			  	center: mylocation,
			  	disableDefaultUI: true,
			  	styles:[
					    {"featureType": "administrative","elementType": "all","stylers": [{"visibility": "off"}]},
					    {"featureType": "landscape","elementType": "all","stylers": [{"visibility": "off"}]},
					    {"featureType": "poi","elementType": "all","stylers": [{"visibility": "off"}]},
					    {"featureType": "road","elementType": "all","stylers": [{"visibility": "on"}]},
					    {"featureType": "transit","elementType": "all","stylers": [{"visibility": "off"}]}
					],
			  	mapTypeId: google.maps.MapTypeId.ROADMAP
			});
			//var latlng = {lat: -23.544826, lng: -46.636622};
			var latlng = {lat: position.coords.latitude, lng: position.coords.longitude};
			localStorage.setItem('LatitLongi', JSON.stringify(latlng));
			var meuslocs = JSON.parse(localStorage.getItem('meus_locais'));
			if(meuslocs.length < 1){
				var geocoder = new google.maps.Geocoder();
			    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
			        if(status == google.maps.GeocoderStatus.OK){
			            if(results[1]){
			            	ts.placeIdOrigem=results[1].place_id;
			            	ts.itemOpcPgmSelec.pid_ori=results[1].place_id;
			               	//ts.autocomplete.query_origem=results[1].formatted_address;
			               	ts.minha_endereco_atual=results[1];
							var nodoend = [];
							nodoend.push(
								{
									nome:results[1].formatted_address,
									endereco:results[1].formatted_address, 
									placeId:results[1].place_id, 
									detalhes:results[1].address_components, 
									lat:position.coords.latitude, 
									lng:position.coords.longitude,
									padrao:true
								}
							);
							//localStorage.setItem('meus_locais', JSON.stringify(nodoend));
			               	//var ll_frt = latlng.lat.toString()+','+latlng.lng.toString()
			               	//ts.filtraCidadeEstado(latlng.lat, latlng.lng, ll_frt, results[1].formatted_address);
			            }else{
			                alert('Não foi possível obter sua localização (e1)');
			            }
			        }else{
			            alert('Não foi possível obter sua localização (e2)');
			        }
			    });
			}else{
				for(var ml in meuslocs){
					if( meuslocs[ml].padrao == true ){
		               	ts.placeIdOrigem=meuslocs[ml].place_id;
		               	ts.autocomplete.query_origem=meuslocs[ml].nome;
					}
				}
			}

        	/*if(JSON.parse(localStorage.getItem('meu_local_padrao')).length < 1){

        	}else{
               	ts.placeIdOrigem=JSON.parse(localStorage.getItem('meu_local_padrao')).place_id;
               	ts.autocomplete.query_origem=JSON.parse(localStorage.getItem('meu_local_padrao')).nome;
        	}*/

        	this.curLatLng={lat:position.coords.latitude, lng:position.coords.longitude}
			this.deleteMarkers();
			let image = 'assets/imgs/perfil_usuario.png';
			this.addMarker(mylocation,image);
			this.setMapOnAll(this.map);
			this.origselec=true;
	    })
	    .catch(
	      (err) => {
	        console.log('nao foi possivel pegar localizacao', err);
	    });
	}

	latLngByPlaceId(placeId){
		let ts = this;
		var geocoder = new google.maps.Geocoder();
		geocoder.geocode({'placeId': placeId}, function(results, status) {
		  if(status === 'OK'){
		    if(results[0]){
		    	ts.lat_long_destino_escolhido = {lat:results[0].geometry.location.lat(),lng:results[0].geometry.location.lng()};
		    	//console.log('ok',ts.lat_long_destino_escolhido);
		    }else{
		    	console.log('erro-1');
		    }
		  }else{
		  	console.log('erro-2');
		  }
		});
	}

	/*opcaoPagamento(item,i){
		document.getElementById('boxpgmt0').classList.remove('forcefocus');
		document.getElementById('boxpgmt'+this.indexOpcaoPgmt).classList.remove('forcefocus');
		document.getElementById('boxpgmt'+i).classList.add('forcefocus');
		this.indexOpcaoPgmt = i;
		this.itemOpcPgmSelec.preco=item.preco;
		this.itemOpcPgmSelec.tipo=i;
	}*/

	escolherEndereco(item:any){
		let ts = this;
		var address = item.descricao;
		ts.endereco_destino = address;
		var geocoder = new google.maps.Geocoder();
		let lod = ts.loadingCtrl.create({content: 'Aguarde...'});
		lod.present();
		geocoder.geocode({ 'address': address}, function(results, status){
			if(status == google.maps.GeocoderStatus.OK){
				ts.zone.run(() => {
					//ts.opcpgm = []
					var opcoes = [
						'assets/imgs/icone_eco.png',
						'assets/imgs/icone_plus.png',
						'assets/imgs/icone_master.png',
						'assets/imgs/icone_family.png',
					];
					var txtopt = ['Eco', 'Plus', 'Master', 'Family'];
					var precos = ['10,50', '12,68', '16,40', '17,35'];
					for(var p in opcoes){
						ts.opcoes_carros.push({
							texto:txtopt[p],
							img:opcoes[p],
							preco_label: 'R$ '+precos[p],
							preco: precos[p]
						});
					}
					ts.autocomplete.query_destino = item.descricao;
					ts.itemOpcPgmSelec.pid_dest = item.placeId;
					lod.dismiss();
					if(ts.autocomplete.query_origem != "" && ts.autocomplete.query_destino != ""){
						ts.iniciarbusca = false;
						ts.placeIdPesquisado = item.placeId;
						ts.latLngByPlaceId(item.placeId);

						if(JSON.parse(localStorage.getItem('meu_local_padrao')).length < 1){
							var meuslocs = JSON.parse(localStorage.getItem('meus_locais'));
							for(var ml in meuslocs){
								if( meuslocs[ml].padrao == true ){
									ts.rota(meuslocs[ml].placeId, item.placeId);
								}
							}
						}else{
							ts.rota(JSON.parse(localStorage.getItem('meu_local_padrao')).placeId, item.placeId);
						}
						/*setTimeout(function(){
							ts.itemOpcPgmSelec.preco = ts.opcpgm[0].preco;
							if(document.getElementById('boxpgmt0')){
								document.getElementById('boxpgmt0').classList.add('forcefocus');
							}
						}, 300);*/
					}
				});
			}else{
				alert('ops! erro inesperado');
			}
		});
	}
 
	meuslocais(){
		let ts = this;
		let myModal = ts.modalCtrl.create('ModalMeuEnderecoPage');
    	myModal.present();
		myModal.onDidDismiss((result) =>{
			if(result){
				ts.iniciarbusca = false;
				ts.autocomplete.query_origem=result.nome;
				ts.placeIdOrigem=result.placeId;
				if(result.placeId != "" && typeof ts.placeIdPesquisado != "undefined"){
					if(ts.autocomplete.query_destino != ""){
						ts.rota(result.placeId, ts.placeIdPesquisado);
					}else{
					    if(ts.directionsDisplay) {
					        ts.directionsDisplay.setMap(null);
					        ts.directionsDisplay = null;
					    }
					}
				}
			}else{
				ts.autocomplete.query_origem = '';
				if(ts.directionsDisplay) {
					ts.directionsDisplay.setMap(null);
					ts.directionsDisplay = null;
				}
				ts.confirmachamada=false;
			}
		});
	}

	procuraMotoristas(ts){

		var mloc = JSON.parse(localStorage.getItem('meus_locais'));
		for(var m in mloc){
			if( mloc[m].padrao == true ){
				// busca motoristas
				for(var m in mloc){
					if( mloc[m].padrao == true ){

						var end_pd = mloc[m].endereco;
						var lat_pd = mloc[m].lat;
						var lng_pd = mloc[m].lng;

				        ts.fb.ref("motoristas/"+ts.cidade_esta)
				        .once('value', function (childSnapshot){
				        	var snap = childSnapshot.val();
				        	if(snap !== null){
				        		for(var p in snap){
				        			if(snap[p].status){
					        			var checkdist = ts.calculaDistanciaRadius(
					        				lat_pd,
					        				lng_pd,
					        				snap[p].lat,
					        				snap[p].lng
					        			); 
					        			if(checkdist <= ts.distanciaRadius){
											ts.markerscarros = new google.maps.Marker({
												position: new google.maps.LatLng(snap[p].lat, snap[p].lng),
												map: ts.map,
												icon: {
													url: 'assets/imgs/carro_jaubra.png',
													//scaledSize: new google.maps.Size(40, 40)
												}
											});
											var idpassa = ts.meuid;
											var idmoto  = snap[p].id;
											var chmds = ts.fb.ref('chamadas/motoristas/'+ts.cidade_esta+"/"+idmoto+"/"+idpassa);
											chmds.set({
												solicitante: idpassa,
												latlong_passageiro: {lat:lat_pd,lng:lng_pd},// local onde estará o passageiro da corrida, não necessariamente a mesma localização do usuário q solicita
												latlong_motorista: {lat:snap[p].lat,lng:snap[p].lng},// localização do(s) motorista(s) encontrado(s)
												latlong_parada_final: ts.lat_long_destino_escolhido,// após motorista pegar passageiro, esse será destino final
												origem:end_pd,
												destino:ts.endereco_destino,
												distancia:checkdist
											}); 
											ts.fb_chamada_aguardando.push(chmds);
											ts.fb.ref('chamadas/passageiro/'+idpassa).push({
												motorista: idmoto,
												local_base:ts.cidade_esta
											});
									        ts.fb.ref("usuarios_fcm/"+idmoto+"/fcmId")
									        .once('value', function (childSnapshot){
									        	var snap = childSnapshot.val();
									        	if(snap !== null){
									        		ts.enviaPush(snap, "Passageiro solicita corrida", "Chamada solicitada");
									        	}
									        });
					        			}
					        		}
				        		}
								ts.chamando=false;
				        		ts.chamada_cancelada=false;
				        		ts.confirmacaoCorrida();
				        	}else{
								ts.toastCtrl.create({
									message: 'Nenhum motorista encontrado para essa região',
									position: 'middle',
									duration:4000
								}).present();
				        	}
				        });
					}
				}//end for 2
			}
		}// end for 1
	}

	efetuaChamada(){ 
		let ts = this;
		let indexSlideCarro = this.slides.getActiveIndex();
		ts.itemOpcPgmSelec.preco = ts.opcoes_carros[indexSlideCarro].preco
		var opcaoescolhida = {data:ts.itemOpcPgmSelec, carro:ts.opcoes_carros[indexSlideCarro]};
		let myModal = ts.modalCtrl.create('ModalEscolherPagamentoPage', opcaoescolhida);
    	myModal.present();
		myModal.onDidDismiss((result) =>{
			if(result){
				ts.zone.run(() => {
					ts.confirmachamada = false;
					clearInterval(ts.setintv); // cancela a busca por motoristas no mapa atual antes de efetuar um chamada

					if(ts.directionsDisplay) {
						ts.directionsDisplay.setMap(null);
					}
			        ts.destinselec=false;
			        ts.origselec=false;
			        ts.chamadaefetuada=true;
			        ts.chamando=false;
					ts.deleteMarkers();

					localStorage.setItem('buscando_motorista', 'true');

		   			let toast_aguardando = ts.toastCtrl.create({
						message: 'Espere motorista confirmar',
						position: 'middle',
						cssClass:'toast_motorista_aguardando',
						showCloseButton:true,
						closeButtonText:'Cancelar',
					});
					toast_aguardando.present();
					ts.toast_chamada = toast_aguardando;

		        	var ovm = document.getElementById("overMaps");
					ovm.style.display='block';
					ts.procuraMotoristas(ts);

					toast_aguardando.onDidDismiss(() => {
						var ovm = document.getElementById("overMaps");
						ovm.style.display='none';
						// cancelado pelo fim da busca
						if(ts.toast_aguardando_cancelar){
							setTimeout(function(){
						        ts.destinselec=false;
						        ts.origselec=false;
							}, 800);
							//console.log('motorista cancelou??');
						}else{
							console.log('passageiro cancelou a busca');
							localStorage.setItem('buscando_motorista', 'false');
							localStorage.setItem('id_motorista_corrida', JSON.stringify(0));
							var chamdaguard = ts.fb.ref("chamadas/passageiro/"+ts.meuid);
					        chamdaguard.once('value', function(childSnapshot){
					        	var snap = childSnapshot.val();
					        	if(snap !== null){
						        	for(var d in snap){
						        		ts.fb.ref("chamadas/motoristas/"+snap[d].local_base+"/"+snap[d].motorista+"/"+ts.meuid).remove();
						        		ts.fb.ref("chamadas/passageiro/"+ts.meuid+"/"+d).remove();
						        	}
					        	}
								ts.info_motorista=false;
								ts.getLocation();
								ts.atualizaVeiculos();
					        });
						}

						for(var c in ts.fb_chamada_aguardando){
							ts.fb_chamada_aguardando[c].remove();
						}
						ts.origselec=true;
					});
					
				});
			}
		});
	}

	// passageiro recebe a confirmação do motorista
	confirmacaoCorrida(){
		let ts = this;
		var buscaPas = 'buscar_passageiro/'+ts.cidade_esta+"/"+ts.meuid;
       	ts.fb.ref(buscaPas)
       	.once('child_added', function(childSnapshot){

       		var snap = childSnapshot.val();
       		localStorage.setItem('buscando_motorista', 'false');
    		//ts.zone.run(() => {
				let toast = ts.toastCtrl.create({
					message: 'O motorista está a caminho',
					position: 'top',
					//cssClass:'toast_motorista_run',
					duration: 3500
				});
				toast.present();
				ts.toast_corrida = toast;

    			ts.id_motorista_corrida = snap.iniciada.motorista;
    			localStorage.setItem('corrida_atual', JSON.stringify(snap.iniciada));
    			//console.log('verificar se os dados de corrida_atual estão corretos, ver em atualizaLocalizacaoMotorista ');
    			localStorage.setItem('id_motorista_corrida', JSON.stringify(ts.id_motorista_corrida));
				ts.apiProvider.dadosMotoristaCorrida(ts.id_motorista_corrida)
				.then((result: any) => {
					if(result.dados){
						var motst = result.dados[0];
						//ts.motorista_corrida.carro = motst.descricao+' '+motst.cor;
						ts.motorista_corrida.carro = motst.descricao;
						ts.motorista_corrida.nome = motst.nome;
						ts.motorista_corrida.celular = motst.celular;
						ts.motorista_corrida.placa = motst.placa;
					}
				}).catch((error: any) => {
					console.log('error1',error);
				});

				if(ts.toast_chamada){
					setTimeout(function(){
						if(ts.toast_chamada){
							ts.toast_chamada.dismiss();
						}
						ts.toast_chamada=null;
					}, 300);
					
					ts.toast_aguardando_cancelar=true;
				}

				ts.info_motorista = true;
				ts.motoristaAcaminhoEncontrarPassageiro(snap.iniciada);
				ts.destinselec=false;
				ts.origselec=false;
				ts.chamando=false;
	    		ts.chamada_cancelada=false;

	    		ts.passageiroObservaAguardaMotoristaBuscar();

		       	ts.fb.ref(buscaPas+"/"+snap.iniciada.motorista+"/cancelado_motorista")
		       	.on('value', function(childSnapshot){
		       		var snap = childSnapshot.val();
		       		if(ts.onceCM==false){
			       		if(snap != null){
			       			localStorage.setItem('id_motorista_corrida', JSON.stringify(0));
			       			ts.info_motorista=false;
			       			localStorage.setItem('buscando_motorista', 'false');
							let alert = ts.alertCtrl.create({
							  title: 'Corrida cancelada',
							  message: 'Por alguma razão o motorista cancelou a corrida',
							  buttons: [
							  {text: 'Ok',role: 'ok',
							    handler: () => {
									ts.getLocation();
									ts.atualizaVeiculos();
									setTimeout(function(){
										ts.onceCM=false;
									}, 300);
							    }
							  }
							  ],
							  enableBackdropDismiss: false
							});
							alert.present();
							ts.onceCM=true;
						}
						if(ts.modal_mensagem != null){
							ts.modal_mensagem.dismiss();
						} 
		       		}
		        });
		    //});
        });
	}
	
	passageiroObservaAguardaMotoristaBuscar(){
		let ts = this;
		var idMotorista = localStorage.getItem('id_motorista_corrida');
       	var buscaPasAguar = 'buscar_passageiro/'+ts.cidade_esta+"/"+ts.meuid+"/"+idMotorista+"/iniciada";
        ts.alert_aguardar_passageiro = setInterval(function(){

	       	ts.fb.ref(buscaPasAguar)
	       	.once('value', function(childSnapshot){
	       		var retn = childSnapshot.val();
	       		if(retn !== null){

					if(ts.marker_motorista_corrida_atual){
						ts.marker_motorista_corrida_atual.setMap(null);
					}

					ts.marker_motorista_corrida_atual = new google.maps.Marker({
						position: retn.latLng_motorista,
						map: ts.map,
						icon: {
							url: 'assets/imgs/carro_jaubra.png',
							//scaledSize: new google.maps.Size(40, 40)
						}
					});

					ts.criaRotaDoMotorista(retn.latLng_motorista, retn.dados_corrida.latlong_passageiro);

	       			//dadosCorrida = retn;
	       		}else{
	       			localStorage.setItem('busca_aguardando', 'true');
					clearInterval(ts.alert_aguardar_passageiro);		
					ts.motoristaAguardandoPassageiroEntrarNoCarro();
	       			//console.log('/iniciada é apagada pq a API detectou a proximidade');
	       		}
	        });
        }, 2000);
	}

	// 
	motoristaAguardandoPassageiroEntrarNoCarro(){
		let ts = this;
		if( localStorage.getItem('id_motorista_corrida') ){
			if( localStorage.getItem('id_motorista_corrida') != '0'){
				
				var idMotorista = localStorage.getItem('id_motorista_corrida');

				var buscaPas = 'buscar_passageiro/'+ts.cidade_esta+"/"+ts.meuid+"/"+idMotorista;
		       	ts.fb.ref(buscaPas+"/aguardando")
		       	.once('value', function(childSnapshot){

		       		var snap = childSnapshot.val();
		       		if(snap !== null){
						//localStorage.setItem('corrida_atual', JSON.stringify(dadosCorrida));
						//localStorage.setItem('aguarda_iniciar', 'true');
					    localStorage.setItem('buscando_motorista', 'false');
						ts.fb.ref('buscar_passageiro/'+ts.cidade_esta+"/"+ts.meuid+"/"+idMotorista+"/iniciada").remove();

					    let alert = ts.alertCtrl.create({
					      title: 'Motorista chegou',
					      message: 'Você já está dentro do veículo?',
					      buttons: [
						      {
						        text: 'Sim',
						        role: 'sim',
						        handler: () => {
									
									//localStorage.setItem('aguarda_iniciar', 'false');
									localStorage.setItem('busca_aguardando', 'false');

							        ts.fb.ref("usuarios_fcm/"+idMotorista+"/fcmId")
							        .once('value', function (childSnapshot){
							        	var psh = childSnapshot.val();
							        	if(psh !== null){
							        		ts.enviaPush(psh, "Passageiro confirma", "Você já pode iniciar a corrida");
							        	}
							        });

									ts.permitirInicioCorrida(idMotorista);
									/*setTimeout(function(){
										ts.onceMTatualiza = true;
									}, 300);*/
									ts.loading_iniciocorrida = ts.loadingCtrl.create({content: 'Aguardando motorista iniciar...'});
									ts.loading_iniciocorrida.present();
						        }
						      },
						      {
						        text: 'Ainda aguardando',
						        role: 'aguardando',
						        handler: () => {
									if(ts.alert_aguardando_iniciar){
										ts.alert_aguardando_iniciar.dismiss();
									}
						            ts.motoristaAguardandoPassageiroEntrarNoCarro();
						            //ts.aguardaEntrarCarro();
									/*setTimeout(function(){
										ts.onceMTatualiza = true;
									}, 300);*/
						        }
						      }
					      ],
					      enableBackdropDismiss: false
					    });
					    alert.present();
		       		}
		        });
			}
		}
	}

	motoristaAcaminhoEncontrarPassageiro(dadosCorrida){
		let ts = this;
		ts.deleteMarkers();
		if(ts.markerscarros){
			ts.markerscarros.setMap(null);
		}
		//ts.markerscarros = [];

		if(ts.markers){
			ts.markers=[];
		}

		if(ts.allmarkerscarros){
	        for(var i = 0; i < ts.allmarkerscarros.length; i++){
	        	ts.allmarkerscarros[i].setMap(null);
	        }
		}

		if(ts.allmarkerscarros){
	        for(var i = 0; i < ts.allmarkerscarros.length; i++){
	        	ts.allmarkerscarros[i].setMap(null);
	        }
		}
		ts.allmarkerscarros = [];

		if(ts.markers){
	        for(var i = 0; i < ts.markers.length; i++){
	        	ts.markers[i].setMap(null);
	        }
		}
		ts.markers=[];
		ts.chamando=false;


		if(ts.marker_motorista_corrida_atual){
			ts.marker_motorista_corrida_atual.setMap(null);
		}
		// adiciona marker do motorista
        var ltlngmot = {lat:dadosCorrida.latLng_motorista.lat, lng: dadosCorrida.latLng_motorista.lng};
	    ts.marker_motorista_corrida_atual =  new google.maps.Marker({
	        position:ltlngmot,
	        map: this.map,
	        icon:{
				url: 'assets/imgs/carro_jaubra.png',
				//scaledSize: new google.maps.Size(40, 40)
			}
	    });
 
 		// adiciona marker do passageiro
	    var ltlndff = {lat:dadosCorrida.dados_corrida.latlong_passageiro.lat, lng: dadosCorrida.dados_corrida.latlong_passageiro.lng};
		//var newpos = new google.maps.LatLng(dadosCorrida.dados_corrida.latlong_passageiro.lat,dadosCorrida.dados_corrida.latlong_passageiro.lng);
		new google.maps.Marker({
			position: ltlndff,
			map: ts.map,
			icon: {
				url: 'assets/imgs/perfil_usuario.png',
				//scaledSize: new google.maps.Size(40, 40)
			}
		});
		//ts.map.setCenter(newpos);
		ts.map.setCenter(ltlndff);
		ts.map.setZoom(15);
		localStorage.setItem('corrida_atual', JSON.stringify(dadosCorrida));

		//ts.simularChegadaDoMotorista(ltlngmot, ltlndff);
	}

	aguardaEntrarCarro(){
		let ts = this;
		var dadosCorrida = JSON.parse(localStorage.getItem('corrida_atual'));
		var idMotorista = dadosCorrida.motorista;
		setTimeout(function(){
		    ts.alert_aguardando_iniciar = ts.alertCtrl.create({
		      title: 'Motorista aguardando',
		      message: 'Desloque-se até o motorista ou envie uma mensagem.',
		      buttons: [
		      {text: 'Ok, entrei no carro',role: 'sim',handler: () => {localStorage.setItem('aguarda_iniciar', 'false'); ts.permitirInicioCorrida(idMotorista);}},
		      {text: 'Continuar aguardando',role: 'aguardando',handler: () => {ts.aguardaEntrarCarro();}},
		      {text: 'Mensagem',role: 'mensagem',handler: () => {ts.msgParaMorotista();}}
		      ],
		      enableBackdropDismiss: false
		    });
		    ts.alert_aguardando_iniciar.present();
		}, 300);
	}

	permitirInicioCorrida(idMotorista){
		let ts = this;
		var buscaPas = 'buscar_passageiro/'+ts.cidade_esta+"/"+ts.meuid+"/"+idMotorista;
		ts.fb.ref(buscaPas+"/finalizada").set(true);
		ts.fb.ref(buscaPas+"/aguardando").remove();

		//ts.fb.ref(buscaPas+"/finalizada").remove(); 
		// quem tem q apagar tudo é o morotista na hora q confirma corrida_iniciada

		var dadosCorrida = JSON.parse(localStorage.getItem('corrida_atual'));
		var corriInit = 'corrida_iniciada/'+ts.cidade_esta+"/"+dadosCorrida.motorista+"/"+ts.meuid;
		var novaCorrida = ts.fb.ref(corriInit);
		novaCorrida.on('child_added', function(childSnapshot){
			var snap = childSnapshot.val();
			if(snap != null){
				localStorage.setItem('corrida_iniciada', 'true');

				ts.toastCtrl.create({
					message: 'Corrida iniciada, boa viagem.',
					position: 'middle',
					duration:3500
				}).present();

				setTimeout(function(){
					ts.origselec=false;
					ts.btn_atualizar=false;
					ts.aguardando_final=true;
					ts.info_motorista=false;
					var ovm = document.getElementById("overMaps");
					ovm.style.display='block';
				}, 3700);

				if(ts.loading_iniciocorrida){
					setTimeout(function(){
						if(ts.loading_iniciocorrida){
							ts.loading_iniciocorrida.dismiss();
						}
						ts.loading_iniciocorrida=null;
					}, 300);
				}
			} 
		});
	}

	criaRotaDoMotorista(latlng1, latlng2){
		let ts = this;
		var directionsService = new google.maps.DirectionsService;
	    if(ts.directionsDisplay) {
	        ts.directionsDisplay.setMap(null);
	        ts.directionsDisplay = null;
	    }
    	ts.directionsDisplay = new google.maps.DirectionsRenderer();
		directionsService.route({
          origin: latlng1,
          destination: latlng2,
		  //optimizeWaypoints: true,
		  travelMode: 'DRIVING'
		  /*,drivingOptions: {
		    departureTime: new Date(Date.now()),  // for the time N milliseconds from now.
		    trafficModel: 'pessimistic'
		  }*/
		}, function(response, status){
		  if(status === 'OK'){
		  	ts.itemOpcPgmSelec.distancia = response.routes[0].legs[0].distance.text;
		  	ts.itemOpcPgmSelec.duracao = response.routes[0].legs[0].duration.text;
		  	//console.log('distancia-2',response.routes[0].legs[0].distance.text);
		  	//console.log('duração-2',response.routes[0].legs[0].duration.text);
			for(var j = 0; j < response.routes.length; j++){
				ts.directionsDisplay.setMap(ts.map);
				ts.directionsDisplay.setDirections(response); 
				ts.directionsDisplay.setRouteIndex(j);
				ts.directionsDisplay.setOptions({
					draggable: false,
					hideRouteIndex: true,
					polylineOptions : {
						strokeColor: "#29a3a9",
						//strokeColor: colours[j],
						strokeOpacity: 0.9,
						strokeWeight: 4
					}
				});
			}
			
			//console.log('inicia movimentoCarro');
			//ts.movimentoCarro(response, latlng2);
		  }else{
		  	console.log('erroRorre');
		  	console.error(status);
		  }
		});
    }


	getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
	  var R = 6371; // Radius of the earth in km
	  var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
	  var dLon = this.deg2rad(lon2-lon1); 
	  var a = 
	    Math.sin(dLat/2) * Math.sin(dLat/2) +
	    Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) * 
	    Math.sin(dLon/2) * Math.sin(dLon/2); 
	  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
	  var d = R * c; // Distance in km
	  return d;
	}

	deg2rad(deg) {
	  return deg * (Math.PI/180)
	}

	distanciaCorrida(lat1, lng1, lat2, lng2){
		var dts = this.getDistanceFromLatLonInKm( lat2, lng2,lat1, lng1);
		return Number((dts).toFixed(3));
	}

	cancelarBusca(){
	    let alert = this.alertCtrl.create({
	      title: 'Cancelar corrida',
	      message: 'Deseja mesmo cancelar?',
	      buttons: [
	      {
	        text: 'Não',
	        role: 'cancel',
	        handler: () => {
	            console.log('aguardando motorista chegar');
	        }
	      },
	      {
	        text: 'Sim',
	        handler: () => {

	        	let ts = this;
	        	//var buscaPas = 'buscar_passageiro/'+ts.cidade_esta+"/"+ts.meuid;
	        	ts.fb.ref('buscar_passageiro/'+ts.cidade_esta+"/"+ts.meuid+"/"+ts.id_motorista_corrida+"/cancelado_passageiro").set(true);
				setTimeout(function(){
					ts.fb.ref('buscar_passageiro/'+ts.cidade_esta+"/"+ts.meuid).remove();
				}, 300);
		        var motoBucs = 'motoristas/'+ts.cidade_esta+"/"+ts.id_motorista_corrida+"/busca";
				ts.fb.ref(motoBucs).remove();

	        	localStorage.setItem('buscando_motorista', 'false');
	        	//localStorage.setItem('aguarda_iniciar', 'false');
	        	localStorage.setItem('busca_aguardando', 'false');
	        	localStorage.setItem('id_motorista_corrida', JSON.stringify(0));
				if(ts.toast_corrida){
					//ts.toast_corrida.dismiss();
				}
				
				ts.info_motorista=false;
				ts.getLocation();
				ts.atualizaVeiculos();

		        ts.fb.ref("usuarios_fcm/"+ts.id_motorista_corrida+"/fcmId")
		        .once('value', function (childSnapshot){
		        	var snap = childSnapshot.val();
		        	if(snap !== null){
		        		ts.enviaPush(snap, "Passageiro cancelou corrida", "Corrida cancelada");
		        	}
		        });
				console.log('corrida cancelada');

				var idmotochat = JSON.parse(localStorage.getItem('corrida_atual')).motorista;
				ts.fb.ref('chat/'+JSON.parse(localStorage.getItem('dados_usuario')).id+'-'+idmotochat).remove();
				ts.fb.ref('chat/'+idmotochat+'-'+JSON.parse(localStorage.getItem('dados_usuario')).id).remove();

	        }
	      }
	      ]
	    });
	    alert.present();
	}

    movimentoCarro(res, latlngdest){
	    var path = res.routes[0].overview_path;
	    console.log('movimentoCarro',res);
	    //console.log(path);
	    var maxIter = path.length;
	    var taxiCab = new google.maps.Marker({
	        position: path[0],
	        map: this.map,
	        icon:{
				url: 'assets/imgs/carro_jaubra.png',
				//scaledSize: new google.maps.Size(40, 40)
			}
	    });
	    //var ts = this;
	    var delay = 1500, count = 0;
	    function delayed(){
	      count += 1;
	      taxiCab.setPosition({lat:path[count].lat(),lng:path[count].lng()});
	      //console.log(count , maxIter);
	      //console.log(path[count].lat() +","+ path[count].lng());
	      //ts.fimDaCorrida(latlngdest.lat, latlngdest.lng, path[count].lat(), path[count].lng());
	      if(count < maxIter-1){
	        setTimeout(delayed, delay);
	      }
	    }
	    delayed();
    }

	msgParaMorotista(){
		let ts = this;
		if(ts.toast_corrida){
			ts.toast_corrida.dismiss();
		}
		var dadosCorrida = JSON.parse(localStorage.getItem('corrida_atual'));
		ts.modal_mensagem = ts.modalCtrl.create('ModalChatPage', {tipo:'pas',id_usuario:dadosCorrida.motorista});
    	ts.modal_mensagem.present();
		ts.modal_mensagem.onDidDismiss((result) =>{
			if(result){
				console.log('chat onDidDismiss',result);
			}
		});
	}

	ligarParaMotorista(numero){
		console.log(numero);
	}

	filtraCidadeEstado(lat, lng, latlong:string, endereco:string){
		let ts = this;
		this.apiProvider.filtaLatLong(latlong)
		.then((result: any) => {
			result.forEach( (re) => {
				var cidade = endereco.search(re.MUNICIPIO);
				if(cidade !== -1){
					var cidest = re.UF.toLowerCase()+'/'+re.MUNICIPIO.toLowerCase();
					localStorage.setItem('filtro_ce', cidest);
					//ts.cidade_esta

					if(JSON.parse(localStorage.getItem('dados_usuario')).perfil == 'mot'){
						this.fb.ref('motoristas/'+cidest+"/"+ts.meuid)
						.set({
							id:ts.meuid,
							lat: lat,
							lng: lng
						});
					}
				}
			});
		})
		.catch((error: any) => {
			console.log('error1',error);
		});
	}

	calculaDistanciaRadius(lat1, long1, lat2, long2){
		var setlat1;
		var setlong1;
		if(lat1 == null){
			setlat1 = JSON.parse( localStorage.getItem('LatitLongi') ).lat;
		}else{
			setlat1 = lat1;
		}
		if(long1 == null){
			setlong1 = JSON.parse( localStorage.getItem('LatitLongi') ).lng;
		}else{
			setlong1 = long1;
		}

		//this.calculateDistances({lat:setlat1,lng:setlong1},{lat:lat2,lng:long2});

		var distance = google.maps.geometry.spherical
		.computeDistanceBetween(
			new google.maps.LatLng(setlat1, setlong1), 
			new google.maps.LatLng(lat2, long2)
		);
		return Number((distance).toFixed(1));
	}

	atualizaVeiculos(){
		let ts = this; 
		ts.allmarkerscarros=[];

		ts.setintv = setInterval(function(){
			if(localStorage.getItem('logado')){
		        ts.fb.ref("motoristas/"+ts.cidade_esta)
		        .once('value', function (childSnapshot){
		        	var snap = childSnapshot.val();
		        	if(snap !== null){
		        		for(var p in snap){
		        			if(snap[p].status){
								if(snap[p].status == true){
									if(ts.curLatLng.lat && ts.curLatLng.lng){
										var checkdist = ts.calculaDistanciaRadius(ts.curLatLng.lat, ts.curLatLng.lng, snap[p].lat, snap[p].lng);
										if( checkdist <= ts.distanciaRadius){
											var icone2 = {
												url: 'assets/imgs/carro_jaubra.png',
												//scaledSize: new google.maps.Size(40, 40)
											};
											ts.markerscarros = new google.maps.Marker({
												position: new google.maps.LatLng(snap[p].lat, snap[p].lng),
												map: ts.map,
												icon: icone2
											});
											ts.allmarkerscarros.push(ts.markerscarros);
										}
									}
								}
							}
		        		}
		        	}
		        });
		        for(var i = 0; i < ts.allmarkerscarros.length; i++){
		          ts.allmarkerscarros[i].setMap(null);
		        }
			}else{
				clearInterval(ts.setintv);
			}
		}, 3000);
		ts.tempo_atualiza = 4000;
	}

	rota(pIdOrigem, pIdDestino){
		let ts = this;
		var directionsService = new google.maps.DirectionsService;
	    if(ts.directionsDisplay) {
	        ts.directionsDisplay.setMap(null);
	        ts.directionsDisplay = null;
	    }
    	ts.directionsDisplay = new google.maps.DirectionsRenderer();
		directionsService.route({
          origin: {'placeId': pIdOrigem},
          destination: {'placeId': pIdDestino},
		  optimizeWaypoints: true,
		  travelMode: google.maps.DirectionsTravelMode.DRIVING
		}, function(response, status) {
		  if(status === 'OK'){
		  	ts.itemOpcPgmSelec.distancia = response.routes[0].legs[0].distance.text;
		  	ts.itemOpcPgmSelec.duracao = response.routes[0].legs[0].duration.text;

		  	//console.log('steps',response.routes[0].legs[0].steps);
		  	//console.log('overview_path',response.routes[0].overview_path);

			/*var str1 = '[';
		  	var overpat = response.routes[0].overview_path
		  	for(var ov in overpat){
				var str2 = `['${overpat[ov].lat()}', '${overpat[ov].lng()}'],`;
				str1 += str2;
		  	}
		  	str1 += ']';

		  	console.log(str1);*/
		  	//console.log('distancia',response.routes[0].legs[0].distance.text);
		  	//console.log('duração',response.routes[0].legs[0].duration.text);

			for(var j = 0; j < response.routes.length; j++){
				ts.directionsDisplay.setMap(ts.map);
				ts.directionsDisplay.setDirections(response); 
				//ts.dirDisplay.setPanel(document.getElementById("setPanel"));
				ts.directionsDisplay.setRouteIndex(j);
				ts.directionsDisplay.setOptions({
					draggable: false,
					hideRouteIndex: true,
					polylineOptions : {
						strokeColor: "#29a3a9",
						//strokeColor: colours[j],
						strokeOpacity: 0.9,
						strokeWeight: 4
					}
				});
			}
			ts.confirmachamada = true;
			//ts.movimentoCarro(response);

			/*setTimeout(function(){
				if(document.getElementById('boxpgmt0')){
					document.getElementById('boxpgmt0').classList.add('forcefocus');
				}
			}, 400);*/
		  }else{
		  	console.log('erro', status);
		  }
		});
    }

	buscaEndeDestino(){
		let ts = this;
		if(ts.autocomplete.query_destino == ""){
			ts.confirmachamada = false;
		    if(ts.directionsDisplay) {
		        ts.directionsDisplay.setMap(null);
		        ts.directionsDisplay = null;
		    }
		}
		let service = new google.maps.places.AutocompleteService();
		this.iniciarbusca = true;
		this.destinselec = true;
		if(this.autocomplete.query_destino == ''){
			this.autocompleteItems = [];
		  	return;
		}
		this.tipoOrigemDestino = "d"
		this.textoinfoend_var='';
		
		service.getPlacePredictions({ input: this.autocomplete.query_destino, componentRestrictions: {country: 'BR'} }, (predictions, status) => {
		ts.autocompleteItems = []; 
			ts.zone.run( () => {
				if(status == "OK"){
					predictions.forEach( (res) => {
						ts.autocompleteItems.push({descricao:res.description, placeId:res.place_id});
					});
				}
			});
		});
	}

	dismiss(){
		this.viewCtrl.dismiss();
	}

	fechameuslocais(){
		this.clicameuslocais=false;
	}

	checkBlur(){
		this.iniciarbusca = false;
		this.destinselec = false;
	}

	abreLocaisSalvos(){
		this.opcoes_carros=[]
		this.destinselec = true;
		this.iniciarbusca = true;
		this.confirmachamada = false;
		setTimeout(function(){//}, 300);
			document.getElementById("buscadestino").focus();
		}, 300);
	}

	atualizar(){
		let ts = this;
		
		var latlng = {lat: ts.curLatLng.lat, lng: ts.curLatLng.lng};
		var geocoder = new google.maps.Geocoder();
	    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
	        if(status == google.maps.GeocoderStatus.OK){
	            if(results[1]){

            		var novoatual = {
						nome:results[1].formatted_address,
						endereco:results[1].formatted_address, 
						placeId:results[1].place_id, 
						detalhes:[], 
						lat:ts.curLatLng.lat, 
						lng:ts.curLatLng.lng,
						padrao:true
					};
	               	ts.placeIdOrigem=results[1].place_id;
	               	ts.autocomplete.query_origem=results[1].formatted_address;

					//localStorage.setItem('meus_locais', JSON.stringify([]));
					var todosende=[];
					var meuslocs = JSON.parse(localStorage.getItem('meus_locais'));
					for(var ml in meuslocs){
						if(ml == '0'){
							todosende.push(novoatual);
						}else{
		            		var outroend = {
								nome:meuslocs[ml].nome,
								endereco:meuslocs[ml].endereco, 
								placeId:meuslocs[ml].placeId, 
								detalhes:meuslocs[ml].detalhes, 
								lat:meuslocs[ml].lat, 
								lng:meuslocs[ml].lng,
								padrao:false
							};
							todosende.push(outroend);
						}
					}
					//localStorage.setItem('meus_locais', JSON.stringify(todosende));
					var mloca = JSON.parse(localStorage.getItem('meus_locais'));
					console.log(mloca);
	            }else{
	                alert('Não foi possível obter sua localização (e1)');
	            }
	        }else{
	            alert('Não foi possível obter sua localização (e2)');
	        }
	    });

		new google.maps.Circle({
			strokeColor: '#18777b',
			strokeOpacity: 0.8,
			strokeWeight: 2,
			fillColor: '#35c3ca',
			fillOpacity: 0.35,
			map: ts.map,
			center: {lat: ts.curLatLng.lat, lng: ts.curLatLng.lng},
			radius: Math.sqrt(100)
		});
		//var newpos = new google.maps.LatLng(mloc[m].lat,mloc[m].lng);
		ts.map.setCenter(latlng);
		ts.map.setZoom(17);

		/*setTimeout(function(){
			ts.map.setCenter(new google.maps.LatLng(ts.curLatLng.lat,ts.curLatLng.lng));
			ts.map.setZoom(15);
		}, 1500);
		if(ts.chamadaefetuada == true){
			setTimeout(function(){
				ts.map.setZoom(12);
				setTimeout(function(){
					var mloc = JSON.parse(localStorage.getItem('meus_locais'));
					for(var m in mloc){
						if( mloc[m].padrao == true ){
							var newpos = new google.maps.LatLng(mloc[m].lat,mloc[m].lng);
							ts.map.setCenter(newpos);
							ts.map.setZoom(15);
						}
					}
				}, 1500);
			}, 4000);
		}*/
	}
 
	addMarker(location, image) {
		let marker = new google.maps.Marker({
			position: location,
			map: this.map,
			icon: image,
			//animation: google.maps.Animation.DROP,
		});
		this.markers.push(marker);
	}

	setMapOnAll(map){
		for(var i = 0; i < this.markers.length; i++){
			this.markers[i].setMap(map);
		}
	}

	clearMarkers() {
		this.setMapOnAll(null);
	}

	deleteMarkers() {
		this.clearMarkers();
		this.markers = [];
	}

    // envia push notification
    enviaPush(regIdMoto:any, body:string, title:string){
        let headers = new Headers({ 
        	'Authorization': 'key=AAAAk2ACz9Y:APA91bECA4DYnVrTzhFGtCUa-9qqqSbAiGq3zvHtMObfmNKGPyyunu6vpfPlBWSxMDaDVyryCr4zEGKBKOZ-d6fMoeHwtGjObwf0hZxkohIxh58yECkoSU6ZH7pD0Pf7fa0vE9AhyeAQ', 
        	'Content-Type': 'application/json'
        });
        let options = new RequestOptions({ headers: headers });
        let notification = {
            "notification": {
            "title": title, //"Chamada solicitada",
            "body": body, //"Passageiro solicita corrida",
            //"click_action": "FCM_PLUGIN_ACTIVITY",
            "sound":"sound2",
            //"icon":"fcm_push_icon"
        }, "data": {
           "id_passageiro":this.meuid
        },
        	"to": regIdMoto,
            "priority": "high"
        }
        let url = 'https://fcm.googleapis.com/fcm/send';

        this.http.post(url, notification, options)
        .subscribe(resp => {
            //console.log(resp);
        });

        /*
            {
            "notification":{
                "title":"Notification title",
                "body":"Notification body",
                "sound":"default",
                "click_action":"FCM_PLUGIN_ACTIVITY",
                "icon":"fcm_push_icon"
              },
              "data":{
                "param1":"value1",
                "param2":"value2"
              },
                "to":"/topics/topicExample",
                "priority":"high",
                "restricted_package_name":""
            }
        */
    }

	registraFCM(){
		if(localStorage.getItem('id_fcm') == ''){
			let ts = this;
			const options: PushOptions = {
				android: {
					senderID: '632970989526',
					clearNotifications:true,
					clearBadge:false,
					forceShow:true,
					iconColor: '#33afb5'
				},
				ios: {
					alert: 'true',
					badge: true,
					sound: 'true'
				},
				windows: {},
				browser: {}
			};
			const pushObject: PushObject = this.push.init(options);

			pushObject.on('registration')
			.subscribe(function(regs){
				localStorage.setItem('id_fcm',JSON.stringify(regs.registrationId));
				ts.fb.ref('usuarios_fcm/'+ts.meuid)
				.set({fcmId:regs.registrationId});
			});
		}
	}

	ionViewWillLeave(){
		clearInterval(this.setintv);
		if(this.toast_chamada){
			this.toast_chamada.dismiss();
		}
	}

}

