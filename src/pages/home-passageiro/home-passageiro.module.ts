import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePassageiroPage } from './home-passageiro';

@NgModule({
  declarations: [
    HomePassageiroPage,
  ],
  imports: [
    IonicPageModule.forChild(HomePassageiroPage),
  ],
})
export class HomePassageiroPageModule {}
