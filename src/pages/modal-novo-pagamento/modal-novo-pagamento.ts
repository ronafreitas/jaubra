import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,ViewController } from 'ionic-angular';
import { ApiProvider } from './../../providers/api/api';
@IonicPage()
@Component({
  selector: 'page-modal-novo-pagamento',
  templateUrl: 'modal-novo-pagamento.html',
})
export class ModalNovoPagamentoPage {

	cartao = {
		numero: '',
		mesvenc: '',
		anovenc: '',
		cvv: '',
		tipo: ''
	};

	constructor(public navCtrl: NavController, 
		private apiProvider:ApiProvider, 
		public viewCtrl: ViewController,
		private alertCtrl: AlertController,	
		public navParams: NavParams) {
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ModalNovoPagamentoPage');
	}


	cardImage = 'assets/imgs/credit-card.png';

	checanumeros(tp, ev){
		if(tp == 'cartao'){
			if(this.cartao.numero.length > 16){
				var inpt1 = ev.target.value;
				var nwe1 = inpt1.substring(0, inpt1.length - 1);
				this.cartao.numero = nwe1;
			}
		}else if(tp == 'mes'){
			if(this.cartao.mesvenc.length > 2){
				var inpt2 = ev.target.value;
				var nwe2 = inpt2.substring(0, inpt2.length - 1);
				this.cartao.mesvenc = nwe2;
			}
		}else if(tp == 'ano'){
			if(this.cartao.anovenc.length > 2){
				var inpt3 = ev.target.value;
				var nwe3 = inpt3.substring(0, inpt3.length - 1);
				this.cartao.anovenc = nwe3;
			}
		}else{
			if(this.cartao.cvv.length > 3){
				var inpt4 = ev.target.value;
				var nwe4 = inpt4.substring(0, inpt4.length - 1);
				this.cartao.cvv = nwe4;
			}
		}
	}

	GetCardType(number){
	    // visa
	    var re = new RegExp("^4");
	    if (number.match(re) != null)
	        return "Visa";

	    // Mastercard 
	    // Updated for Mastercard 2017 BINs expansion
	     if (/^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/.test(number)) 
	        return "Mastercard";

	    // AMEX
	    re = new RegExp("^3[47]");
	    if (number.match(re) != null)
	        return "AMEX";

	    // Discover
	    re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
	    if (number.match(re) != null)
	        return "Discover";

	    // Diners
	    re = new RegExp("^36");
	    if (number.match(re) != null)
	        return "Diners";

	    // Diners - Carte Blanche
	    re = new RegExp("^30[0-5]");
	    if (number.match(re) != null)
	        return "Diners - Carte Blanche";

	    // JCB
	    re = new RegExp("^35(2[89]|[3-8][0-9])");
	    if (number.match(re) != null)
	        return "JCB";

	    // Visa Electron
	    re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
	    if (number.match(re) != null)
	        return "Visa Electron";

	    return "";
	}

	salvar(cartao){

		var d = new Date();
		var m = d.getMonth();
		m = m+1;
		var a = d.getFullYear().toString();
		var an = parseInt(a.substr(2, 2));

		if(cartao.numero.length < 16){
			let alert = this.alertCtrl.create({
				title: 'Número de cartão inválido',
				buttons: ['Ok']
			});
			alert.present();
			return false;
		}

		this.cartao.tipo = this.GetCardType(cartao.numero);

		if( (cartao.mesvenc.length < 2) ){
			let alert = this.alertCtrl.create({
				title: 'Mês de vencimento inválido',
				buttons: ['Ok']
			});
			alert.present();
			return false;
		}
		
		if( parseInt(cartao.mesvenc) < 1 || parseInt(cartao.mesvenc) > 12 ){
			let alert = this.alertCtrl.create({
				title: 'Mês de vencimento inválido',
				buttons: ['Ok']
			});
			alert.present();
			return false;
		}
		
		if(cartao.anovenc.length < 2){
			let alert = this.alertCtrl.create({
				title: 'Ano de vencimento inválido',
				buttons: ['Ok']
			});
			alert.present();
			return false;
		}

		if(cartao.anovenc < an){
			let alert = this.alertCtrl.create({
				title: 'Ano de vencimento inválido',
				buttons: ['Ok']
			});
			alert.present();
			return false;
		}

		if(cartao.cvv.length < 3){
			let alert = this.alertCtrl.create({
				title: 'Número CVV inválido',
				buttons: ['Ok']
			});
			alert.present();
			return false;
		}

		if( cartao.anovenc == an ){
			if( parseInt(cartao.mesvenc) < m ){
				let alert = this.alertCtrl.create({
					title: 'Data de vencimento inválida',
					buttons: ['Ok']
				});
				alert.present();
				return false;
			}
		}

		this.apiProvider.novoCartaoCredito(parseInt(JSON.parse(localStorage.getItem('dados_usuario')).id),cartao)
		.then((result: any) => {
			if(result.error){
				let alert = this.alertCtrl.create({
					title: 'Não foi possível cadastrar, verifique os dados e tente novamente',
					buttons: ['Ok']
				});
				alert.present();
				return false;
			}else{
			    this.viewCtrl.dismiss({confirmado:'ok'}); 
			}
		})
		.catch((error: any) => {
			console.log('error1',error);
		});
	}

	dismiss(){
		this.viewCtrl.dismiss();
	}
}
