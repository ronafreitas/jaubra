import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalNovoPagamentoPage } from './modal-novo-pagamento';

@NgModule({
  declarations: [
    ModalNovoPagamentoPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalNovoPagamentoPage),
  ],
})
export class ModalNovoPagamentoPageModule {}
