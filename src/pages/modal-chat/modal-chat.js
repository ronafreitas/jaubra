var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavParams, Content, ViewController } from 'ionic-angular';
import firebase from 'firebase';
import { Http, Headers, RequestOptions } from '@angular/http';
var ModalChatPage = /** @class */ (function () {
    function ModalChatPage(viewCtrl, navParams, http, zone) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.http = http;
        this.zone = zone;
        this.data = { perfil: '', nome: '', mensagem: '' };
        this.mensagens = []; //chats = [];
        this.fb = firebase.database();
    }
    ModalChatPage.prototype.ionViewDidLoad = function () {
        this.meu_id = JSON.parse(localStorage.getItem('dados_usuario')).id;
        var ts = this;
        this.fb.ref('chat/' + this.navParams.data.id_usuario + '-' + this.meu_id)
            .on('child_added', function (childSnapshot) {
            var snap = childSnapshot.val();
            if (snap !== null) {
                ts.zone.run(function () {
                    ts.mensagens.push({ perfil: snap.perfil, mensagem: snap.mensagem });
                });
            }
        });
    };
    ModalChatPage.prototype.sendMessage = function () {
        var _this = this;
        var msg = this.data.mensagem;
        if (msg != "") {
            var ts_1 = this;
            var chatdata = { perfil: this.navParams.data.tipo, mensagem: msg };
            //this.mensagens.push(chatdata);
            this.fb.ref('chat/' + this.meu_id + '-' + this.navParams.data.id_usuario).push(chatdata);
            this.fb.ref('chat/' + this.navParams.data.id_usuario + '-' + this.meu_id).push(chatdata);
            this.data.mensagem = '';
            this.fb.ref("usuarios_fcm/" + this.navParams.data.id_usuario + "/fcmId")
                .once('value', function (childSnapshot) {
                var snap = childSnapshot.val();
                if (snap !== null) {
                    ts_1.enviaPush(snap, "Mensagem do chat", msg);
                }
            });
            setTimeout(function () {
                _this.content.scrollToBottom(300);
            }, 1000);
        }
    };
    ModalChatPage.prototype.confirmar = function () {
        //this.viewCtrl.dismiss({busca:'ok', escolha:this.escolha, dados: this.navParams.data.data});
    };
    ModalChatPage.prototype.voltar = function () {
        this.viewCtrl.dismiss();
    };
    // envia push notification
    ModalChatPage.prototype.enviaPush = function (regIdMoto, body, title) {
        var headers = new Headers({
            'Authorization': 'key=AAAAk2ACz9Y:APA91bECA4DYnVrTzhFGtCUa-9qqqSbAiGq3zvHtMObfmNKGPyyunu6vpfPlBWSxMDaDVyryCr4zEGKBKOZ-d6fMoeHwtGjObwf0hZxkohIxh58yECkoSU6ZH7pD0Pf7fa0vE9AhyeAQ',
            'Content-Type': 'application/json'
        });
        var options = new RequestOptions({ headers: headers });
        var notification = {
            "notification": {
                "title": title,
                "body": body,
                "sound": "sound2",
            }, "data": {
                "id_passageiro": this.meu_id
            },
            "to": regIdMoto,
            "priority": "high"
        };
        var url = 'https://fcm.googleapis.com/fcm/send';
        this.http.post(url, notification, options)
            .subscribe(function (resp) {
            //console.log(resp);
        });
    };
    __decorate([
        ViewChild(Content),
        __metadata("design:type", Content)
    ], ModalChatPage.prototype, "content", void 0);
    ModalChatPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-modal-chat',
            templateUrl: 'modal-chat.html',
        }),
        __metadata("design:paramtypes", [ViewController, NavParams, Http, NgZone])
    ], ModalChatPage);
    return ModalChatPage;
}());
export { ModalChatPage };
//# sourceMappingURL=modal-chat.js.map