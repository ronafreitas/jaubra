import { Component, ViewChild,NgZone } from '@angular/core';
import { IonicPage, NavParams, Content,ViewController } from 'ionic-angular';
import firebase from 'firebase';
import { Http, Headers, RequestOptions } from '@angular/http';
@IonicPage()
@Component({
  selector: 'page-modal-chat',
  templateUrl: 'modal-chat.html',
})
export class ModalChatPage {
	@ViewChild(Content) content: Content;

	data = { perfil:'', nome:'', mensagem:'' };
	mensagens = []; //chats = [];
	id_usuario:any;
	meu_id:any;
	fb:any;

	constructor(public viewCtrl: ViewController,public navParams: NavParams,public http: Http,private zone: NgZone){
		this.fb = firebase.database();
	}

	ionViewDidLoad(){
		this.meu_id	= JSON.parse(localStorage.getItem('dados_usuario')).id;
		let ts = this;
		this.fb.ref('chat/'+this.navParams.data.id_usuario+'-'+this.meu_id)
		.on('child_added', function(childSnapshot){
			var snap = childSnapshot.val();
			if(snap !== null){
				ts.zone.run(() => {
					ts.mensagens.push({perfil:snap.perfil, mensagem:snap.mensagem});
				});
			}
		});
	}

	sendMessage(){
		var msg = this.data.mensagem;
		if(msg != ""){
			let ts = this;
			var chatdata = {perfil:this.navParams.data.tipo, mensagem:msg};
			//this.mensagens.push(chatdata);
	
			this.fb.ref('chat/'+this.meu_id+'-'+this.navParams.data.id_usuario).push(chatdata);
			this.fb.ref('chat/'+this.navParams.data.id_usuario+'-'+this.meu_id).push(chatdata);
			this.data.mensagem ='';
			this.fb.ref("usuarios_fcm/"+this.navParams.data.id_usuario+"/fcmId")
			.once('value', function (childSnapshot){
				var snap = childSnapshot.val();
				if(snap !== null){
					ts.enviaPush(snap, "Mensagem do chat", msg);
				}
			});
			setTimeout(() => {
				this.content.scrollToBottom(300);
			}, 1000);
		}
	}

	confirmar(){
		//this.viewCtrl.dismiss({busca:'ok', escolha:this.escolha, dados: this.navParams.data.data});
	}

	voltar(){
		this.viewCtrl.dismiss();
	}

  // envia push notification
  enviaPush(regIdMoto:any, body:string, title:string){
			let headers = new Headers({ 
				'Authorization': 'key=AAAAk2ACz9Y:APA91bECA4DYnVrTzhFGtCUa-9qqqSbAiGq3zvHtMObfmNKGPyyunu6vpfPlBWSxMDaDVyryCr4zEGKBKOZ-d6fMoeHwtGjObwf0hZxkohIxh58yECkoSU6ZH7pD0Pf7fa0vE9AhyeAQ', 
				'Content-Type': 'application/json'
			});
			let options = new RequestOptions({ headers: headers });
			let notification = {
					"notification": {
					"title": title,
					"body": body,
					"sound":"sound2",
			}, "data": {
				 "id_passageiro":this.meu_id
			},
				"to": regIdMoto,
					"priority": "high"
			}
			let url = 'https://fcm.googleapis.com/fcm/send';

			this.http.post(url, notification, options)
			.subscribe(resp => {
					//console.log(resp);
			});
	}

}
