import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-cadastro-nome',
  templateUrl: 'cadastro-nome.html',
})
export class CadastroNomePage {

	email:any;
	nomedigita:boolean=false;;
	sobrenomedigita:boolean=false;
	perfil:any;
	cadastro:any={nome:'', sobrenome:''};

	constructor(public navCtrl: NavController, private alertCtrl: AlertController, public navParams: NavParams) {
		this.email = navParams.get('email');
		this.perfil = navParams.get('perfil');
		this.cadastro.nome='';
		this.cadastro.sobrenome='';
	}

	exibeLimpa(tp, valor){
		if(tp == 'a'){
			if(valor){
				this.nomedigita=true;
			}else{
				this.nomedigita=false;
			}
		}else{
			if(valor){
				this.sobrenomedigita=true;
			}else{
				this.sobrenomedigita=false;
			}
		}
	}

	limpa(tp){
		if(tp == 'a'){
			this.cadastro.nome='';
			this.nomedigita=false;
		}else{
			this.cadastro.sobrenome='';
			this.sobrenomedigita=false;
		}
	}

	celular(nome, sobrenome){

		if(!nome){
			let alert = this.alertCtrl.create({
				title: 'Necessário preencher seu nome',
				buttons: ['Ok']
			});
			alert.present();
			return false;
		}

		if(!sobrenome){
			let alert = this.alertCtrl.create({
				title: 'Necessário preencher seu sobrenome',
				buttons: ['Ok']
			});
			alert.present();
			return false;
		}

	    this.navCtrl.push('CadastroCelularPage', {
	    	perfil:this.perfil,
	    	email:this.email,
	    	nome:this.cadastro.nome,
	    	sobrenome:this.cadastro.sobrenome
	    });
	}

}
