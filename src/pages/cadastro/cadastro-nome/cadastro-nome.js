var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
var CadastroNomePage = /** @class */ (function () {
    function CadastroNomePage(navCtrl, alertCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.nomedigita = false;
        this.sobrenomedigita = false;
        this.cadastro = { nome: '', sobrenome: '' };
        this.email = navParams.get('email');
        this.perfil = navParams.get('perfil');
        this.cadastro.nome = '';
        this.cadastro.sobrenome = '';
    }
    ;
    CadastroNomePage.prototype.exibeLimpa = function (tp, valor) {
        if (tp == 'a') {
            if (valor) {
                this.nomedigita = true;
            }
            else {
                this.nomedigita = false;
            }
        }
        else {
            if (valor) {
                this.sobrenomedigita = true;
            }
            else {
                this.sobrenomedigita = false;
            }
        }
    };
    CadastroNomePage.prototype.limpa = function (tp) {
        if (tp == 'a') {
            this.cadastro.nome = '';
            this.nomedigita = false;
        }
        else {
            this.cadastro.sobrenome = '';
            this.sobrenomedigita = false;
        }
    };
    CadastroNomePage.prototype.celular = function (nome, sobrenome) {
        if (!nome) {
            var alert_1 = this.alertCtrl.create({
                title: 'Necessário preencher seu nome',
                buttons: ['Ok']
            });
            alert_1.present();
            return false;
        }
        if (!sobrenome) {
            var alert_2 = this.alertCtrl.create({
                title: 'Necessário preencher seu sobrenome',
                buttons: ['Ok']
            });
            alert_2.present();
            return false;
        }
        this.navCtrl.push('CadastroCelularPage', {
            perfil: this.perfil,
            email: this.email,
            nome: this.cadastro.nome,
            sobrenome: this.cadastro.sobrenome
        });
    };
    CadastroNomePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-cadastro-nome',
            templateUrl: 'cadastro-nome.html',
        }),
        __metadata("design:paramtypes", [NavController, AlertController, NavParams])
    ], CadastroNomePage);
    return CadastroNomePage;
}());
export { CadastroNomePage };
//# sourceMappingURL=cadastro-nome.js.map