import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastroNomePage } from './cadastro-nome';

@NgModule({
  declarations: [
    CadastroNomePage,
  ],
  imports: [
    IonicPageModule.forChild(CadastroNomePage),
  ],
})
export class CadastroNomePageModule {}
