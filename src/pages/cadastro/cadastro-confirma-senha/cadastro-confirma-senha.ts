import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-cadastro-confirma-senha',
  templateUrl: 'cadastro-confirma-senha.html',
})
export class CadastroConfirmaSenhaPage {

	email:any;
	nome:any;
	sobrenome:any;
	celular:any;
	senha:any;
	perfil:any;
	cpf:any;
	cadastro:any={confirmasenha:''}

	constructor(public navCtrl: NavController, private alertCtrl: AlertController, public navParams: NavParams) {
		this.cadastro.confirmasenha='';
		this.email = navParams.get('email');
		this.nome = navParams.get('nome');
		this.sobrenome = navParams.get('sobrenome');
		this.celular = navParams.get('celular');
		this.senha = navParams.get('senha');
		this.perfil = navParams.get('perfil');
		this.cpf = navParams.get('cpf');
	}

	limpa(){
		this.cadastro.confirmasenha = ''
	}

	foto(confirmasenha){

		if(!confirmasenha){
			let alert = this.alertCtrl.create({
				title: 'Necessário confirmar sua senha',
				buttons: ['Ok']
			});
			alert.present();
			return false;
		}

		if(this.senha != confirmasenha){
			let alert = this.alertCtrl.create({
				title: 'Senha de confirmação diferente',
				buttons: ['Ok']
			});
			alert.present();
			return false;
		}

	    this.navCtrl.push('CadastroFotoPage', {
	    	perfil:this.perfil,
	    	email:this.email,
	    	nome:this.nome,
	    	sobrenome:this.sobrenome,
	    	celular:this.celular,
	    	cpf:this.cpf,
	    	senha:this.senha,
	    });
	}
}
