var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
var CadastroConfirmaSenhaPage = /** @class */ (function () {
    function CadastroConfirmaSenhaPage(navCtrl, alertCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.cadastro = { confirmasenha: '' };
        this.cadastro.confirmasenha = '';
        this.email = navParams.get('email');
        this.nome = navParams.get('nome');
        this.sobrenome = navParams.get('sobrenome');
        this.celular = navParams.get('celular');
        this.senha = navParams.get('senha');
        this.perfil = navParams.get('perfil');
        this.cpf = navParams.get('cpf');
    }
    CadastroConfirmaSenhaPage.prototype.limpa = function () {
        this.cadastro.confirmasenha = '';
    };
    CadastroConfirmaSenhaPage.prototype.foto = function (confirmasenha) {
        if (!confirmasenha) {
            var alert_1 = this.alertCtrl.create({
                title: 'Necessário confirmar sua senha',
                buttons: ['Ok']
            });
            alert_1.present();
            return false;
        }
        if (this.senha != confirmasenha) {
            var alert_2 = this.alertCtrl.create({
                title: 'Senha de confirmação diferente',
                buttons: ['Ok']
            });
            alert_2.present();
            return false;
        }
        this.navCtrl.push('CadastroFotoPage', {
            perfil: this.perfil,
            email: this.email,
            nome: this.nome,
            sobrenome: this.sobrenome,
            celular: this.celular,
            cpf: this.cpf,
            senha: this.senha,
        });
    };
    CadastroConfirmaSenhaPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-cadastro-confirma-senha',
            templateUrl: 'cadastro-confirma-senha.html',
        }),
        __metadata("design:paramtypes", [NavController, AlertController, NavParams])
    ], CadastroConfirmaSenhaPage);
    return CadastroConfirmaSenhaPage;
}());
export { CadastroConfirmaSenhaPage };
//# sourceMappingURL=cadastro-confirma-senha.js.map