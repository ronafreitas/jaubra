import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastroConfirmaSenhaPage } from './cadastro-confirma-senha';

@NgModule({
  declarations: [
    CadastroConfirmaSenhaPage,
  ],
  imports: [
    IonicPageModule.forChild(CadastroConfirmaSenhaPage),
  ],
})
export class CadastroConfirmaSenhaPageModule {}
