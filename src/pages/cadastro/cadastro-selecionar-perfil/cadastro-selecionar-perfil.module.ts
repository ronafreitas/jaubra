import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastroSelecionarPerfilPage } from './cadastro-selecionar-perfil';

@NgModule({
  declarations: [
    CadastroSelecionarPerfilPage,
  ],
  imports: [
    IonicPageModule.forChild(CadastroSelecionarPerfilPage),
  ],
})
export class CadastroSelecionarPerfilPageModule {}
