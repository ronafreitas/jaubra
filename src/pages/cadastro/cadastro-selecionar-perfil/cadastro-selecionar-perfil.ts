import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-cadastro-selecionar-perfil',
  templateUrl: 'cadastro-selecionar-perfil.html',
})
export class CadastroSelecionarPerfilPage {

	constructor(public navCtrl: NavController, public navParams: NavParams) {

	}

	escolhaPerfil(perfil){
	    this.navCtrl.push('CadastroTermosPage', {
	      perfil:perfil
	    })
	}

}
