var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
var CadastroSelecionarPerfilPage = /** @class */ (function () {
    function CadastroSelecionarPerfilPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    CadastroSelecionarPerfilPage.prototype.escolhaPerfil = function (perfil) {
        this.navCtrl.push('CadastroTermosPage', {
            perfil: perfil
        });
    };
    CadastroSelecionarPerfilPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-cadastro-selecionar-perfil',
            templateUrl: 'cadastro-selecionar-perfil.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], CadastroSelecionarPerfilPage);
    return CadastroSelecionarPerfilPage;
}());
export { CadastroSelecionarPerfilPage };
//# sourceMappingURL=cadastro-selecionar-perfil.js.map