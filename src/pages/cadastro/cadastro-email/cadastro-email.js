var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams, LoadingController } from 'ionic-angular';
import { CadastroProvider } from './../../../providers/cadastro/cadastro';
var CadastroEmailPage = /** @class */ (function () {
    function CadastroEmailPage(navCtrl, loadingCtrl, alertCtrl, navParams, cadastroProvider) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.cadastroProvider = cadastroProvider;
        this.cadastro = { email: '' };
        this.cadastro.email = '';
        this.perfil = navParams.get('perfil');
    }
    CadastroEmailPage.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };
    CadastroEmailPage.prototype.limpa = function () {
        this.cadastro.email = '';
    };
    CadastroEmailPage.prototype.nome = function (email) {
        var _this = this;
        var ts = this;
        ts.loading = ts.loadingCtrl.create({
            content: 'Aguarde...'
        });
        ts.loading.present();
        if (!email) {
            ts.loading.dismiss();
            var alert_1 = this.alertCtrl.create({
                title: 'Necessário preencher seu email',
                buttons: ['Ok']
            });
            alert_1.present();
            return false;
        }
        if (!this.validateEmail(email)) {
            ts.loading.dismiss();
            var alert_2 = this.alertCtrl.create({
                title: 'Preencher um email válido.',
                buttons: ['Ok']
            });
            alert_2.present();
            return false;
        }
        ts.cadastroProvider.checaEmail(email)
            .then(function (result) {
            if (result.exists) {
                ts.loading.dismiss();
                var alert_3 = _this.alertCtrl.create({
                    title: result.message,
                    buttons: ['Ok']
                });
                alert_3.present();
            }
            else {
                ts.loading.dismiss();
                ts.navCtrl.push('CadastroNomePage', {
                    email: email,
                    perfil: _this.perfil
                });
            }
        })
            .catch(function (error) {
            console.log('error1', error);
        });
    };
    CadastroEmailPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-cadastro-email',
            templateUrl: 'cadastro-email.html',
        }),
        __metadata("design:paramtypes", [NavController, LoadingController, AlertController, NavParams, CadastroProvider])
    ], CadastroEmailPage);
    return CadastroEmailPage;
}());
export { CadastroEmailPage };
//# sourceMappingURL=cadastro-email.js.map