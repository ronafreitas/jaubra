import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController,NavParams,LoadingController } from 'ionic-angular';
import { CadastroProvider } from './../../../providers/cadastro/cadastro';

@IonicPage()
@Component({
  selector: 'page-cadastro-email',
  templateUrl: 'cadastro-email.html',
})
export class CadastroEmailPage {

	perfil:any;
	loading:any;
	cadastro:any={email:''}

	constructor(public navCtrl: NavController, public loadingCtrl: LoadingController,private alertCtrl: AlertController, public navParams: NavParams, private cadastroProvider: CadastroProvider) {
		this.cadastro.email='';
		this.perfil = navParams.get('perfil');
	}

	validateEmail(email) {
	    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(String(email).toLowerCase());
	}

	limpa(){
		this.cadastro.email='';
	}

	nome(email){
		var ts = this;
		ts.loading = ts.loadingCtrl.create({
			content: 'Aguarde...'
		});
		ts.loading.present();

		if(!email){
			ts.loading.dismiss();
			let alert = this.alertCtrl.create({
				title: 'Necessário preencher seu email',
				buttons: ['Ok']
			});
			alert.present();
			return false;
		}
		if( !this.validateEmail(email) ){
			ts.loading.dismiss();
			let alert = this.alertCtrl.create({
				title: 'Preencher um email válido.',
				buttons: ['Ok']
			});
			alert.present();
			return false;
		}

		ts.cadastroProvider.checaEmail(email)
		.then((result: any) => {
			if(result.exists){
				ts.loading.dismiss();
				let alert = this.alertCtrl.create({
					title: result.message,
					buttons: ['Ok']
				});
				alert.present();
			}else{
				ts.loading.dismiss();
			    ts.navCtrl.push('CadastroNomePage', {
			    	email:email,
			    	perfil:this.perfil
			    });
			}
		})
		.catch((error: any) => {
			console.log('error1',error);
		});
	}
}
