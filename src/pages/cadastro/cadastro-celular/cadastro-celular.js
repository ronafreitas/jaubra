var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
var CadastroCelularPage = /** @class */ (function () {
    function CadastroCelularPage(navCtrl, alertCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.cadastro = { celular: "" };
        this.email = navParams.get('email');
        this.nome = navParams.get('nome');
        this.sobrenome = navParams.get('sobrenome');
        this.perfil = navParams.get('perfil');
        this.cadastro.celular = '';
    }
    CadastroCelularPage.prototype.limpa = function () {
        this.cadastro.celular = '';
    };
    CadastroCelularPage.prototype.cpf = function (celular) {
        if (!celular) {
            var alert_1 = this.alertCtrl.create({
                title: 'Necessário preencher seu celular',
                buttons: ['Ok']
            });
            alert_1.present();
            return false;
        }
        this.navCtrl.push('CadastroCpfPage', {
            perfil: this.perfil,
            email: this.email,
            nome: this.nome,
            sobrenome: this.sobrenome,
            celular: this.cadastro.celular
        });
    };
    CadastroCelularPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-cadastro-celular',
            templateUrl: 'cadastro-celular.html',
        }),
        __metadata("design:paramtypes", [NavController, AlertController, NavParams])
    ], CadastroCelularPage);
    return CadastroCelularPage;
}());
export { CadastroCelularPage };
//# sourceMappingURL=cadastro-celular.js.map