import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastroCelularPage } from './cadastro-celular';

@NgModule({
  declarations: [
    CadastroCelularPage,
  ],
  imports: [
    IonicPageModule.forChild(CadastroCelularPage),
  ],
})
export class CadastroCelularPageModule {}
