import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-cadastro-celular',
  templateUrl: 'cadastro-celular.html',
})
export class CadastroCelularPage {

	email:any;
	nome:any;
	sobrenome:any;
	perfil:any;
	cadastro:any={celular:""};

	constructor(public navCtrl: NavController, private alertCtrl: AlertController, public navParams: NavParams) {
		this.email = navParams.get('email');
		this.nome = navParams.get('nome');
		this.sobrenome = navParams.get('sobrenome');
		this.perfil = navParams.get('perfil');
		this.cadastro.celular = '';
	}

	limpa(){
		this.cadastro.celular = ''
	}

	cpf(celular){

		if(!celular){
			let alert = this.alertCtrl.create({
				title: 'Necessário preencher seu celular',
				buttons: ['Ok']
			});
			alert.present();
			return false;
		}

	    this.navCtrl.push('CadastroCpfPage', {
	    	perfil:this.perfil,
	    	email:this.email,
	    	nome:this.nome,
	    	sobrenome:this.sobrenome,
	    	celular:this.cadastro.celular
	    });
	}
}
