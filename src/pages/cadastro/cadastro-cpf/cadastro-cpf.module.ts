import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastroCpfPage } from './cadastro-cpf';

@NgModule({
  declarations: [
    CadastroCpfPage,
  ],
  imports: [
    IonicPageModule.forChild(CadastroCpfPage),
  ],
})
export class CadastroCpfPageModule {}
