import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,LoadingController } from 'ionic-angular';
import { ValidaCpfProvider } from './../../../providers/valida-cpf/valida-cpf';
import { ApiProvider } from './../../../providers/api/api';

@IonicPage()
@Component({
  selector: 'page-cadastro-cpf',
  templateUrl: 'cadastro-cpf.html',
})
export class CadastroCpfPage {

	perfil:any;
	email:any;
	nome:any;
	sobrenome:any;
	celular:any;
	cadastro:any={cpf:""};
	loading:any;

	constructor(public navCtrl: NavController, public loadingCtrl: LoadingController,private apiProvider:ApiProvider, public validacpf:ValidaCpfProvider,private alertCtrl: AlertController, public navParams: NavParams) {
		this.email = navParams.get('email');
		this.nome = navParams.get('nome');
		this.sobrenome = navParams.get('sobrenome');
		this.perfil = navParams.get('perfil');
		this.celular = navParams.get('celular');
		this.cadastro.cpf = '';
	}

	limpa(){
		this.cadastro.cpf = '';
	}

	senha(cpf){
		this.loading = this.loadingCtrl.create({
			content: 'Aguarde...'
		});
		this.loading.present();

		if(!cpf){
			let alert = this.alertCtrl.create({
				title: 'Necessário preencher seu CPF',
				buttons: ['Ok']
			});
			alert.present();
			this.loading.dismiss();
			return false;
		}

		if(!this.validacpf.cpf(cpf.toString())){
			let alert = this.alertCtrl.create({
				title: 'Informe CPF válido',
				buttons: ['Ok']
			});
			alert.present();
			this.loading.dismiss();
			return false;
		}

		this.apiProvider.checacpf(cpf.toString())
		.then((result: any) => {
			if(result.exists){
				this.loading.dismiss();
				let alert = this.alertCtrl.create({
					title: 'CPF já cadastrado',
					buttons: ['Ok']
				});
				alert.present();
				return false;
			}else{
				this.loading.dismiss();
			    this.navCtrl.push('CadastroSenhaPage', {
			    	perfil:this.perfil,
			    	email:this.email,
			    	nome:this.nome,
			    	sobrenome:this.sobrenome,
			    	celular:this.celular,
			    	cpf:this.cadastro.cpf
			    });	    
			}
		})
		.catch((error: any) => {
			console.log('error1',error);
		});
		
	}
}
