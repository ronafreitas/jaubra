var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { ValidaCpfProvider } from './../../../providers/valida-cpf/valida-cpf';
import { ApiProvider } from './../../../providers/api/api';
var CadastroCpfPage = /** @class */ (function () {
    function CadastroCpfPage(navCtrl, loadingCtrl, apiProvider, validacpf, alertCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.apiProvider = apiProvider;
        this.validacpf = validacpf;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.cadastro = { cpf: "" };
        this.email = navParams.get('email');
        this.nome = navParams.get('nome');
        this.sobrenome = navParams.get('sobrenome');
        this.perfil = navParams.get('perfil');
        this.celular = navParams.get('celular');
        this.cadastro.cpf = '';
    }
    CadastroCpfPage.prototype.limpa = function () {
        this.cadastro.cpf = '';
    };
    CadastroCpfPage.prototype.senha = function (cpf) {
        var _this = this;
        this.loading = this.loadingCtrl.create({
            content: 'Aguarde...'
        });
        this.loading.present();
        if (!cpf) {
            var alert_1 = this.alertCtrl.create({
                title: 'Necessário preencher seu CPF',
                buttons: ['Ok']
            });
            alert_1.present();
            this.loading.dismiss();
            return false;
        }
        if (!this.validacpf.cpf(cpf.toString())) {
            var alert_2 = this.alertCtrl.create({
                title: 'Informe CPF válido',
                buttons: ['Ok']
            });
            alert_2.present();
            this.loading.dismiss();
            return false;
        }
        this.apiProvider.checacpf(cpf.toString())
            .then(function (result) {
            if (result.exists) {
                _this.loading.dismiss();
                var alert_3 = _this.alertCtrl.create({
                    title: 'CPF já cadastrado',
                    buttons: ['Ok']
                });
                alert_3.present();
                return false;
            }
            else {
                _this.loading.dismiss();
                _this.navCtrl.push('CadastroSenhaPage', {
                    perfil: _this.perfil,
                    email: _this.email,
                    nome: _this.nome,
                    sobrenome: _this.sobrenome,
                    celular: _this.celular,
                    cpf: _this.cadastro.cpf
                });
            }
        })
            .catch(function (error) {
            console.log('error1', error);
        });
    };
    CadastroCpfPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-cadastro-cpf',
            templateUrl: 'cadastro-cpf.html',
        }),
        __metadata("design:paramtypes", [NavController, LoadingController, ApiProvider, ValidaCpfProvider, AlertController, NavParams])
    ], CadastroCpfPage);
    return CadastroCpfPage;
}());
export { CadastroCpfPage };
//# sourceMappingURL=cadastro-cpf.js.map