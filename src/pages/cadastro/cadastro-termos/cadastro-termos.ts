import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-cadastro-termos',
  templateUrl: 'cadastro-termos.html',
})
export class CadastroTermosPage {

	perfil:any;

	constructor(public navCtrl: NavController, public navParams: NavParams){
		this.perfil = navParams.get('perfil');
	}

	email(){
	    this.navCtrl.push('CadastroEmailPage',{perfil:this.perfil});
	}

}
