import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastroTermosPage } from './cadastro-termos';

@NgModule({
  declarations: [
    CadastroTermosPage,
  ],
  imports: [
    IonicPageModule.forChild(CadastroTermosPage),
  ],
})
export class CadastroTermosPageModule {}
