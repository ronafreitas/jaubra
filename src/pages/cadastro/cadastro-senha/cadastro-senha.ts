import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-cadastro-senha',
  templateUrl: 'cadastro-senha.html',
})
export class CadastroSenhaPage {

	cadastro:any={senha:""};
	email:any;
	nome:any;
	sobrenome:any;
	perfil:any;
	celular:any;
	cpf:any;

	constructor(public navCtrl: NavController, private alertCtrl: AlertController, public navParams: NavParams) {
		this.cadastro.senha='';
		this.email = navParams.get('email');
		this.nome = navParams.get('nome');
		this.sobrenome = navParams.get('sobrenome');
		this.perfil = navParams.get('perfil');
		this.celular = navParams.get('celular');
		this.cpf = navParams.get('cpf');
		
	}

	limpa(){
		this.cadastro.senha = ''
	}

	confirmasenha(senha){

		if(!senha){
			let alert = this.alertCtrl.create({
				title: 'Necessário preencher sua senha',
				buttons: ['Ok']
			});
			alert.present();
			return false;
		}

	    this.navCtrl.push('CadastroConfirmaSenhaPage', {
	    	perfil:this.perfil,
	    	email:this.email,
	    	nome:this.nome,
	    	sobrenome:this.sobrenome,
	    	celular:this.celular,
	    	cpf:this.cpf,
	    	senha:senha
	    });
	}
}
