import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastroSenhaPage } from './cadastro-senha';

@NgModule({
  declarations: [
    CadastroSenhaPage,
  ],
  imports: [
    IonicPageModule.forChild(CadastroSenhaPage),
  ],
})
export class CadastroSenhaPageModule {}
