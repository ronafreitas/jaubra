var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { CadastroProvider } from './../../../providers/cadastro/cadastro';
import { FileTransfer } from '@ionic-native/file-transfer';
import { Camera } from '@ionic-native/camera';
var CadastroFotoPage = /** @class */ (function () {
    function CadastroFotoPage(navCtrl, camera, alertCtrl, transfer, loadingCtrl, navParams, cadastroProvider) {
        this.navCtrl = navCtrl;
        this.camera = camera;
        this.alertCtrl = alertCtrl;
        this.transfer = transfer;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.cadastroProvider = cadastroProvider;
        this.captureDataUrl = 'assets/imgs/profile.jpg';
        this.email = navParams.get('email');
        this.nome = navParams.get('nome');
        this.sobrenome = navParams.get('sobrenome');
        this.celular = navParams.get('celular');
        this.senha = navParams.get('senha');
        this.perfil = navParams.get('perfil');
        this.cpf = navParams.get('cpf');
    }
    CadastroFotoPage.prototype.pictureFromCamera = function () {
        var options = {
            quality: 30,
            allowEdit: true,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            saveToPhotoAlbum: true
        };
        this.capturePhoto(options);
    };
    CadastroFotoPage.prototype.pictureFromGallery = function () {
        var options = {
            quality: 30,
            allowEdit: true,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            saveToPhotoAlbum: true
        };
        this.capturePhoto(options);
    };
    CadastroFotoPage.prototype.capturePhoto = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.camera.getPicture(options)];
                    case 1:
                        result = _a.sent();
                        this.captureDataUrl = "data:image/jpeg;base64," + result;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.error(e_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    CadastroFotoPage.prototype.uploadFile = function () {
        var loader = this.loadingCtrl.create({
            content: "Uploading..."
        });
        loader.present();
        var fileTransfer = this.transfer.create();
        var options = {
            fileKey: 'ionicfile',
            fileName: 'ionicfile',
            chunkedMode: false,
            mimeType: "image/jpeg",
            headers: {}
        };
        fileTransfer.upload(this.captureDataUrl, 'http://jaubra.faromidia.com.br/api_jaubra_uploads', options)
            .then(function (data) {
            console.log(data + " Uploaded Successfully");
            //this.imageFileName = "http://192.168.0.7:8080/static/images/ionicfile.jpg"
            loader.dismiss();
            // this.presentToast("Image uploaded successfully");
        }, function (err) {
            console.log(err);
            loader.dismiss();
            //this.presentToast(err);
        });
    };
    CadastroFotoPage.prototype.uploadImage = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Aguarde...'
        });
        this.loading.present();
        var ts = this;
        ts.captureDataUrl;
        ts.loading.dismiss();
    };
    CadastroFotoPage.prototype.pular = function () {
        this.gravarDados(false);
    };
    CadastroFotoPage.prototype.gravarDados = function (comFoto, imagePath) {
        if (imagePath === void 0) { imagePath = ''; }
        var ts = this;
        this.loading = this.loadingCtrl.create({
            content: 'Aguarde...'
        });
        this.loading.present();
        if (comFoto) {
            alert('com foto');
        }
        else {
            this.cadastroProvider.cadastrar(this.email, this.nome, this.sobrenome, this.celular, this.senha, this.perfil, this.cpf)
                .then(function (result) {
                ts.loading.dismiss();
                if (result.li) {
                    var dados = {
                        id: result.li,
                        email: ts.email,
                        nome: ts.nome,
                        sobrenome: ts.sobrenome,
                        celular: ts.celular,
                        senha: ts.senha,
                        perfil: ts.perfil,
                        cpf: ts.cpf
                    };
                    localStorage.setItem('dados_usuario', JSON.stringify(dados));
                    if (ts.perfil == 'mot') {
                        ts.navCtrl.setRoot('HomeMotoristaPage');
                    }
                    else {
                        ts.navCtrl.setRoot('HomePassageiroPage');
                    }
                }
                else {
                    var alert_1 = ts.alertCtrl.create({
                        title: 'Não foi possível continuar.',
                        buttons: ['Ok']
                    });
                    alert_1.present();
                }
            })
                .catch(function (error) {
                console.log('error1', error);
            });
        }
    };
    CadastroFotoPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-cadastro-foto',
            templateUrl: 'cadastro-foto.html',
        }),
        __metadata("design:paramtypes", [NavController,
            Camera,
            AlertController,
            FileTransfer,
            LoadingController,
            NavParams,
            CadastroProvider])
    ], CadastroFotoPage);
    return CadastroFotoPage;
}());
export { CadastroFotoPage };
//# sourceMappingURL=cadastro-foto.js.map