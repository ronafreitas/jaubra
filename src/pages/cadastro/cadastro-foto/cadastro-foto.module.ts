import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastroFotoPage } from './cadastro-foto';

@NgModule({
  declarations: [
    CadastroFotoPage,
  ],
  imports: [
    IonicPageModule.forChild(CadastroFotoPage),
  ],
})
export class CadastroFotoPageModule {}
