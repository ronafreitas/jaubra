import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController,AlertController } from 'ionic-angular';
import { CadastroProvider } from './../../../providers/cadastro/cadastro';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions} from '@ionic-native/camera';

@IonicPage()
@Component({
  selector: 'page-cadastro-foto',
  templateUrl: 'cadastro-foto.html',
})
export class CadastroFotoPage {

	email:any;
	nome:any;
	sobrenome:any;
	celular:any;
	senha:any;
	perfil:any;
	cpf:any;
	loading:any;
	captureDataUrl: string='assets/imgs/profile.jpg';
	imageURI:any;
	imageFileName:any;

	constructor(public navCtrl: NavController,
				public camera: Camera,
				public alertCtrl: AlertController,
				private transfer: FileTransfer,
				public loadingCtrl: LoadingController,
				public navParams: NavParams,
				private cadastroProvider: CadastroProvider){

		this.email = navParams.get('email');
		this.nome = navParams.get('nome');
		this.sobrenome = navParams.get('sobrenome');
		this.celular = navParams.get('celular');
		this.senha = navParams.get('senha');
		this.perfil = navParams.get('perfil');
		this.cpf = navParams.get('cpf');
	}

	pictureFromCamera(){
		const options: CameraOptions = {
		  quality: 30,
		  allowEdit: true,
		  destinationType: this.camera.DestinationType.DATA_URL,
		  encodingType: this.camera.EncodingType.JPEG,
		  mediaType: this.camera.MediaType.PICTURE,
		  correctOrientation: true,
		  saveToPhotoAlbum: true
		}

		this.capturePhoto(options);
	}

	pictureFromGallery(){
		const options: CameraOptions = {
		  quality: 30,
		  allowEdit: true,
		  destinationType: this.camera.DestinationType.DATA_URL,
		  encodingType: this.camera.EncodingType.JPEG,
		  sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
		  mediaType: this.camera.MediaType.PICTURE,
		  correctOrientation: true,
		  saveToPhotoAlbum: true
		}

		this.capturePhoto(options)
	}

	async capturePhoto(options: CameraOptions){
		try {
		  const result = await this.camera.getPicture(options)
		  this.captureDataUrl = `data:image/jpeg;base64,${result}`;
		}
		catch (e) {
		  console.error(e);
		}
	}

	uploadFile(){
	  let loader = this.loadingCtrl.create({
	    content: "Uploading..."
	  });
	  loader.present();
	  const fileTransfer: FileTransferObject = this.transfer.create();

	  let options: FileUploadOptions = {
	    fileKey: 'ionicfile',
	    fileName: 'ionicfile',
	    chunkedMode: false,
	    mimeType: "image/jpeg",
	    headers: {}
	  }

	  fileTransfer.upload(this.captureDataUrl, 'http://jaubra.faromidia.com.br/api_jaubra_uploads', options)
	    .then((data) => {
	    console.log(data+" Uploaded Successfully");
	    //this.imageFileName = "http://192.168.0.7:8080/static/images/ionicfile.jpg"
	    loader.dismiss();
	   // this.presentToast("Image uploaded successfully");
	  }, (err) => {
	    console.log(err);
	    loader.dismiss();
	    //this.presentToast(err);
	  });
	}

	uploadImage(){
		this.loading = this.loadingCtrl.create({
			content: 'Aguarde...'
		});
		this.loading.present();
		var ts = this;
		ts.captureDataUrl
		ts.loading.dismiss();
	}

	pular(){
		this.gravarDados(false);
	}

	gravarDados(comFoto:boolean, imagePath:string=''){
		let ts = this;
		this.loading = this.loadingCtrl.create({
			content: 'Aguarde...'
		});
		this.loading.present();

		if(comFoto){
			alert('com foto');
		}else{
			this.cadastroProvider.cadastrar(
				this.email,
				this.nome,
				this.sobrenome,
				this.celular,
				this.senha,
				this.perfil,
				this.cpf
				)
			.then((result: any) => {
				ts.loading.dismiss();	
				if(result.li){
					var dados = {
						id: result.li,
						email: ts.email,
						nome: ts.nome,
						sobrenome: ts.sobrenome,
						celular: ts.celular,
						senha: ts.senha,
						perfil: ts.perfil,
						cpf: ts.cpf
					}
					localStorage.setItem('dados_usuario', JSON.stringify(dados));
					if(ts.perfil == 'mot'){
						ts.navCtrl.setRoot('HomeMotoristaPage');
					}else{
						ts.navCtrl.setRoot('HomePassageiroPage');
					}
				}else{
					let alert = ts.alertCtrl.create({
						title: 'Não foi possível continuar.',
						buttons: ['Ok']
					});
					alert.present();
				}
			})
			.catch((error: any) => {
				console.log('error1',error);
			});
		}
	}
}
