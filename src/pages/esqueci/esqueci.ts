import { Component } from '@angular/core';
import { IonicPage, AlertController,LoadingController,ToastController } from 'ionic-angular';
import { ApiProvider } from './../../providers/api/api';

@IonicPage()
@Component({
  selector: 'page-esqueci',
  templateUrl: 'esqueci.html',
})
export class EsqueciPage {

	email:any;
	constructor(private alertCtrl: AlertController,private toastCtrl: ToastController,public loadingCtrl: LoadingController, private apiProvider:ApiProvider) {

	}

	validateEmail(email) {
	    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(String(email).toLowerCase());
	}

	solicitar(email){
		if(!email){
			let alert = this.alertCtrl.create({
				title: 'Necessário preencher seu email',
				buttons: ['Ok']
			});
			alert.present();
		}else{
			if( !this.validateEmail(email) ){
				let alert = this.alertCtrl.create({
					title: 'Preencher um email válido.',
					buttons: ['Ok']
				});
				alert.present();
				return false;
			}else{

				let lod = this.loadingCtrl.create({
					content: 'Aguarde...'
				});
				lod.present();

				this.apiProvider.getEmailSenhaRecuperar(email)
				.then((result: any) => {

					if(result.error){
						lod.dismiss();
						let alert = this.alertCtrl.create({
							title: 'Não foi possível utilizar esse recurso.',
							buttons: ['Ok']
						});
						alert.present();
					}else{
						if(result[0]){
							this.apiProvider.recuperarSenha(email,result[0].senha)
							.then((result: any) => {
								if(result.error){
									lod.dismiss();
									let alert = this.alertCtrl.create({
										title: 'Não foi possível utilizar esse recurso.',
										buttons: ['Ok']
									});
									alert.present();
									console.log('error', result);
								}else{
									lod.dismiss();
									let toast = this.toastCtrl.create({
										message: 'Sua senha foi enviada para seu email',
										duration: 4000,
										position: 'bottom'
									});
									toast.present();
									this.email='';
								}
							})
							.catch((error: any) => {
								console.log('error1',error);
							});
						}else{
							lod.dismiss();
							let alert = this.alertCtrl.create({
								title: 'Email não encontrado.',
								buttons: ['Ok']
							});
							alert.present();
						}
					}
				})
				.catch((error: any) => {
					console.log('error1',error);
				});
			}
		}
	}

	limpa(){
		this.email='';
	}
}
