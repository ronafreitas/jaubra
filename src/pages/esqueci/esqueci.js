var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { ApiProvider } from './../../providers/api/api';
var EsqueciPage = /** @class */ (function () {
    function EsqueciPage(alertCtrl, toastCtrl, loadingCtrl, apiProvider) {
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.apiProvider = apiProvider;
    }
    EsqueciPage.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };
    EsqueciPage.prototype.solicitar = function (email) {
        var _this = this;
        if (!email) {
            var alert_1 = this.alertCtrl.create({
                title: 'Necessário preencher seu email',
                buttons: ['Ok']
            });
            alert_1.present();
        }
        else {
            if (!this.validateEmail(email)) {
                var alert_2 = this.alertCtrl.create({
                    title: 'Preencher um email válido.',
                    buttons: ['Ok']
                });
                alert_2.present();
                return false;
            }
            else {
                var lod_1 = this.loadingCtrl.create({
                    content: 'Aguarde...'
                });
                lod_1.present();
                this.apiProvider.getEmailSenhaRecuperar(email)
                    .then(function (result) {
                    if (result.error) {
                        lod_1.dismiss();
                        var alert_3 = _this.alertCtrl.create({
                            title: 'Não foi possível utilizar esse recurso.',
                            buttons: ['Ok']
                        });
                        alert_3.present();
                    }
                    else {
                        if (result[0]) {
                            _this.apiProvider.recuperarSenha(email, result[0].senha)
                                .then(function (result) {
                                if (result.error) {
                                    lod_1.dismiss();
                                    var alert_4 = _this.alertCtrl.create({
                                        title: 'Não foi possível utilizar esse recurso.',
                                        buttons: ['Ok']
                                    });
                                    alert_4.present();
                                    console.log('error', result);
                                }
                                else {
                                    lod_1.dismiss();
                                    var toast = _this.toastCtrl.create({
                                        message: 'Sua senha foi enviada para seu email',
                                        duration: 4000,
                                        position: 'bottom'
                                    });
                                    toast.present();
                                    _this.email = '';
                                }
                            })
                                .catch(function (error) {
                                console.log('error1', error);
                            });
                        }
                        else {
                            lod_1.dismiss();
                            var alert_5 = _this.alertCtrl.create({
                                title: 'Email não encontrado.',
                                buttons: ['Ok']
                            });
                            alert_5.present();
                        }
                    }
                })
                    .catch(function (error) {
                    console.log('error1', error);
                });
            }
        }
    };
    EsqueciPage.prototype.limpa = function () {
        this.email = '';
    };
    EsqueciPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-esqueci',
            templateUrl: 'esqueci.html',
        }),
        __metadata("design:paramtypes", [AlertController, ToastController, LoadingController, ApiProvider])
    ], EsqueciPage);
    return EsqueciPage;
}());
export { EsqueciPage };
//# sourceMappingURL=esqueci.js.map