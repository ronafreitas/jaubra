var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { CadastroProvider } from './../../providers/cadastro/cadastro';
import { ValidaCpfProvider } from './../../providers/valida-cpf/valida-cpf';
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, alertCtrl, navParams, validacpf, loadingCtrl, cadastroProvider) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.validacpf = validacpf;
        this.loadingCtrl = loadingCtrl;
        this.cadastroProvider = cadastroProvider;
        this.user = { cpf: '', senha: '' };
        this.user.cpf = '';
        this.user.senha = '';
    }
    LoginPage.prototype.login = function (cpf, senha) {
        var _this = this;
        var ts = this;
        if (!cpf) {
            var alert_1 = this.alertCtrl.create({
                title: 'Informe seu CPF',
                buttons: ['Ok']
            });
            alert_1.present();
            return false;
        }
        if (!this.validacpf.cpf(cpf.toString())) {
            var alert_2 = this.alertCtrl.create({
                title: 'Informe CPF válido',
                buttons: ['Ok']
            });
            alert_2.present();
            return false;
        }
        if (!senha) {
            var alert_3 = this.alertCtrl.create({
                title: 'Informe sua senha',
                buttons: ['Ok']
            });
            alert_3.present();
            return false;
        }
        var lod = ts.loadingCtrl.create({
            content: 'Aguarde...'
        });
        lod.present();
        this.cadastroProvider.login(cpf, senha)
            .then(function (result) {
            if (result.error) {
                lod.dismiss();
                var alert_4 = _this.alertCtrl.create({
                    title: result.message,
                    buttons: ['Ok']
                });
                alert_4.present();
            }
            else {
                localStorage.setItem('dados_usuario', JSON.stringify(result[0]));
                localStorage.setItem('logado', 'on');
                if (JSON.parse(localStorage.getItem('dados_usuario')).perfil == 'mot') {
                    lod.dismiss();
                    ts.navCtrl.setRoot('HomeMotoristaPage');
                }
                else {
                    lod.dismiss();
                    ts.navCtrl.setRoot('HomePassageiroPage');
                }
            }
        })
            .catch(function (error) {
            lod.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Não foi possível acessar',
                buttons: ['Ok']
            });
            alert.present();
            console.log('error1', error);
        });
    };
    LoginPage.prototype.cadastrar = function () {
        this.navCtrl.push('CadastroSelecionarPerfilPage');
    };
    LoginPage.prototype.esqueciSenha = function () {
        this.navCtrl.push('EsqueciPage');
    };
    LoginPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-login',
            templateUrl: 'login.html',
        }),
        __metadata("design:paramtypes", [NavController,
            AlertController,
            NavParams,
            ValidaCpfProvider,
            LoadingController,
            CadastroProvider])
    ], LoginPage);
    return LoginPage;
}());
export { LoginPage };
//# sourceMappingURL=login.js.map