import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController, AlertController } from 'ionic-angular';
import { CadastroProvider } from './../../providers/cadastro/cadastro';
import { ValidaCpfProvider } from './../../providers/valida-cpf/valida-cpf';
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

	user:any = {cpf:'', senha:''};

	constructor(public navCtrl: NavController, 
		private alertCtrl: AlertController, 
		public navParams: NavParams, 
		public validacpf:ValidaCpfProvider,
		public loadingCtrl: LoadingController,
		private cadastroProvider: CadastroProvider){
		this.user.cpf='';
		this.user.senha='';
	}

	login(cpf:string, senha:string){
		var ts = this;

		if(!cpf){
			let alert = this.alertCtrl.create({
				title: 'Informe seu CPF',
				buttons: ['Ok']
			});
			alert.present();
			return false;
		}

		if(!this.validacpf.cpf(cpf.toString())){
			let alert = this.alertCtrl.create({
				title: 'Informe CPF válido',
				buttons: ['Ok']
			});
			alert.present();
			return false;
		}

		if(!senha){
			let alert = this.alertCtrl.create({
				title: 'Informe sua senha',
				buttons: ['Ok']
			});
			alert.present();
			return false;
		}
		let lod = ts.loadingCtrl.create({
			content: 'Aguarde...'
		});
		lod.present();
		this.cadastroProvider.login(cpf,senha)
		.then((result: any) => {
			if(result.error){
				lod.dismiss();
				let alert = this.alertCtrl.create({
					title: result.message,
					buttons: ['Ok']
				});
				alert.present();
			}else{
				localStorage.setItem('dados_usuario', JSON.stringify(result[0]));
				localStorage.setItem('logado', 'on');
				if(JSON.parse(localStorage.getItem('dados_usuario')).perfil == 'mot'){
					lod.dismiss();
					ts.navCtrl.setRoot('HomeMotoristaPage');
				}else{
					lod.dismiss();
					ts.navCtrl.setRoot('HomePassageiroPage');
				}
			}
		})
		.catch((error: any) => {
			lod.dismiss();
			let alert = this.alertCtrl.create({
				title: 'Não foi possível acessar',
				buttons: ['Ok']
			});
			alert.present();
			console.log('error1',error);
		});
	}

	cadastrar(){
	    this.navCtrl.push('CadastroSelecionarPerfilPage');
	}

	esqueciSenha(){
		this.navCtrl.push('EsqueciPage');
	}
}
